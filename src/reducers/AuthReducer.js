import {
	LOGIN_USER,
	LOGIN_USER_SUCCESS,
	LOGIN_USER_FAIL,
	NONCE_FETCH_FAILURE,
	USER_UPDATE
} from "../actions/types"

const INITIAL_STATE = {
	loading: false,
	username: "",
	password: "",
	user: {
		"thang": "this"
	}
};

export default (state= INITIAL_STATE, action ) => {
	switch (action.type){
		case LOGIN_USER:
			return { ... state, loading: true, error: ''};
		case LOGIN_USER_SUCCESS:
			return { ...state, ...INITIAL_STATE, user: action.payload, loading: false};
		case USER_UPDATE:
			return {... state, [action.payload.prop]: action.payload.value};
		case LOGIN_USER_FAIL:
			return { ... state, error: 'Authentication Failed.', password: '', confirmPassword: '', loading: false};
		case NONCE_FETCH_FAILURE:
			return { ... state, error: 'Nonce Error', loading: false};
		default:
			return state;
	}
};