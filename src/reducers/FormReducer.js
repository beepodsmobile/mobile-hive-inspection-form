//import from types here
import {CONTINUE_FORM, FORM_INITIALIZED, FORM_UPDATE, LOAD_YARD_FOR_FORM, HIVES_LOADED_FOR_FORMS, HIVE_SELECTED_FOR_FORM, WEATHER_SUCCESS, NEXT_BAR, MODAL_STATE_CHANGE} from '../actions/types'
const INITIAL_STATE = {
		data: [],
		pollenColors: [],
		externalBeeActivityLevel: '',
		ventBoardPosition: '',
		numberBarsInnerChamber: 0,
		numberBarsCombsAttached: 0,
		miteCheckPerformed: '',
		internalActivity: '',
		hiveCapacity: '',
		externalVisualBearding: false,
    	externalVisualGuarding: false,
    	externalVisualDrones: false,
    	externalVisualRobbers: false,
    	entranceUsed: '',
    	entranceActivity: '',
        finalNumberBars: 0,
        initialJarFluid: "0",
        finalJarFluid: "0",
        honeyHarvestedForMe: "0",
        honeyHarvestedForBees: "0",
        modalVisible: false,
    	beesFoundOnBars: false,
    	beesFoundOnLid: false,
    	beesFoundOnFloor: false,
    	deadBeesFound: false,
    	pollenCollectionVisible: '',
    	critters: [],
    	bugs: [],
    	bars: [],
        yardId: " ",
        currentBarNumber: 1,
        currentCombBuiltOut: 0,
        currentHoney: 0,
        currentNectar: 0,
        currentPollen: 0,
        currentEggs: 0,
        currentLarva: 0,
        currentDroneBrood: 0,
        currentWorkerBrood: 0,
        currentSpacer: false,
        currentJoin: false,
        currentWavyComb: false,
        currentAttachedComb: false,
        currentFollower: false,
        currentTeaCupCount: 0,
        currentUncappedCount: 0,
        currentCappedCount: 0,
        currentEmergedCount: 0,
        queenCellContent: "None",
        observations: "",
        hives: [],
        colonySplit: false

};

export default (state= INITIAL_STATE, action ) => {
	console.log("FormReducer", action);
	switch (action.type){
        case CONTINUE_FORM:
            return state;
        case FORM_INITIALIZED:
            return {... state,
                    currentBarNumber: 1,
                    currentCombBuiltOut: 0,
                    currentHoney: 0,
                    currentNectar: 0,
                    currentPollen: 0,
                    currentEggs: 0,
                    currentLarva: 0,
                    currentDroneBrood: 0,
                    currentWorkerBrood: 0,
                    currentSpacer: false,
                    currentJoin: false,
                    currentWavyComb: false,
                    currentAttachedComb: false,
                    currentFollower: false,
                    currentTeaCupCount: 0,
                    currentUncappedCount: 0,
                    currentCappedCount: 0,
                    currentEmergedCount: 0,
                    queenCellContent: "None",
                    observations: "",
                    colonySplit: false,
                    pollenColors: [],
                    externalBeeActivityLevel: '',
                    ventBoardPosition: '',
                    numberBarsInnerChamber: 0,
                    numberBarsCombsAttached: 0,
                    miteCheckPerformed: '',
                    internalActivity: '',
                    hiveCapacity: '',
                    externalVisualBearding: false,
                    externalVisualGuarding: false,
                    externalVisualDrones: false,
                    externalVisualRobbers: false,
                    entranceUsed: '',
                    entranceActivity: '',
                    finalNumberBars: 0,
                    initialJarFluid: "0",
                    finalJarFluid: "0",
                    honeyHarvestedForMe: "0",
                    honeyHarvestedForBees: "0",
                    modalVisible: false,
                    beesFoundOnBars: false,
                    beesFoundOnLid: false,
                    beesFoundOnFloor: false,
                    deadBeesFound: false,
                    pollenCollectionVisible: '',
                    critters: [],
                    bugs: [],
                    bars: []
                };
		case FORM_UPDATE:
			return {...state, [action.payload.prop]: action.payload.value };
		case LOAD_YARD_FOR_FORM:
            return {... state, yardId: action.payload.yardId, yardName:action.payload.yardName};
        case HIVES_LOADED_FOR_FORMS:
            return{... state, hives: action.payload};
        case HIVE_SELECTED_FOR_FORM:
            return {... state, hiveId: action.payload['id'], hiveName: action.payload['name']};
        case NEXT_BAR:
            return {... state, 
                        currentCombBuiltOut: 0,
                        currentHoney: 0,
                        currentNectar: 0,
                        currentPollen: 0,
                        currentEggs: 0,
                        currentLarva: 0,
                        currentDroneBrood: 0,
                        currentWorkerBrood: 0,
                        currentSpacer: false,
                        currentJoin: false,
                        currentWavyComb: false,
                        currentAttachedComb: false,
                        currentFollower: false,
                        currentTeaCupCount: 0,
                        currentUncappedCount: 0,
                        currentCappedCount: 0,
                        currentEmergedCount: 0};
        case MODAL_STATE_CHANGE:
            return {...state, modalVisible: !state.modalVisible};
        case WEATHER_SUCCESS:
            return{...state, temperature: action.payload.temperature, airPressure: action.payload.airPressure, humidity: action.payload.humidity};
        default:
			return state;
	}
};