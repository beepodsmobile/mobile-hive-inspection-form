import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import DisplayReducer from './DisplayReducer';
import FormReducer from './FormReducer';

export default combineReducers({
	auth: AuthReducer,
	display: DisplayReducer,
	form: FormReducer
});