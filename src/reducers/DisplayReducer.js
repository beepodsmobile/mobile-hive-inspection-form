//import from types here
import { 
    ADD_HIVE,
    ADD_YARD,
    ADDRESS_LOADED,
    HIVE_LOADING,
    HIVE_LOADED,
    HIVE_OWNER_STATUS_LOADED,
    INSPECTION_SELECTED,
    LOAD_YARDS,
    LOCATION_SUCCESS,
    POPULATE_YARD_DATA,
    SAVE_YARD_SETTINGS,
    YARD_LOADING,
    YARD_LOADED,
    LOGOUT,
    YARD_OWNER_STATUS_LOADED,
    YARD_SETTINGS_UPDATED
 } from '../actions/types';

const INITIAL_STATE = {
    currentHiveId: 0,
    currentHiveName: "",
    currentInspection: {},
    currentYardAddressCity: "",
    currentYardAddressLine: "",
    currentYardAddressState: "",
    currentYardAddressZip: "",
    currentYardDescription: "",
    currentYardLatitude: "",
    currentYardLongitude: "",
    currentYardId: 0,
    currentYardName: "",
    hiveInspections: [],
    hiveOwnerStatus: false,
    hiveLoading: false,
    yardHives: [],
    yards: [],
    yardLoading: false,
    yardOwnerStatus: false
};

export default (state = INITIAL_STATE, action ) => {
    console.log('displayReducer', action.type, action.payload)
	switch (action.type){
        case ADD_HIVE:
            return {... state, hiveInspections: [],
                        hiveLoading: false,
                        currentHiveId: 0,
                        currentHiveName: ""
                    };
        case ADD_YARD:
            return {... state, yardLoading: false, currentYardId:0, currentYardName:"", currentYardDescription: "", currentYardAddressLine: "", currentYardAddressCity: "", currentYardAddressState: "", currentYardAddressZip: ""};
        case ADDRESS_LOADED:
            return{... state, currentYardAddressZip: action.payload.zip, currentYardAddressState: action.payload.state, currentYardAddressCity: action.payload.city, currentYardAddressLine: action.payload.address};
        case INSPECTION_SELECTED:
            return {... state, currentInspection: action.payload};
        case HIVE_LOADED:
            return {... state, hiveInspections: action.payload};
        case HIVE_LOADING:
            return {... state, currentHiveId: action.payload.id, currentHiveName: action.payload.name};
        case LOGOUT:
            return {INITIAL_STATE};
        case HIVE_OWNER_STATUS_LOADED:
            return {... state, hiveOwnerStatus: action.payload};
        case LOAD_YARDS:
            return {... state, yards: action.payload};
        case LOCATION_SUCCESS:
            return {... state, currentYardLatitude: action.payload.latitude.toString(), currentYardLongitude: action.payload.longitude.toString()};
        case POPULATE_YARD_DATA:
            return {... state, currentYardAddressLine: action.payload.AddressLine, currentYardAddressCity: action.payload.City, currentYardAddressState: action.payload.State, currentYardAddressZip: action.payload.Zip};
        case SAVE_YARD_SETTINGS: 
            return state;
        case YARD_LOADED:
            return {... state, yardHives: action.payload, yardLoading: false};
        case YARD_LOADING:
            return { ... state, yardLoading: true, currentYardId: action.payload.id, currentYardName: action.payload.name, currentYardDescription: action.payload.description};
        case YARD_OWNER_STATUS_LOADED:
            return { ... state, yardOwnerStatus: action.payload};
        case YARD_SETTINGS_UPDATED:
            return {... state, [action.payload.prop]: action.payload.value, yardLoading: false};
		default:
			return state;
	}
}