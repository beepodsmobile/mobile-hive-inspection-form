import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Linking, KeyboardAvoidingView, Keyboard, Platform } from 'react-native';
import { connect } from 'react-redux';
import { userUpdate, loginUser } from '../../actions';
import { 
	Card, 
	CardSection, 
	Input, 
	Spinner,  
	TitledInput,
	WideButton, 
	BasicLink,
	} from '../common';

class LoginForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      focusDescriptionInput: false,
      keyboardAvoidingViewKey:"keyboardAvoidingViewKey"
    };
  }
componentDidMount() {
  // using keyboardWillHide is better but it does not work for android
  this.keyboardHideListener = Keyboard.addListener(Platform.OS === 'android' ? 'keyboardDidHide': 'keyboardWillHide', this.keyboardHideListener.bind(this));
}

componentWillUnmount() {
  this.keyboardHideListener.remove()
}
keyboardHideListener() {
  let newKey = 'keyboardAvoidingViewKey' + new Date().getTime();
  this.setState({
    keyboardAvoidingViewKey: newKey
  });
}
  onEmailChange(text){
    this.props.usernameChanged(text);
  }

  onPasswordChange ( text ){
    this.props.passwordChanged(text);
  }

  onButtonPress (){
    Keyboard.dismiss();
    const {username, password} = this.props;
    this.props.loginUser({ username, password});
  }

  renderButton(){
    if (this.props.loading){
      return <Spinner size = "large" />
    }
    return (
      <WideButton Title= "Login" Action={this.onButtonPress.bind(this)} />
    );
  } 

  render() {
    const signUpURL = "https://www.beepods.com/my-account/";
    const forgotPassword = "https://www.beepods.com/my-account/lost-password/";
    
    return (
      <KeyboardAvoidingView style={styles.container} key={this.state.keyboardAvoidingViewKey} behavior = 'padding'>
        <KeyboardAvoidingView behavior = 'height' style= {styles.image}>
          <Image
           source={require('../../img/beepods_tagline_logo-LARGE.png')}
           style = {{height:'60%', width: '80%', paddingTop: 20}}
          />
          <Text>Healthy Hive Management Software</Text>
        </KeyboardAvoidingView>

        <Text style={styles.errorTextStyle}>{this.props.error}</Text>
        <View style ={styles.loginForm}>
          <TitledInput 
            label='User Name'
            placeholder='Username'
            onChangeText = {value => this.props.userUpdate({ prop: 'username', value})}
            value = {this.props.username}
          />
          <TitledInput 

            label='Password'
            autoCorrect={false}
            placeholder='*******'
            secureTextEntry
            onChangeText = {value => this.props.userUpdate({ prop: 'password', value})}
            value = {this.props.password}
          />
          {this.renderButton()}
        </View>

        <View style = {styles.links}>
          <BasicLink
            Title="Forgot your password?"
            Action={()=> Linking.openURL(forgotPassword)}
          />
          <BasicLink
            Title="Sign up"
            Action={()=> Linking.openURL(signUpURL)}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:20,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  image:{
    flex:4,
    alignItems: 'center'
  },
  loginForm:{
    flex:3,
    padding:10,
    marginBottom:10,
    justifyContent: 'center'
  },
  links:{
    flex:1
  },
  errorTextStyle: {
    color: '#E64A19',
    alignSelf: 'center',

}

});
const mapStateToProps = ({auth}) => {
	const { username, password, error, loading } = auth;

	return { username, password, error, loading};
};

export default connect(mapStateToProps, {userUpdate, loginUser})(LoginForm);