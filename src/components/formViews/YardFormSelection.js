import React, { Component } from 'react';
import { connect } from 'react-redux';
import {BottomNavBar, DisplayWideTiles, Button, Header, Spinner} from '../common';
import {View, Text} from 'react-native';
import {getHivesForForm} from '../../actions';
import axios from 'axios';


class YardFormSelection extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: []
    }
  }
    componentWillMount(){
      let yardOptions = this.props.yards.map(item =>{
        let option = { };
        const userId = this.props.user['id'];
        option.key = parseInt(item['YardId']);
        option.display = item['YardName'];
        const yardId = item['YardId'];
        const yardName = item['YardName'];
        option.action = () => {this.props.getHivesForForm({yard: item, userId: userId})};
        return option;
      });

      this.setState( {
        data: yardOptions
      });
    }
    renderList(){
      if(this.state.data.length>0){
        return (
            <DisplayWideTiles options={this.state.data} />
        );
      }
      else{
        return(<Text style={{flex: .9}}>No Yards For Current User</Text>);
      }
    }
	render(){
		return(
      <View style={{flex: 1}}>
        {this.renderList()}
        <BottomNavBar activeTab={0}/>
      </View>
		);
	}

}

const mapStateToProps = (state) => {
    const {user} = state.auth;
    const {yards} = state.display
	return {user, yards};
};
export default connect( mapStateToProps, {getHivesForForm})(YardFormSelection);