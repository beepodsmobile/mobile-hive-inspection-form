import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class PollenCollectionVisible extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'Is pollen collection visible?';
    const options = [
      { key: 1, option: 'YES', action: () => {this.props.formUpdate({ prop: 'pollenCollectionVisible', value: true})}, buttonHeight: 120 },
      { key: 2, option: 'NO', action: () => {this.props.formUpdate({ prop: 'pollenCollectionVisible', value: false})}, buttonHeight: 120 }
    ];

    return(
			<ScrollView style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</ScrollView>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};


const mapStateToProps = (state) => {
  const {
  pollenCollectionVisible
  } = state.form
  return {
  pollenCollectionVisible
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(PollenCollectionVisible);

