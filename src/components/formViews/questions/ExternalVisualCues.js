import React, { Component } from 'react';
import { Text, ScrollView, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import {formUpdate, continueForm, setModalState} from '../../../actions';
import { RadioOptionsQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class ExternalVisualCues extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
  toggleState(stateToToggle){
    switch(stateToToggle){
      case 'externalVisualBearding':
        this.props.formUpdate({prop: 'externalVisualBearding', value: !this.props.externalVisualBearding});
        break;
      case 'externalVisualGuarding':
        this.props.formUpdate({prop: 'externalVisualGuarding', value: !this.props.externalVisualGuarding});
        break;
      case 'externalVisualDrones':
        this.props.formUpdate({prop: 'externalVisualDrones',value: !this.props.externalVisualDrones});
        break;
      case 'externalVisualRobbers':
        this.props.formUpdate({prop: 'externalVisualRobbers', value: !this.props.externalVisualRobbers});
        break;
      default:
        break;
    }
  }

  ButtonPress(){ 
    const prop = 'externalVisual';
    this.props.continueForm({prop});
  }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'External Visual Cues';
    const radioOptions = [
      { key: 1, option: 'External Bearding?', action: () => {this.toggleState('externalVisualBearding')}, value: this.props.externalVisualBearding },
      { key: 2, option: 'External Guarding?', action: () => {this.toggleState('externalVisualGuarding')}, value: this.props.externalVisualGuarding },
      { key: 3, option: 'External Drones?', action: () => {this.toggleState('externalVisualDrones')}, value: this.props.externalVisualDrones },
      { key: 4, option: 'External Robbers?', action: () => {this.toggleState('externalVisualRobbers')}, value: this.props.externalVisualRobbers }
    ];
    const buttonText = 'CONTINUE';
    const buttonAction = this.ButtonPress.bind(this);

    return(
			<ScrollView style={styles.containerStyle}>
        <RadioOptionsQuestion
          question={question}
          options={radioOptions}
          buttonText={buttonText}
          buttonAction={buttonAction}
        />
        <NotesModal/>
        <SaveExitButton/>
			</ScrollView>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
    externalVisualBearding,
    externalVisualGuarding,
    externalVisualDrones,
    externalVisualRobbers  
  } = state.form
  return {
    externalVisualBearding,
    externalVisualGuarding,
    externalVisualDrones,
    externalVisualRobbers  
  };
};

export default connect( mapStateToProps, {formUpdate, continueForm, setModalState})(ExternalVisualCues);

