import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {formUpdate, setModalState} from '../../../actions';
import { MultipleChoiceQuestion , WideButton, SaveExitButton} from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';

class ExternalBeeActivityLevel extends Component {
  componentDidMount(){
    Actions.refresh({
      noteButton: this.renderNoteButton()
    });
  }
  renderNoteButton(){
    return (
    <TouchableOpacity
      onPress = {()=>{this.props.setModalState()}}
      style = {{paddingRight: 10}}
    >
      <Text>
        Notes
      </Text>
    </TouchableOpacity>
  );
  }
	render () {
    const { containerStyle } = styles;
    return(
			<View style={containerStyle}>
        <MultipleChoiceQuestion
          question="What is your external bee activity level?"
          options={[
            { key: 4, option: 'NONE', action: () => {this.props.formUpdate({ prop: 'externalBeeActivityLevel', value: 'NONE'})}, buttonHeight: 100 },
            { key: 1, option: 'LIGHT', action: () => {this.props.formUpdate({ prop: 'externalBeeActivityLevel', value: 'LIGHT'})}, buttonHeight: 100 },
            { key: 2, option: 'MODERATE', action: () => {this.props.formUpdate({ prop: 'externalBeeActivityLevel', value: 'MODERATE'})}, buttonHeight: 100 },
            { key: 3, option: 'HEAVY', action: () => {this.props.formUpdate({ prop: 'externalBeeActivityLevel', value: 'HEAVY'})}, buttonHeight: 100 }
          ]}
        />
        <NotesModal/>
			 <SaveExitButton/>
      </View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
  externalBeeActivityLevel,
  modalVisible,
  } = state.form
  return {
  externalBeeActivityLevel,
  modalVisible
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(ExternalBeeActivityLevel);