import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Slider, ScrollView, Picker, Switch, FlatList } from 'react-native';
import { SliderList, SectionHeader, WideButton, Question, SaveExitButton} from '../../common';
import { connect } from 'react-redux';
import { BeepodsFont, PrimaryColor } from '../../../beePodsStyles.js';
import {formUpdate, continueForm, barUpdate,setModalState} from '../../../actions';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class BarQuestion extends Component{

    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
            currentSpacer: false,
            combBuiltOut: 0,
            honey: 0,
            nectar: 0,
            pollen: 0,
            larva: 0,
            droneBrood: 0,
            workerBrood: 0,
        }
    }

    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }

    continue(){
        let bars = this.props.bars;
        let nextBarNumber = this.props.currentBarNumber + 1;
        bars.push({
            barNumber: this.props.currentBarNumber,
            combBuiltOut: this.props.currentCombBuiltOut,
            contentsHoney: this.props.currentHoney,
            contentsNectar: this.props.currentNectar,
            contentsPollen: this.props.currentPollen,
            contentsEggs: this.props.currentEggs,
            contentsLarva: this.props.currentLarva,
            contentsBroodWorker: this.props.currentWorkerBrood,
            contentsBroodDrone: this.props.currentDroneBrood,
            spacer: this.props.currentSpacer ? 1 : 0 ,
            wavyComb: this.props.currentWavyComb ? 1 : 0,
            attachedComb: this.props.currentAttachedComb ? 1 : 0,
            follower: this.props.currentFollower ? 1 : 0,
            join: this.props.currentJoin ? 1 : 0,
            teaCupCount: this.props.currentTeaCupCount,
            uncappedQueenCellCount: this.props.currentUncappedCount,
            cappedCount: this.props.currentCappedCount,
            emergedQueenCellCount: this.props.currentEmergedCount
        });
        this.props.formUpdate({prop: 'currentBarNumber', value: nextBarNumber});
        this.props.barUpdate({prop: 'bars', value: bars});

    }
    handleSliderValueChange(e, selection){
        let honey = this.props.currentHoney;
        let nectar = this.props.currentNectar;
        let pollen = this.props.currentPollen;
        let larva = this.props.currentLarva;
        let droneBrood = this.props.currentDroneBrood;
        let workerBrood = this.props.currentWorkerBrood;
        let eggs = this.props.currentEggs;
        let counter = this.state.counter + 1;
        switch(selection){
            case "comb":
                this.props.formUpdate({ prop: 'currentCombBuiltOut', value: e})
                break;
            case "honey":
                honey = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentHoney', value: e})
                }
                break;
            case "nectar":
                nectar = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentNectar', value: e})
                }
                break;
            case "pollen":
                pollen = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentPollen', value: e})
                }
                break;
            case "larva":
                larva = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentLarva', value: e})
                }
                break;
            case "droneBrood":
                droneBrood = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentDroneBrood', value: e})
                }
                break;
            case "workerBrood":
                workerBrood = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentWorkerBrood', value: e})
                } 
                break;           
            case "eggs":
                eggs = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.formUpdate({ prop: 'currentEggs', value: e})
                }
                break; 
        }
        this.setState({
            counter: counter
        });
    }
    toggleState(stateToToggle){
        switch(stateToToggle){
            case 'currentWavyComb':
                this.props.formUpdate({prop: 'currentWavyComb', value: !this.props.currentWavyComb});
                break;
            case 'currentSpacer':
                this.props.formUpdate({prop: 'currentSpacer', value: !this.props.currentSpacer});
                break;
            case 'currentAttachedComb':
                this.props.formUpdate({prop: 'currentAttachedComb', value: !this.props.currentAttachedComb});
                break;
            case 'currentFollower':
                this.props.formUpdate({prop: 'currentFollower',value: !this.props.currentFollower});
                break;
            case 'currentJoin':
                this.props.formUpdate({prop: 'currentJoin',value: !this.props.currentFollower});
                break;
            default:
                break;
        }
    }
    upDateSliderValue(value, prop){
        this.props.formUpdate({prop: prop, value: value});
    }
    updateSpacer(spacer){
        this.props.formUpdate({ prop: 'currentSpacer', value: spacer});
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
    render(){
        const switchOptions = [
            { key: 100, option: 'Spacer?', action: () => {this.toggleState('currentSpacer')}, value: this.props.currentSpacer },
            { key: 1, option: 'Wavy Comb Seen?', action: () => {this.toggleState('currentWavyComb')}, value: this.props.currentWavyComb },
            { key: 2, option: 'Comb Attached To Wall?', action: () => {this.toggleState('currentAttachedComb')}, value: this.props.currentAttachedComb },
            { key: 3, option: 'Follower On This Bar?', action: () => {this.toggleState('currentFollower')}, value: this.props.currentFollower },
            { key: 4, option: 'This bar joined with next?', action: () => {this.toggleState('currentJoin')}, value: this.props.currentJoin }
        ]
        const options = [
            { key: 0, option: 'Comb Built Out', value: this.props.currentCombBuiltOut, action: (e) => {this.handleSliderValueChange(e, "comb")}},
            { key: 1, option: 'Honey', value: this.props.currentHoney, action: (e) => {this.handleSliderValueChange(e, "honey")}},
            { key: 2, option: 'Nectar', value: this.props.currentNectar, action:(e) => {this.handleSliderValueChange(e, "nectar")}},
            { key: 3, option: 'Pollen', value: this.props.currentPollen, action: (e) => {this.handleSliderValueChange(e, "pollen")} },
            { key: 4, option: 'Eggs', value: this.props.currentEggs, action: (e) => {this.handleSliderValueChange(e, "eggs")} },
            { key: 5, option: 'Larva', value: this.props.currentLarva, action: (e) => {this.handleSliderValueChange(e, "larva")}},
            { key: 6, option: 'Drone Brood', value: this.props.currentDroneBrood, action: (e) => {this.handleSliderValueChange(e, "droneBrood")}},
            { key: 7, option: 'Worker Brood', value: this.props.currentWorkerBrood, action: (e) => {this.handleSliderValueChange(e, "workerBrood")}}];
        return(
            <ScrollView>
                <SectionHeader>Comb Contents</SectionHeader>
                <SliderList
                    options = {options}
                    height = {400}
                />
                <SectionHeader>Details</SectionHeader>
                <View style={styles.optionsContainerStyle}>
                    <FlatList
                        style={styles.radioListStyle}
                        data={switchOptions}
                        alwaysBounceVertical={false}
                        renderItem={({ item }) => (
                            <View style={styles.listItem}>
                                <Text style={styles.listText}>{item.option}</Text>
                                <Switch
                                    onValueChange={item.action}
                                    value={item.value}
                                />
                            </View>
                        )}
                    />
                </View>
                <SectionHeader>Tea Cup Count</SectionHeader>
                <View style={styles.numberContainerStyle}>
                    <Text style={styles.numberTextStyle}>{this.props.currentTeaCupCount }</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={10}
                        step={1}
                        value={this.props.currentTeaCupCount}
                        onValueChange={(value)=>this.upDateSliderValue(value, 'currentTeaCupCount')}
                        minimumTrackTintColor={PrimaryColor}
                    />
                </View>
                <SectionHeader>Uncapped Queen Cell Count</SectionHeader>
                <View style={styles.numberContainerStyle}>
                    <Text style={styles.numberTextStyle}>{this.props.currentUncappedCount}</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={10}
                        step={1}
                        value={this.props.currentUncappedCount}
                        onValueChange={(value)=>this.upDateSliderValue(value, 'currentUncappedCount')}
                        minimumTrackTintColor={PrimaryColor}
                    />
                </View>
                <SectionHeader>Capped Queen Cell Count</SectionHeader>
                <View style={styles.numberContainerStyle}>
                    <Text style={styles.numberTextStyle}>{this.props.currentCappedCount}</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={10}
                        step={1}
                        value={this.props.currentCappedCount}
                        onValueChange={(value)=>this.upDateSliderValue(value, 'currentCappedCount')}
                        minimumTrackTintColor={PrimaryColor}
                    />
                </View>
                <SectionHeader>Emerged Queen Cell Count</SectionHeader>
                <View style={styles.numberContainerStyle}>
                    <Text style={styles.numberTextStyle}>{this.props.currentEmergedCount}</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={10}
                        step={1}
                        value={this.props.currentEmergedCount}
                        onValueChange={(value)=>this.upDateSliderValue(value, 'currentEmergedCount')}
                        minimumTrackTintColor={PrimaryColor}
                    />
                </View>
                <WideButton Action={this.continue.bind(this)} Title={' Continue    '}/>
                <NotesModal/>
                <SaveExitButton />
            </ScrollView>
        );
    }
}

const styles = {
    containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  optionsContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    paddingLeft: 10,
    alignSelf: 'center',
    fontSize: 16
  },
  numberContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  numberTextStyle: {
    fontSize: 24,
    fontFamily: BeepodsFont,
    textAlign: 'center'
  }
};
const mapStateToProps = (state) => {
    const {
        bars,
        currentBarNumber,
        currentCombBuiltOut,
        currentHoney,
        currentNectar,
        currentPollen,
        currentLarva,
        currentEggs,
        currentDroneBrood,
        currentWorkerBrood,
        currentSpacer,
        currentJoin,
        currentWavyComb,
        currentAttachedComb,
        currentFollower,
        currentTeaCupCount,
        currentUncappedCount,
        currentCappedCount,
        currentEmergedCount

    } = state.form
    return {
        bars,
        currentBarNumber,
        currentCombBuiltOut,
        currentHoney,
        currentJoin,
        currentNectar,
        currentPollen,
        currentLarva,
        currentEggs,
        currentDroneBrood,
        currentWorkerBrood,
        currentSpacer,
        currentWavyComb,
        currentAttachedComb,
        currentFollower,
        currentTeaCupCount,
        currentUncappedCount,
        currentCappedCount,
        currentEmergedCount
    };
};

export default connect( mapStateToProps, {formUpdate, continueForm, barUpdate, setModalState})(BarQuestion);