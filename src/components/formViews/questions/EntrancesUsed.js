import React, { Component } from 'react';
import { Text, View, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class EntrancesUsed extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'Which entrances are being used?';
    const options = [
      { key: 1, option: 'SIDE', action: () => {this.props.formUpdate({ prop: 'entrancesUsed', value: 'SIDE' })}, buttonHeight: 80 },
      { key: 2, option: 'END', action: () => {this.props.formUpdate({ prop: 'entrancesUsed', value: 'END' })}, buttonHeight: 80 },
      { key: 3, option: 'BOTH', action: () => {this.props.formUpdate({ prop: 'entrancesUsed', value: 'BOTH' })}, buttonHeight: 80 },
      { key: 4, option: 'ALTERNATIVE', action: () => {this.props.formUpdate({ prop: 'entrancesUsed', value: 'ALTERNATIVE' })}, buttonHeight: 80 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
  entrancesUsed
  } = state.form
  return {
  entrancesUsed
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(EntrancesUsed);

