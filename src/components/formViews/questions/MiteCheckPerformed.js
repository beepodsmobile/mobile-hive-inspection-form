import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class MiteCheckPerformed extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'Did you perform a mite check?';
    const options = [
      { key: 1, option: 'YES', action: () => {this.props.formUpdate({ prop: 'miteCheckPerformed', value: true })}, buttonHeight: 120 },
      { key: 2, option: 'NO', action: () => {this.props.formUpdate({ prop: 'miteCheckPerformed', value: false })}, buttonHeight: 120 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};


const mapStateToProps = (state) => {
  const {
  miteCheckPerformed
  } = state.form
  return {
  miteCheckPerformed
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(MiteCheckPerformed);