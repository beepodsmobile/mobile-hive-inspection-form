import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion,SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class InternalActivity extends Component {

    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'What is the internal activity level?';
    const options = [
      { key: 2, option: 'DOCILE', action: () => {this.props.formUpdate({ prop: 'internalActivity', value: 'DOCILE' })}, buttonHeight: 80 },
      { key: 1, option: 'ACTIVE', action: () => {this.props.formUpdate({ prop: 'internalActivity', value: 'ACTIVE' })}, buttonHeight: 80 },
      { key: 4, option: 'DEFENSIVE', action: () => {this.props.formUpdate({ prop: 'internalActivity', value: 'DEFENSIVE' })}, buttonHeight: 80 },
      { key: 3, option: 'FRENZIED', action: () => {this.props.formUpdate({ prop: 'internalActivity', value: 'FRENZIED' })}, buttonHeight: 80 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
  internalActivity
  } = state.form
  return {
  internalActivity
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(InternalActivity);