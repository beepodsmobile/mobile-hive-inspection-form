import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, ScrollView, Picker } from 'react-native';
import {Question, WideButton, SaveExitButton} from '../../common';
import { connect } from 'react-redux';
import {formUpdate, continueForm,setModalState} from '../../../actions';
import axios from 'axios';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';




class CritterPickerQuestion extends Component {

    constructor(props){
        super(props);
        this.state = {
            critterDefault: "Select critters",
            critterSelection: "Select critters",
            critterOptions: ["Select critters"]
        }
    }
    continue(){
        this.props.continueForm({prop: 'critterEvidence'});
    }
    componentWillMount(){
        let critterRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/critteroptions/`
        axios.get(critterRequestUrl)
            .then((response)=>{
                let options = response.data.map((option)=>{
                    console.log(option);
                    return option.CritterName;
                });
                options.push(this.state.critterDefault);
                this.setState({critterOptions: options, critters:response.data});
                console.log(this.state.critters);
            });
    }
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    getcritterSelections(){
        let critterSelection = this.props.critters.map(critter => {
            let selection = {
                display: critter['CritterName'],
                action: () => {this.removecritter(critter['CritterName'])}
            }
            return selection;
        });
        return critterSelection;
    }
    addCritter(){
        let critterSelections = this.props.critters;
        for(let i = 0; i < critterSelections.length; i++){
            if(critterSelections[i]['CritterName'] == this.state.critterSelection){
                return;
            }
        }
        for(let j = 0; j < this.state.critters.length; j++){
            if(this.state.critters[j]['CritterName'] == this.state.critterSelection){
                critterSelections.push(this.state.critters[j]);
                this.props.formUpdate({ prop: 'critters', value: critterSelections});
            }
        }
    }
    removecritter(name){
        console.log('name',name);
        let critterSelections = this.props.critters;
        console.log("critter selections", critterSelections);
        for(let i = 0; i < critterSelections.length; i++){
            console.log(name);
            if(critterSelections[i]['CritterName'] == name){

                let newCritterSelection;
                if(critterSelections.length == 1){
                    newCritterSelection = [];
                }
                else{
                    newCritterSelection = critterSelections.splice(i,1);
                    console.log(newCritterSelection);
                }

                this.props.formUpdate({ prop: 'critters', value: newCritterSelection});
            }
        }
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
    render(){
        const pickItems = this.state.critterOptions.map((item)=>{
            console.log(item);
            return <Picker.Item key= {item} label={item} value = {item} />
        });
        const { containerStyle, optionsContainerStyle, listItem, listText, radioListStyle } = styles;
        return(
            <View>
                <Question>
                    Select bugs, pests and critters that are evident in the hive
                </Question>
                <Picker
                    selectedValue={this.state.critterSelection}
                    onValueChange={(critter)=>(this.setState({critterSelection: critter}))}>
                    {pickItems}
                </Picker>
                <WideButton Action={this.addCritter.bind(this)} Title={' Add Critter    '}/>
                <FlatList
                    data={this.getcritterSelections()}
                    alwaysBounceVertical= {false}
                    renderItem={({item}) =>(
                        <View style={listItem}>
                            <Text style={listText}>{item.display}</Text>
                            <WideButton Title={' Remove    '} Action={item.action}/>
                        </View>
                        )}
                />
                <WideButton Action={this.continue.bind(this)} Title={' Continue    '}/>
                <NotesModal/>
                <SaveExitButton/>
            </View>

        );
    }

}
const styles = {
    containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  optionsContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    paddingLeft: 10,
    alignSelf: 'center',
    fontSize: 16
  }
};
const mapStateToProps = (state) => {
  const {
  critters
  } = state.form
  return {
  critters
  };
};

export default connect( mapStateToProps, {formUpdate, continueForm, setModalState})(CritterPickerQuestion);