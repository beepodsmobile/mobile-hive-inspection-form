import React, { Component } from 'react';
import { Text, ScrollView, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import {formUpdate, continueForm, setModalState} from '../../../actions';
import { RadioOptionsQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class BeesFound extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
  toggleState(stateToToggle){
    switch(stateToToggle){
      case 'beesFoundOnBars':
        this.props.formUpdate({prop: 'beesFoundOnBars', value: !this.props.beesFoundOnBars });
        break;
      case 'beesFoundOnLid':
        this.props.formUpdate({prop: 'beesFoundOnLid', value: !this.props.beesFoundOnLid});
        break;
      case 'beesFoundOnFloor':
        this.props.formUpdate({prop: 'beesFoundOnFloor', value: !this.props.beesFoundOnFloor});
        break;
      case 'deadBeesFound':
        this.props.formUpdate({prop: 'deadBeesFound', value:!this.props.deadBeesFound});
        break;
      default:
        break;
    }
  }

  ButtonPress(){
    this.props.continueForm({prop:'beesFound'});
  }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'Bee location/status';
    const radioOptions = [
      { key: 1, option: 'Bees on Bars?', action: () => {this.toggleState('beesFoundOnBars')}, value: this.props.beesFoundOnBars },
      { key: 2, option: 'Bees on Lid?', action: () => {this.toggleState('beesFoundOnLid')}, value: this.props.beesFoundOnLid },
      { key: 3, option: 'Bees on Floor?', action: () => {this.toggleState('beesFoundOnFloor')}, value: this.props.beesFoundOnFloor },
      { key: 4, option: 'Dead Bees Found?', action: () => {this.toggleState('deadBeesFound')}, value: this.props.deadBeesFound }
    ];
    const buttonText = 'CONTINUE';
    const buttonAction = this.ButtonPress.bind(this);

    return(
			<ScrollView style={styles.containerStyle}>
        <RadioOptionsQuestion
          question={question}
          options={radioOptions}
          buttonText={buttonText}
          buttonAction={buttonAction}
        />
        <NotesModal/>
        <SaveExitButton/>
			</ScrollView>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
      beesFoundOnBars,
      beesFoundOnLid,
      beesFoundOnFloor,
      deadBeesFound
  } = state.form
  return {
      beesFoundOnBars,
      beesFoundOnLid,
      beesFoundOnFloor,
      deadBeesFound
  };
};

export default connect( mapStateToProps, {formUpdate, continueForm, setModalState})(BeesFound);