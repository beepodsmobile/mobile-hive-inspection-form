import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, ScrollView, Picker } from 'react-native';
import {Question, WideButton, SaveExitButton} from '../../common';
import { connect } from 'react-redux';
import {formUpdate, continueForm, setModalState} from '../../../actions';
import axios from 'axios';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';




class BugPickerQuestion extends Component {

    constructor(props){
        super(props);
        this.state = {
            bugDefault: "Select Bugs",
            bugSelection: "Select Bugs",
            bugOptions: ["Select Bugs"]
        }
    }

    continue(){
        this.props.continueForm({prop: 'bugsSeen'});
    }
    componentWillMount(){
        let bugRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/bugoptions/`
        axios.get(bugRequestUrl)
            .then((response)=>{
                let options = response.data.map((option)=>{
                    console.log(option);
                    return option.BugName;
                });
                options.push(this.state.bugDefault);
                this.setState({bugOptions: options, bugs:response.data});
                console.log(this.state.bugs);
            });
    }
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    getBugSelections(){
        let bugSelection = this.props.bugs.map(bug => {
            let selection = {
                display: bug['BugName'],
                action: () => {this.removeBug(bug['BugName'])}
            }
            return selection;
        });
        return bugSelection;
    }
    addBug(){
        let bugSelections = this.props.bugs;
        for(let i = 0; i < bugSelections.length; i++){
            if(bugSelections[i]['BugName'] == this.state.bugSelection){
                return;
            }
        }
        for(let j = 0; j < this.state.bugs.length; j++){
            if(this.state.bugs[j]['BugName'] == this.state.bugSelection){
                bugSelections.push(this.state.bugs[j]);
                this.props.formUpdate({ prop: 'bugs', value: bugSelections});
            }
        }
    }
    removeBug(name){
        console.log('name',name);
        let bugSelections = this.props.bugs;
        console.log("bug selections", bugSelections);
        for(let i = 0; i < bugSelections.length; i++){
            console.log(name);
            if(bugSelections[i]['BugName'] == name){

                let newBugSelection;
                if(bugSelections.length == 1){
                    newBugSelection = [];
                }
                else{
                    newBugSelection = bugSelections.splice(i,1);
                    console.log(newBugSelection);
                }

                this.props.formUpdate({ prop: 'bugs', value: newBugSelection});
            }
        }
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
    render(){
        const pickItems = this.state.bugOptions.map((item)=>{
            console.log(item);
            return <Picker.Item key= {item} label={item} value = {item} />
        });
        const { containerStyle, optionsContainerStyle, listItem, listText, radioListStyle } = styles;
        return(
            <View>
                <Question>
                    Select Bugs Found
                </Question>
                <Picker
                    selectedValue={this.state.bugSelection}
                    onValueChange={(bug)=>(this.setState({bugSelection: bug}))}>
                    {pickItems}
                </Picker>
                <WideButton Action={this.addBug.bind(this)} Title={' Add Bug    '}/>
                <FlatList
                    data={this.getBugSelections()}
                    alwaysBounceVertical= {false}
                    renderItem={({item}) =>(
                        <View style={listItem}>
                            <Text style={listText}>{item.display}</Text>
                            <WideButton Title={' Remove    '} Action={item.action}/>
                        </View>
                        )}
                />
                <WideButton Action={this.continue.bind(this)} Title={' Continue    '}/>
                <NotesModal/>
                <SaveExitButton/>
            </View>

        );
    }

}
const styles = {
    containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  optionsContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    paddingLeft: 10,
    alignSelf: 'center',
    fontSize: 16
  }
};
const mapStateToProps = (state) => {
  const {
  bugs
  } = state.form
  return {
  bugs
  };
};

export default connect( mapStateToProps, {formUpdate, continueForm, setModalState})(BugPickerQuestion);