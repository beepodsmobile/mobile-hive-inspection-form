import React, { Component } from 'react';
import { Text, View, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { TextNumberQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class HoneyHarvestedForMe extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
  ButtonPress(){
    this.props.formUpdate({prop:'honeyHarvestedForMeComplete'});
  }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
    render () {
        const question = 'Honey Harvested For Me';
        const buttonText = 'CONTINUE';
        const buttonAction = this.ButtonPress.bind(this);


    return(
            <View style={styles.containerStyle}>
        <TextNumberQuestion
          question={question}
          buttonText={buttonText}
          buttonAction={buttonAction}
          value={this.props.honeyHarvestedForMe}
          onValueChange={ value => {this.props.formUpdate({ prop: 'honeyHarvestedForMe', value })} }
          buttonAction={buttonAction}
        />
        <NotesModal/>
        <SaveExitButton/>
            </View>
        );
    }

}

const styles = {
    containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
  honeyHarvestedForMe 
  } = state.form
  return {
  honeyHarvestedForMe 
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(HoneyHarvestedForMe);