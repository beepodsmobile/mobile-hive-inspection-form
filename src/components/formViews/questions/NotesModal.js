import React, {Component} from 'react';
import {Modal, TextInput, View} from 'react-native';
import {WideButton} from '../../common';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';


class NotesModal extends Component {
    constructor(props){
      super(props);
    }
    render(){
      const { containerStyle, textStyle } = styles;
        return(
            <Modal
                animationType="fade"
                transparent= {false}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    this.props.setModalState();
                }}>
          <View style={containerStyle}>
            <View style={{flex: .8}}>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' numberOfLines={10} style={textStyle} value={this.props.observations} multiline={true} onChangeText={(value)=>{this.props.formUpdate({prop: 'observations', value: value})}}/>
              <WideButton Action={() => {this.props.setModalState()}} Title={' Close    '}/>
            </View>
          </View>
        </Modal>
            );
    }
    
}
const styles = {
  textStyle: {
    padding:10,
    backgroundColor: "white",
    flex: 1,
    fontSize: 16,
    lineHeight: 40,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black'
  },
  containerStyle: {
    padding:20,
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center'
  }
};
const mapStateToProps = (state) => {
  const {
  observations,
  modalVisible
  } = state.form
  return {
  observations,
  modalVisible
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(NotesModal);