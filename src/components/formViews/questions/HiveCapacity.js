import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion,SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class HiveCapacity extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'What is the hive capacity?';
    const options = [
      { key: 1, option: 'FULL', action: () => {this.props.formUpdate({ prop: 'hiveCapacity', value: 'FULL'})}, buttonHeight: 100 },
      { key: 2, option: 'PARTLY FULL', action: () => {this.props.formUpdate({ prop: 'hiveCapacity', value: 'PARTLY FULL'})}, buttonHeight: 100 },
      { key: 3, option: 'SPARSE', action: () => {this.props.formUpdate({ prop: 'hiveCapacity', value: 'SPARSE'})}, buttonHeight: 100 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};


const mapStateToProps = (state) => {
  const {
  hiveCapacity
  } = state.form
  return {
  hiveCapacity
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(HiveCapacity);

