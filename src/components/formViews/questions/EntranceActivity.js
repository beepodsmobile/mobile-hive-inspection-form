import React, { Component } from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion,SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class EntranceActivity extends Component {

    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'What is your entrance activity?';
    const options = [
      { key: 1, option: 'ENTERING MORE', action: () => {this.props.formUpdate({ prop: 'entranceActivity', value: 'ENTERING MORE'})}, buttonHeight: 100 },
      { key: 2, option: 'EXITING MORE', action: () => {this.props.formUpdate({ prop: 'entranceActivity', value: 'EXITING MORE'})}, buttonHeight: 100 },
      { key: 3, option: 'ABOUT THE SAME', action: () => {this.props.formUpdate({ prop: 'entranceActivity', value: 'ABOUT THE SAME'})}, buttonHeight: 100 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};


const mapStateToProps = (state) => {
  const {
  entranceActivity
  } = state.form
  return {
  entranceActivity
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(EntranceActivity);