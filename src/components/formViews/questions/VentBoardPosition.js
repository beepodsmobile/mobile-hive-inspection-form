import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class VentBoardPosition extends Component {
  componentDidMount(){
    Actions.refresh({
      noteButton: this.renderNoteButton()
    });
  }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'What is the vent board position?';
    const options = [
      { key: 1, option: 'REMOVED', action: () => {this.props.formUpdate({ prop: 'ventBoardPosition', value: 'REMOVED' })}, buttonHeight: 80 },
      { key: 2, option: 'FULLY OPEN', action: () => {this.props.formUpdate({ prop: 'ventBoardPosition', value: 'FULLY OPEN' })}, buttonHeight: 80 },
      { key: 3, option: 'PARTIALLY OPEN', action: () => {this.props.formUpdate({ prop: 'ventBoardPosition', value: 'PARTIALLY OPEN' })}, buttonHeight: 80 },
      { key: 4, option: 'CLOSED', action: () => {this.props.formUpdate({ prop: 'ventBoardPosition', value: 'CLOSED' })}, buttonHeight: 80 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
    ventBoardPosition
  } = state.form
  return {
    ventBoardPosition
  };
}

export default connect ( mapStateToProps, {formUpdate,setModalState})(VentBoardPosition);
