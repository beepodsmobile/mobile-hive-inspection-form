import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {formUpdate, setModalState} from '../../../actions';
import { MultipleChoiceQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class ColonySplit extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'Should the colony be split?';
    const options = [
      { key: 1, option: 'YES', action: () => {this.props.formUpdate({ prop: 'colonySplit', value: true})}, buttonHeight: 120 },
      { key: 2, option: 'NO', action: () => {this.props.formUpdate({ prop: 'colonySplit', value: false})}, buttonHeight: 120 }
    ];

    return(
			<View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};


const mapStateToProps = (state) => {
  const {
  colonySplit
  } = state.form
  return {
  colonySplit
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(ColonySplit);