import React, {Component} from 'React';
import {Flatlist, Text, ScrollView, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { ColorOptionsQuestion, SaveExitButton } from '../../common';
import {formUpdate, continueForm, setModalState} from '../../../actions';
import axios from 'axios';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class PollenColor extends Component {



  componentWillMount(){
    let colorsCall = "https://www.beepods.com/wp-json/inspection/v1/pollencoloroptions/";

      axios.get(colorsCall)
      .then((response) => {
        let colors = response['data'].map(item => {
          let color = {};
          color.key = parseInt(item['PollenColorId']);
          color.hue = item['HexCode'];
          color.present = false;
          color.action = () => this.toggleState(item['PollenColorId']);
          return color;
        });
        this.props.formUpdate({prop: 'pollenColors', value: colors});
      });
    }
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
   toggleState(id){
      let myState = this.props.pollenColors.map( item => {
        if(item.key == id){
          item.present = !item.present;
        }
        return item;
      } );
      console.log("myState Updated", myState)
      this.props.formUpdate({
        prop: 'pollenColors',
        value: myState
      })
  }

  ButtonPress(){
    const prop = 'pollenColor';
    this.props.continueForm({prop});

  }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
  render () {
    const question = 'Pollen Color? \n (Select all that apply)';
    const buttonText = 'CONTINUE';
    const buttonAction = this.ButtonPress.bind(this);
    const colors = this.props.pollenColors

    return(
    <ScrollView>
        <ColorOptionsQuestion
          question={question}
          options= {this.props.pollenColors}
          buttonText={buttonText}
          buttonAction={buttonAction}
        />
        <NotesModal/>
        <SaveExitButton/>
    </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    pollenColors
    } = state.form
    return {
    pollenColors
    };
  };
export default connect(mapStateToProps, { formUpdate, continueForm, setModalState }) (PollenColor);
