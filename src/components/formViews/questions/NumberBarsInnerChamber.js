import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { NumberQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class NumberBarsInnerChamber extends Component {
    componentDidMount(){
        Actions.refresh({
            noteButton: this.renderNoteButton()
        });
    }
  ButtonPress(){
    this.props.formUpdate({prop:'numberBarsInnerChamberComplete'});
  }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
	render () {
    const question = 'Number of bars in the inner chamber';
    const buttonText = 'CONTINUE';
    const buttonAction = this.ButtonPress.bind(this);
    const minimumValue = 0;
    const maximumValue = 32;
    const step = 1;

    return(
			<View style={styles.containerStyle}>
        <NumberQuestion
          question={question}
          buttonText={buttonText}
          buttonAction={buttonAction}
          value={this.props.numberBarsInnerChamber}
          minimumValue={minimumValue}
          maximumValue={maximumValue}
          step={step}
          onValueChange={ value => {this.props.formUpdate({ prop: 'numberBarsInnerChamber', value })} }
          buttonAction={buttonAction}
        />
        <NotesModal/>
        <SaveExitButton/>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
  numberBarsInnerChamber
  } = state.form
  return {
  numberBarsInnerChamber
  };
};

export default connect( mapStateToProps, {formUpdate, setModalState})(NumberBarsInnerChamber);