import React, { Component } from 'react';
import { Text, View, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { formUpdate, setModalState } from '../../../actions';
import { MultipleChoiceQuestion, SaveExitButton } from '../../common';
import NotesModal from './NotesModal';
import { Actions } from 'react-native-router-flux';


class QueenCellContent extends Component {
    componentDidMount(){
      Actions.refresh({
        noteButton: this.renderNoteButton()
      });
    }
    renderNoteButton(){
        return (
            <TouchableOpacity
                onPress = {()=>{this.props.setModalState()}}
                style = {{paddingRight: 10}}
            >
                <Text>
                    Notes
                </Text>
            </TouchableOpacity>
        );
    }
    render () {
    const question = 'What is the Queen Cell Content?';
    const options = [
      { key: 1, option: 'EGGS', action: () => {this.props.formUpdate({ prop: 'queenCellContent', value: 'EGGS' })}, buttonHeight: 80 },
      { key: 2, option: 'ROYAL JELLY', action: () => {this.props.formUpdate({ prop: 'queenCellContent', value: 'ROYAL JELLY' })}, buttonHeight: 80 },
      { key: 3, option: 'LARVA', action: () => {this.props.formUpdate({ prop: 'queenCellContent', value: 'LARVA' })}, buttonHeight: 80 },
      { key: 6, option: 'UNCAPPED', action: () => {this.props.formUpdate({ prop: 'queenCellContent', value: 'UNCAPPED' })}, buttonHeight: 80 },
      { key: 4, option: 'CAPPED', action: () => {this.props.formUpdate({ prop: 'queenCellContent', value: 'CAPPED' })}, buttonHeight: 80 },
      { key: 5, option: 'NONE', action: () => {this.props.formUpdate({ prop: 'queenCellContent', value: 'NONE' })}, buttonHeight: 80 }
    ];

    return(
            <View style={styles.containerStyle}>
        <MultipleChoiceQuestion
          question={question}
          options={options}
        />
        <NotesModal/>
        <SaveExitButton/>
            </View>
        );
    }

}

const styles = {
    containerStyle: {
    flex: 1,
    paddingTop: 40
  }
};

const mapStateToProps = (state) => {
  const {
    queenCellContent
  } = state.form
  return {
    queenCellContent
  };
}

export default connect ( mapStateToProps, {formUpdate, setModalState})(QueenCellContent);
