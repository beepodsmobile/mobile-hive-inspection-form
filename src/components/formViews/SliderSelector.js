import React, { Component } from 'react';
import {View, Text, Slider, StyleSheet } from 'react-native';

export default class SlideSelector extends Component {
	constructor(props){
		super();
		this.state = {
				total: 100,
				combBuiltOut: 0,
				nectar: 0,
				honey: 0,
				brood: 0,
				minimum: 0
		};
		this.onSlideFinish = this.onSlideFinish.bind(this);
	}
	onSlideFinish() {
		alert(this._nectarInput.value);

		this.setState({

		});
	}
	render(){
		return(
			<View>
				<Text>{this.state.nectar}</Text>
			 	<Slider 
			 		minimumValue ={this.state.minimum} style={styles.bigblue} 
			 		value= {this.state.nectar} 
			 		onValueChange={val => this.setState({nectar: val})} 
			 		onSlidingComplete={this.onSlideFinish}
			 		ref={component => this._nectarInput = component}></Slider>
				<Slider minimumValue = {0.5} value={this.state.honey} onValueChange={val => this.setState({honey: val})}></Slider>
			</View>
		);
	}
}
const styles = StyleSheet.create({
  bigblue: {
    color: 'blue',
    width: 100
  }
});