import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View, Text, Image} from 'react-native';
import {submitForm} from '../../actions';
import {WideButton, BottomNavBar} from '../common'


class FormEnd extends Component {
    componentWillMount(){
        const today = this.getCurrentDate();
        let teaCupTotal = 0;
        let uncappedTotal = 0;
        let cappedTotal = 0;
        let emergedTotal = 0;
        for(var i = 0; i < this.props.bars.length; i++){
            teaCupTotal += this.props.bars[i].teaCupCount;
            uncappedTotal += this.props.bars[i].uncappedQueenCellCount;
            cappedTotal += this.props.bars[i].cappedCount;
            emergedTotal += this.props.bars[i].emergedQueenCellCount;
        }
        const inspection = {
            id: this.props.user['id'],
            hiveId: this.props.hiveId,
            inspectorEmail: this.props.user['email'],
            inspectorName: this.props.user['firstname']+" "+this.props.user['lastname'],
            guests: " ",
            datePosted: today,
            dateModified: today,
            temperature: this.props.temperature,
            airPressure: this.props.airPressure,
            humidity: this.props.humidity,
            exteriorActivity: this.props.externalBeeActivityLevel,
            exteriorBearding: this.props.externalVisualBearding ? 1 : 0,
            exteriorDrones: this.props.externalVisualDrones ? 1 : 0,
            exteriorGaurding: this.props.externalVisualGuarding ? 1 : 0,
            exteriorRobbers: this.props.externalVisualRobbers ? 1 : 0,
            entranceActivity: this.props.entranceActivity,
            entrancesUsed: this.props.entranceUsed,
            hiveCapacity: this.props.hiveCapacity,
            pollenCollection: this.props.pollenCollectionVisible ? 1 : 0,
            pollenArray: this.props.pollenColors,
            interiorActivity: this.props.internalActivity,
            numbersOfBarsWithAttachedComb: this.props.numberBarsCombsAttached,
            initialNumberOfBarsInInnerChamber: this.props.numberBarsInnerChamber,
            beesSeenOnLid: this.props.beesFoundOnLid ? 1 : 0,
            beesSeenOnBars: this.props.beesFoundOnBars ? 1 : 0,
            beesSeenOnFloor: this.props.beesFoundOnFloor ? 1 : 0,
            beesSeenDead: this.props.deadBeesFound ? 1 : 0,
            critterDetails: " ",
            critterArray: this.props.critters,
            bugDetails: " ", 
            bugArray: this.props.bugs,
            ventBoardPosition: this.props.ventBoardPosition,
            queenCellContent:this.props.queenCellContent,
            barArray: this.props.bars,
            teaCupTotal: teaCupTotal,
            uncappedTotal: uncappedTotal,
            cappedTotal: cappedTotal,
            emergedTotal: emergedTotal,
            miteCheckPerformed: this.props.miteCheckPerformed ? 1 : 0,
            shouldColonyBeSplit: this.props.colonySplit ? 1 : 0,
            observations: this.props.observations,
            actionsTaken: " ",
            futureActions: " ",
            nextTimeBring: " ",
            initialFeederFluid: this.props.initialJarFluid,
            finalFeederFluid: this.props.finalJarFluid,
            finalBarsInInnerChamber: this.props.finalNumberBars,
            honeyCollectedForMe: this.props.honeyHarvestedForMe,
            honeyCollectedForBees: this.props.honeyHarvestedForBees,
            complete: this.props.complete ? 1 : 0
        }
        this.props.submitForm({inspection});
    }

    getCurrentDate(){
        let dateItem = new Date();
        var year = dateItem.getFullYear();
        var month = dateItem.getMonth() + 1;
        var day = dateItem.getDate();
        var monthString;
        var dayString;
        if(month < 10){
            monthString = '0'+ String(month);
        }
        else{
            monthString = String(month);
        }
        if(day < 10 ){
            dayString = "0"+ String(day);
        }
        else{
            dayString= String(day);
        }
        return year+"-"+monthString+"-"+dayString;
    };
    render(){
        return(
            <View style={{flex:1}}>
                <Text style={{flex: .9}}>Submitting Form</Text>
                <BottomNavBar/>
            </View>
        );
    }

}

const mapStateToProps = (state) => {
    const { airPressure,
            colonySplit,
            temperature,
            humidity,
            pollenColors,
            externalBeeActivityLevel,
            ventBoardPosition,
            numberBarsInnerChamber,
            numberBarsCombsAttached,
            miteCheckPerformed,
            internalActivity,
            hiveCapacity,
            hiveId,
            externalVisualBearding,
            externalVisualGuarding,
            externalVisualDrones,
            externalVisualRobbers,
            entranceUsed,
            entranceActivity,
            finalNumberBars,
            initialJarFluid,
            finalJarFluid,
            honeyHarvestedForMe,
            honeyHarvestedForBees,
            beesFoundOnBars,
            beesFoundOnFloor,
            beesFoundOnLid,
            deadBeesFound,
            pollenCollectionVisible,
            critters,
            bugs,
            bars,
            observations,
            complete,
            queenCellContent
            } = state.form;
    const {user} = state.auth;
    return { airPressure,
            colonySplit,
            temperature,
            humidity,
            pollenColors,
            externalBeeActivityLevel,
            ventBoardPosition,
            numberBarsInnerChamber,
            numberBarsCombsAttached,
            miteCheckPerformed,
            internalActivity,
            hiveCapacity,
            hiveId,
            externalVisualBearding,
            externalVisualGuarding,
            externalVisualDrones,
            externalVisualRobbers,
            entranceUsed,
            entranceActivity,
            finalNumberBars,
            initialJarFluid,
            finalJarFluid,
            honeyHarvestedForMe,
            honeyHarvestedForBees,
            beesFoundOnBars,
            beesFoundOnFloor,
            beesFoundOnLid,
            deadBeesFound,
            pollenCollectionVisible,
            critters,
            bugs,
            bars,
            observations,
            complete,
            user,
            queenCellContent
            };
};
export default connect( mapStateToProps, {submitForm})(FormEnd);