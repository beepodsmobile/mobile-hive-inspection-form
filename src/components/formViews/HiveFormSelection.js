import React, { Component } from 'react';
import { connect } from 'react-redux';
import {DisplayWideTiles, BottomNavBar} from '../common'
import {getFormStartSplash} from '../../actions';
import {View, Text} from 'react-native';


class HiveFormSelection extends Component {
    constructor(props){
        super(props);
        this.state={
            data: []
        }
    }
    componentWillMount(){
        let hiveOptions = this.props.hives.map(item =>{
            let option = { };
            option.key = parseInt(item['HiveId']);
            option.display = item['HiveName'].substring(0, 20);
            const hiveId = item['HiveId'];
            const hiveName = item['HiveName'];
            option.action = () => {this.props.getFormStartSplash({hiveId, hiveName})};
            return option;
            });

            this.setState( {
                data: hiveOptions
            });
            console.log('state', this.state.data);
    }
    renderList(){
      if(this.state.data.length>0){
        return (
            <DisplayWideTiles options={this.state.data} />
        );
      }
      else{
        return(<Text style={{flex: .9}}>No Hives For Current User</Text>);
      }
    }
	render(){
		return(
            <View style={{flex: 1}}>
                {this.renderList()}
                <BottomNavBar/>
            </View>
		);
	}

}

const mapStateToProps = (state) => {
	const {hives} = state.form;
	return {hives};
};
export default connect( mapStateToProps, {getFormStartSplash})(HiveFormSelection);