import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View, Text, Image} from 'react-native';
import {beginForm} from '../../actions';
import {WideButton, BottomNavBar, Question} from '../common'


class FormStart extends Component {
    startForm(){
        this.props.beginForm()
    }
	render(){
		return(
            <View style={{flex:1}}>
                <View style={{flex: .9, paddingTop: 40}}><Question>When entering information into this inspection form, describe the hive, bars and comb as you found it (before taking actions) not after you have made changes. </Question></View>
                <WideButton Title={' Begin Form    '} Action={this.startForm.bind(this)}/>
                <BottomNavBar/>
            </View>
		);
	}

}

const mapStateToProps = (state) => {
	const {hiveName} = state.form
	return {hiveName};
};
export default connect( mapStateToProps, {beginForm})(FormStart);