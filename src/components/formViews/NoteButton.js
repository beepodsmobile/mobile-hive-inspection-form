import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { BeepodsFont } from '../../beePodsStyles.js';
import {setModalState} from '../../actions';

const NotesButton = (props) => {
  const { buttonStyle, textStyle } = styles;
  return (
    <TouchableOpacity
      onPress = {()=>{this.props.setModalState()}}
      style = {[buttonStyle, {backgroundColor: this.props.color, height: this.props.height, width: '100%'}]}
    >
      <Text style={textStyle}>
        Notes
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    borderRadius: 5,
    marginTop: 5,
    margin: 5,
    marginBottom: 5,
    justifyContent: 'center'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    //fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: BeepodsFont
  }
};
export default connect( mapStateToProps, { setModalState})(NotesButton);