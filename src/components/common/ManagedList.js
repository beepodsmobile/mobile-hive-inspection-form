import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { ListButton, Button, HorizontalRule, Confirm } from '../common';
import { PrimaryColor, DangerColor, LightGrayColor } from '../../beePodsStyles.js';

const ManagedList = ({ data, addButtonText, addButtonOnPress, removeButtonOnPress }) => {
  return (
    <View style={styles.container}>
      <Button color={PrimaryColor} onPress={addButtonOnPress}>
        {addButtonText}
      </Button>
      <FlatList
        data={data}
        alwaysBounceVertical={false}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text style={styles.listText}>{item.name}</Text>
            <Confirm onAccept={item.accept} visible={item.visible} onDecline={item.decline}>Are You Sure</Confirm>
            <ListButton onPress={item.removeButtonOnPress} color={DangerColor}>Remove</ListButton>
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    alignSelf: 'center',
    fontSize: 16
  }
});

export  { ManagedList };
