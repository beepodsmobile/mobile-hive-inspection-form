import React, { Component } from 'react';
import {Text, View, ScrollView, ListView, FlatList} from 'react-native';

import {WideTile} from './index';
import { BeepodsGraySix, BeepodsGrayfour, BeepodsPaleYellow, DarkGrayColor, GrayColor, LightGrayColor } from '../../beePodsStyles.js';


class DisplayWideTiles extends Component {

    constructor(props){
        super(props);
        this.state ={
            options: props.options
        }
    }


// to do add flex for heights on all of this
    render () {
        console.log('options', this.props.options);
        return(
            <View style= {styles.containerStyle}>
                <FlatList
                    data={this.props.options}
                    renderItem = {({item}) => (
                        <WideTile key={item.key} color={BeepodsPaleYellow} onPress={item.action} display={item.display}/>
                    )}
                />
            </View>
        );
    }

}
const styles = {
    containerStyle: {
        flex: .9,
        paddingLeft: 10,
        paddingRight: 18,
        paddingTop:20,
        backgroundColor: "white",
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flatListStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    }
};
export {DisplayWideTiles};



