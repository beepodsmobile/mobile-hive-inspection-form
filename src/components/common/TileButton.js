import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

//TODO : add onPress property
const TileButton = (props) => {

	return (
			<TouchableOpacity 
			style = {styles.tileStyle} >
				<Text style = {styles.textStyle}>+</Text>
			</TouchableOpacity>		

	);
}

const styles = {
	tileStyle: {
		borderWidth: 2,
		borderRadius: 5,
		padding: 5,
		margin: 5,
		position: 'relative',
		height: 150,
		width: 150,
		alignItems: 'center',
		justifyContent: 'space-around',
		backgroundColor: 'white'
	},
	textStyle: {
		fontSize: 50,
		fontWeight: 'bold',
		color: 'green',
		
	}
};

export {TileButton};