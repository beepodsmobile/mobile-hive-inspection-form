import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { DarkGrayColor} from '../../beePodsStyles.js';

const Question = ({ Title }) => {
    return (
        <View style={styles.containerStyle}>
            <Text style={styles.labelStyle}>{Title}</Text>
        </View>
    );
};


const styles = {
    labelStyle: {
        fontSize: 24,
        color: DarkGrayColor,
        fontWeight: 'bold',
    },
    containerStyle: {
        alignItems: 'center',
    }
};

export {Question} ;