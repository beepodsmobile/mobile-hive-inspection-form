import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { DarkGrayColor, GrayColor, LightGrayColor } from '../../beePodsStyles.js';

const TitledInput = ({ label, value, onChangeText,onEndEditing, placeholder, secureTextEntry,maxLength }) => {

    const { inputStyle, labelStyle, containerStyle } = styles;

    return (
        <View style={containerStyle}>
            <Text style={labelStyle}>{label.toUpperCase()}</Text>
            <TextInput
                autoCorrect={false}
                placeholder={placeholder}
                secureTextEntry={secureTextEntry}
                value={value}
                onChangeText={onChangeText}
                onEndEditing={onEndEditing}
                style={inputStyle}
                maxLength = {maxLength}
                underlineColorAndroid = 'transparent'
            />
        </View>
    );
};

const styles = {
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        paddingBottom: 2,
        color: DarkGrayColor,
        fontSize: 18,
        fontWeight: '200',
        flex: 1,
        height: 40
    },
    labelStyle: {
        fontSize: 12,
        color: DarkGrayColor,
        fontWeight: '200',
        flex: 1
    },
    containerStyle: {
        height: 45,
        flexDirection: 'column',
        width: '100%',
        borderColor: LightGrayColor,
        borderBottomWidth: 1,
        marginBottom: 10
    }
};

export  {TitledInput} ;
