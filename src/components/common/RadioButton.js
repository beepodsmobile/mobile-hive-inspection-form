import React, {Component} from 'react';
import {StyleSheet, Text,View,TouchableHighlight} from 'react-native';
import { DarkGrayColor, GrayColor, LightGrayColor } from '../../beePodsStyles.js';

const RadioButton = ({ Value,Title }) => {

    let color = 'white';

    ButtonStyle = function(){
        return{
            backgroundColor: color,
            borderColor: GrayColor,
            borderRadius: 25,
            height:50,
            width:50,
            borderWidth: 4,
            paddingLeft: 16,
            paddingTop: 14,
            paddingBottom: 16,
            margin:10,
        }
    }

    return (
        <View style={styles.FormStyle}>
            <View style={ButtonStyle()}>
                <TouchableHighlight
                    underlayColor={GrayColor}
                    onPress= {(Value)=> Value ?
                                (Value=false, color = 'white'):
                                (Value=true, color = '#4F80E1')}
                >
                    <Text></Text>
                </TouchableHighlight>
            </View>
            <Text style = {styles.TextStyle}>{Title}</Text>
        </View>
    );
};

const styles = {
    TextStyle: {
        fontSize: 14,
        fontWeight: 'bold',
        marginTop:20
      },
    ButtonStyle:{
        backgroundColor:'white',
        borderColor: GrayColor,
        borderRadius: 25,
        height:50,
        width:50,
        borderWidth: 4,
        paddingLeft: 16,
        paddingTop: 14,
        paddingBottom: 16,
        margin:10,
    },
    FormStyle:{
        flexDirection:'row'
    }
}

export {RadioButton};
