import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {LightGrayColor, BeepodsFont} from '../../beePodsStyles.js';



const WideTile = (props) => {
    return (
            <TouchableOpacity 
                style = {[styles.tileStyle, {backgroundColor: props.color}]}
                onPress = {props.onPress}
            >
                <Text style={props.display === "+"?styles.textPlusStyle:styles.textStyle}>{props.display}</Text>
            </TouchableOpacity>     

    );
}

const styles = {
    tileStyle: {
        borderWidth: 2,
        borderRadius: 5,
        padding: 5,
        margin: 20,
        height: 130,
        width: 260,
        alignItems: 'center',
        borderColor: LightGrayColor,
        justifyContent: 'center',
    },
    textStyle: {
        fontFamily: BeepodsFont,
        fontSize: 30
    },
    textPlusStyle: {
        fontFamily: BeepodsFont,
        fontSize: 75
    }
};

export {WideTile};


