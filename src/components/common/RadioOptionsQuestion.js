import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, Switch } from 'react-native';
import { PrimaryColor } from '../../beePodsStyles.js';
import { Button, Question, HorizontalRule } from '../common';

class RadioOptionsQuestion extends Component {

  constructor(props){
    super(props);
    this.state = {
      question: props.question,
      options: props.options,
      buttonText: props.buttonText,
      buttonAction: props.buttonAction
    };
  }

  renderSeparator = () => {
    return (
      <HorizontalRule />
    );
  };

	render () {
    const { containerStyle, optionsContainerStyle, listItem, listText, radioListStyle } = styles;
    return(
			<View style={containerStyle}>
        <Question>
          {this.props.question}
        </Question>
        <View style={optionsContainerStyle}>
          <FlatList
            style={radioListStyle}
            data={this.props.options}
            alwaysBounceVertical={false}
            renderItem={({ item }) => (
              <View style={listItem}>
                <Text style={listText}>{item.option}</Text>
                <Switch
                  onValueChange={item.action}
                  value={item.value}
                />
              </View>
            )}
            ItemSeparatorComponent={this.renderSeparator}
          />
          <Button color={PrimaryColor} height={80} onPress={this.props.buttonAction}>{this.props.buttonText}</Button>
        </View>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  optionsContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    alignSelf: 'center',
    fontSize: 16
  }
};

export { RadioOptionsQuestion };
