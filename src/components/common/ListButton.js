import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { BeepodsFont } from '../../beePodsStyles.js';

const ListButton = ( { onPress, children, color } ) => {
		const {
			textStyle,
			buttonStyle
		} = styles;

	return (
		<TouchableOpacity
		 onPress = {onPress}
		 style = {[buttonStyle, {backgroundColor: color}]}
		 >
		<Text style = {textStyle}> {children} </Text>
		</TouchableOpacity>
		);
};


const styles = {
	buttonStyle: {
	width: 90,
	borderRadius: 5,
    marginTop: 5,
    marginBottom: 5
  },
  	textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
		fontFamily: BeepodsFont
  }
};

export { ListButton };
