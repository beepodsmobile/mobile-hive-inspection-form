import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { BeepodsFont } from '../../beePodsStyles.js';

const Button = ({ onPress, children, color, height }) => {
  const { buttonStyle, textStyle } = styles;
  return (
    <TouchableOpacity
      onPress = {onPress}
      style = {[buttonStyle, {backgroundColor: color, height: height, width: '100%'}]}
    >
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    borderRadius: 5,
    marginTop: 5,
    margin: 5,
    marginBottom: 5,
    justifyContent: 'center'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    //fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: BeepodsFont
  }
};

export { Button };
