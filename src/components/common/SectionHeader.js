import React from 'react';
import { View, Text } from 'react-native';
import { DarkGrayColor, LightGrayColor } from '../../beePodsStyles.js';
import { BeepodsFont } from '../../beePodsStyles.js';

const SectionHeader = ({ children }) => {
  const { containerStyle, textStyle } = styles;
  return (
		<View style={containerStyle}>
      <Text style={textStyle}>
        {children}
      </Text>
		</View>
  );
};

const styles = {
  containerStyle: {
    backgroundColor: LightGrayColor,
    marginTop: 5,
		paddingTop: 7,
		paddingBottom: 7,
    marginBottom: 5
  },
  textStyle: {
    color: DarkGrayColor,
    alignSelf: 'center',
    fontSize: 16,
    fontFamily: BeepodsFont
  }
};

export { SectionHeader };
