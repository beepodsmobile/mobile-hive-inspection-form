import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { ColorButton, Question, Button } from './index';
import {PrimaryColor} from '../../beePodsStyles.js';


class ColorOptionsQuestion extends Component {

	constructor(props){
    super(props);
    this.state = {
      question: props.question,
      options: props.options,
      buttonText: props.buttonText,
      //buttonAction: props.buttonAction
    };
  }
  componentWillReceiveProps(nextProps){
  			this.setState({options: nextProps.options})
  	}
  renderColors(){
		return this.state.options.map(color =>
			<ColorButton  key = {color.key} hue = {color.hue} action ={color.action} coco = {color.present} />
	);
	}
	
	

	render(){
		return (
		<View  >
	        <Question >
	          	{this.props.question}
	        </Question>
			
			<ScrollView style = {styles.scrollViewStyle}>
					<View style = {styles.containerStyle} >
						{this.renderColors()}
					</View>
			</ScrollView>
			<Button color = {PrimaryColor} marginTop={0} height={80} onPress={this.props.buttonAction} >{this.props.buttonText}</Button>

		</View>
		);
	}
}
const styles = {
	containerStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        flex:1
    },
    scrollViewStyle: {
    	height: 250
    },
};
export {ColorOptionsQuestion};
    

