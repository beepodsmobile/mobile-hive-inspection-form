import React from 'react';
import { View } from 'react-native';
import { LightGrayColor } from '../../beePodsStyles.js';

const HorizontalRule = () => {
  return (
    <View
      style={{
        height: 1,
        backgroundColor: LightGrayColor,
      }}
    />
  );
};

export { HorizontalRule };
