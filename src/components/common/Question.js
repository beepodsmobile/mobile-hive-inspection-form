import React from 'react';
import { View, Text } from 'react-native';
import { LighterGrayColor } from '../../beePodsStyles.js';
import { BeepodsFont } from '../../beePodsStyles.js';

const Question = ({ children }) => {
  const { questionContainerStyle, questionTextStyle } = styles;
  return (
    <View style={questionContainerStyle}>
      <Text style={questionTextStyle}>{children}</Text>
    </View>
  );
};

const styles = {
  questionContainerStyle: {
    justifyContent: 'center',
    backgroundColor: LighterGrayColor,
    height: 150,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 5,
    marginBottom: 10
  },
  questionTextStyle: {
    textAlign: 'center',
    fontSize: 21,
    fontFamily: BeepodsFont
  }
};

export { Question };
