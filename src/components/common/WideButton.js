import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import { DarkGrayColor, GrayColor, LightGrayColor, } from '../../beePodsStyles.js';

const WideButton = ({ Action,Title }) => {
    return (
      <View style={styles.action}>
        <TouchableHighlight
          underlayColor={GrayColor}
          onPress={Action}>
          <Text style={styles.actionText}>{Title}</Text>
        </TouchableHighlight>
      </View>
    );
};

const styles = {
    actionText: {
        color: '#f0f0f1',
        fontSize: 16,
        textAlign: 'center',
      },
      action: {
        backgroundColor: GrayColor,
        borderColor: 'transparent',
        borderRadius: 5,
        borderWidth: 1,
        paddingLeft: 16,
        paddingTop: 14,
        paddingBottom: 16,
        margin:10,
      },
}
// was modeuls.export widebutton
export {WideButton};
