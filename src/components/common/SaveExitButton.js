import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { BeepodsFont, PrimaryColor } from '../../beePodsStyles.js';

const SaveExitButton = ({ onPress }) => {
	const { buttonStyle, textStyle } = styles;
	return (
	<TouchableOpacity
		onPress= {()=> {
			onPress;
			Actions.formEnd();
		}}
		style = { [buttonStyle, {width: '100%'}]}
    >
      <Text style={textStyle}>
        Save & Exit
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    borderRadius: 5,
    marginTop: 5,
    margin: 5,
    marginBottom: 5,
    backgroundColor: PrimaryColor,
    justifyContent: 'center'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    //fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: BeepodsFont
  }
};

export { SaveExitButton };