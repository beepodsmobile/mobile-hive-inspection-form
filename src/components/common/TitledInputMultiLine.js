import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { DarkGrayColor, GrayColor, LightGrayColor } from '../../beePodsStyles.js';

const TitledInputMultiLine = ({ label, value, onChangeText,onEndEditing, placeholder, secureTextEntry,maxLength }) => {

    const { inputStyle, labelStyle, containerStyle } = styles;

    return (
        <View style={containerStyle}>
            <Text style={labelStyle}>{label.toUpperCase()}</Text>
            <TextInput
                placeholder={placeholder}
                secureTextEntry={secureTextEntry}
                value={value}
                onChangeText={onChangeText}
                onEndEditing={onEndEditing}
                style={inputStyle}
                maxLength = {maxLength}
                multiline={true}
            />
        </View>
    );
};

const styles = {
    inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        marginBottom: 10,
        color: DarkGrayColor,
        fontSize: 18,
        fontWeight: '200',
        height: 100,
        borderColor: LightGrayColor,
        borderBottomWidth: 1,
    },
    labelStyle: {
        fontSize: 12,
        color: DarkGrayColor,
        fontWeight: '200',
    },
    containerStyle: {
        flexDirection: 'column',
        width: '100%',
    }
};

export {TitledInputMultiLine};
