import React, {Component} from 'react';
import ReactNative from 'react-native';
import {PrimaryColor } from '../../beePodsStyles.js';
const { StyleSheet, Text, View, TouchableOpacity, TouchableHighlight} = ReactNative;

const CircularButton = ({ Action,Title,color }) => {
    return (
      <View >
            <TouchableHighlight
            style={{
                borderWidth:1,
                borderColor:'rgba(0,0,0,0.2)',
                alignItems:'center',
                justifyContent:'center',
                width:100,
                height:100,
                backgroundColor:color,
                borderRadius:100,
                }}
            onPress={()=>Action}
            >

            </TouchableHighlight>
      </View>
    );
};

const styles = {
    actionText: {
        color: '#f0f0f1',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center'
      },
      action: {
        backgroundColor: PrimaryColor,
        borderColor: 'transparent',
        height: 200,
        paddingTop:65,
        marginTop:20,
        marginBottom:20,
        marginLeft:10,
        marginRight:10,
        borderRadius:5,
      },
}

export {CircularButton};