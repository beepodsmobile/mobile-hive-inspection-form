import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {LightGrayColor, BeepodsFont} from '../../beePodsStyles.js';



const Tile = (props) => {
	return (
			<TouchableOpacity 
				style = {[styles.tileStyle, {backgroundColor: props.color}]}
				onPress = {props.onPress}
			>
				<Text style={props.display === "+"?styles.textPlusStyle:styles.textStyle}>{props.display}</Text>
			</TouchableOpacity>		

	);
}

const styles = {
	tileStyle: {
		borderWidth: 2,
		borderRadius: 5,
		padding: 5,
		margin: 25,
		height: 120,
		width: 120,
		alignItems: 'center',
		borderColor: LightGrayColor,
		justifyContent: 'center',
	},
	textStyle: {
		textAlign: 'center',
		fontFamily: BeepodsFont,
		fontSize: 24
	},
	textPlusStyle: {
		textAlign: 'center',
		fontFamily: BeepodsFont,
		fontSize: 75
	}
};

export {Tile};


