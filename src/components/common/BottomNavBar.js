import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation';
import { Actions } from 'react-native-router-flux';
import { BeepodsLogoYellow, BeepodsLogoBlack, BeepodsPaleYellow} from '../../beePodsStyles.js';

class BottomNavBar extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {
	  	activeTab: props.activeTab ,
	  };
	}
  formsSelection(){
    Actions.yardFormSelection();
  }
    
  settingsSelection(){
    Actions.settings();
  }
  yardSelection(){
    Actions.yardSelectionView();
  }
render() {
    return (
      <BottomNavigation
        labelColor={BeepodsLogoYellow}
        style={{
          flex: .1,
          elevation: 8,
          left: 0,
          bottom: 0,
          right: 0
        }}
        activeTab = {this.props.activeTab}  
        rippleColor={BeepodsPaleYellow}

       >
        <Tab
          barBackgroundColor={BeepodsLogoBlack}
          label="Begin Form"
          onPress = {this.formsSelection.bind(this)}
        />
        <Tab
          barBackgroundColor={BeepodsLogoBlack}
          label="Yards"
          onPress = {this.yardSelection.bind(this)}
        />
        <Tab
          barBackgroundColor={BeepodsLogoBlack}
          label="Settings"
          onPress = {this.settingsSelection.bind(this)}
        />
      </BottomNavigation>
    )
  }
}



export {BottomNavBar};
