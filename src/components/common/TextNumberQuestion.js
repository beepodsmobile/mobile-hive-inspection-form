import React, { Component } from 'react';
import { TextInput, View, Slider } from 'react-native';
import { PrimaryColor } from '../../beePodsStyles.js';
import { Button, Question } from '../common';
import { BeepodsFont } from '../../beePodsStyles.js';

class TextNumberQuestion extends Component {

  constructor(props){
    super(props);
    this.state = {
      question: props.question,
      options: props.options,
      buttonText: props.buttonText,
      buttonAction: props.buttonAction,
      value: props.value,
      onValueChange: props.onValueChange,
      buttonAction: props.buttonAction
    };
  }

    render () {
    const { containerStyle, numberContainerStyle, numberTextStyle } = styles;
    return(
            <View style={containerStyle}>
        <Question>
          {this.props.question}
        </Question>
        <View style={numberContainerStyle}>
          <TextInput style={numberTextStyle} keyboardType='numeric' value ={this.props.value} onChangeText= {this.props.onValueChange}/> 
        </View>
        <Button color={PrimaryColor} height={80} onPress={this.props.buttonAction}>{this.props.buttonText}</Button>
            </View>
        );
    }

}

const styles = {
    containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  numberContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  numberTextStyle: {
    fontSize: 72,
    fontFamily: BeepodsFont,
    textAlign: 'center'
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    alignSelf: 'center',
    fontSize: 16
  }
};

export { TextNumberQuestion };
