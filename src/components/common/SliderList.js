import React, {Component} from 'react';
import { StyleSheet, Text, View, Slider, FlatList, ScrollView} from 'react-native';
import { DarkGrayColor, GrayColor, LightGrayColor, PrimaryColor, BeepodsPaleYellow} from '../../beePodsStyles.js';

class SliderList extends Component {

  	constructor(props){
    	super(props);
    	this.state = {
      	options: props.options,
        height: props.height
    	};
  	}
    createContainerStyle(){
    return {
      margin: 15,
      marginTop: 10,
      marginBottom: 0,
      paddingLeft:35,
      paddingRight:35,
      paddingTop:35,
      paddingBottom:35,
      borderRadius: 10,
      backgroundColor: BeepodsPaleYellow,
      height: this.state.height,
      borderBottomWidth: 2,
      borderRightWidth:2,
      borderRightColor: LightGrayColor,
      borderBottomColor: GrayColor
    }
  }
  	render(){
      const {sliderStyle, thumbStyle} = styles;
      return(
  		    <FlatList
              style={this.createContainerStyle()}
              data={this.props.options}
              alwaysBounceVertical={false}
              scrollEnabled={false}
              keyExtractor={(item)=>{item.option}}
              renderItem={({ item }) => (
                	<View>
              	   	 <Text>{item.option}: {item.value}%</Text>
                	   <Slider 
                        thumbStyle={thumbStyle}
                	      onValueChange ={item.action} 
                		    value = {item.value}
						            step = {5}
						            minimumValue = {0}
						            maximumValue = {100} 
						            minimumTrackTintColor = {DarkGrayColor}
						            maximumTrackTintColor = {GrayColor}
                        thumbTintColor = {PrimaryColor}
					           />
                	</View>
              	)}
          	 />
             );
  	}
}
const styles = {
  sliderStyle:{
    height: 20,
    margin: 10,
    borderRadius: 2,
    padding: 10,
    borderWidth: 1,
  },
  thumbStyle:{
    height: 20
  }
};

export {SliderList};