import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableHighlight, Animated, FlatList, Switch, Picker, ScrollView} from 'react-native'; 
import {SliderList, SectionHeader, TitledIntegerInput} from './index';

class ExpandableBarCard extends Component{
    constructor(props){
        super(props);
        this.state  ={
            title: props.title,
            expanded: false,
            animation   : new Animated.Value(35),
            spacerChoices: ['None', 'For Current Bar', 'For Next Bar']

        };
    }
    componentWillMount(){
        this.toggle.bind(this);
    }
    toggle(){
        let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
        finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded : !this.state.expanded  
        });

        this.state.animation.setValue(initialValue);  
        Animated.spring(    
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }
    _setMaxHeight(event){
        this.setState({
            maxHeight   : event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event){
        this.setState({
            minHeight   : event.nativeEvent.layout.height
        });
    }
    handleSliderValueChange(e, selection){
        let honey = this.props.content.ContentsHoney;
        let nectar = this.props.content.ContentsNectar;
        let pollen = this.props.content.ContentsPollen;
        let larva = this.props.content.ContentsLarva;
        let droneBrood = this.props.content.ContentsBroodDrone;
        let workerBrood = this.props.content.ContentsBroodWorker;
        let eggs = this.props.content.ContentsEggs;
        let counter = this.state.counter + 1;
        switch(selection){
            case "comb":
                this.props.barUpdate({ prop: 'CombBuiltOut', value: e})
                break;
            case "honey":
                honey = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsHoney', value: e})
                }
                break;
            case "nectar":
                nectar = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsNectar', value: e})
                }
                break;
            case "pollen":
                pollen = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsPollen', value: e})
                }
                break;
            case "larva":
                larva = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsLarva', value: e})
                }
                break;
            case 'drone':
                droneBrood = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsBroodDrone', value: e})
                }
                break;
            case 'worker':
                workerBrood = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsBroodWorker', value: e})
                } 
                break;          
            case "eggs":
                eggs = e;
                if((honey + nectar + pollen + larva + droneBrood + workerBrood + eggs)> 100){
                    return;
                }
                else{
                    this.props.barUpdate({ prop: 'ContentsEggs', value: e})
                }
                break; 
        }
    }
    render(){
        const pickItems = this.state.spacerChoices.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        });
        const numberOptions=[
            {key:"TeaCupCount", value: this.props.content.TeaCupCount.toString(), action: (e)=>{this.props.barUpdate({prop: "TeaCupCount", value: parseInt(e)})}},
            {key:"CappedQueenCellCount", value: this.props.content.CappedQueenCellCount.toString(), action: (e)=>{this.props.barUpdate({prop: "CappedQueenCellCount", value: parseInt(e)})}},
            {key:"UncappedQueenCellCount", value: this.props.content.UncappedQueenCellCount.toString(), action: (e)=>{this.props.barUpdate({prop: "UncappedQueenCellCount", value: parseInt(e)})}},
            {key:"EmergedQueenCellCount", value: this.props.content.EmergedQueenCellCount.toString(), action: (e)=>{this.props.barUpdate({prop: "EmergedQueenCellCount", value: parseInt(e)})}}
        ];
        const switchOptions = [
            { key: 4, option: 'Spacer?', action: () => {this.props.barUpdate({prop: 'spacer', value: !this.props.content.spacer == true?1:0})}, value: this.props.content.JoinedBar == 1?true:false },
            { key: 1, option: 'Wavy Comb Seen?', action: () => {this.props.barUpdate({prop: 'WavyComb', value: !this.props.content.WavyComb == true?1:0})}, value: this.props.content.WavyComb == 1?true:false },
            { key: 2, option: 'Comb Attached To Wall?', action: () => {this.props.barUpdate({prop: 'AttachedComb', value: !this.props.content.AttachedComb == true?1:0})}, value: this.props.content.AttachedComb == 1?true:false},
            { key: 3, option: 'Follower On This Bar?', action: () => {this.props.barUpdate({prop: 'Follower', value: !this.props.content.Follower == true?1:0})}, value: this.props.content.Follower == 1?true:false},
            { key: 4, option: 'This bar joined with next?', action: () => {this.props.barUpdate({prop: 'JoinedBar', value: !this.props.content.JoinedBar == true?1:0})}, value: this.props.content.JoinedBar == 1?true:false }
        ];
        const sliderOptions = [{option:"Comb Built Out", value: parseFloat(this.props.content.CombBuiltOut), action: (e)=>{this.handleSliderValueChange(e, 'comb')}},
                                {option: "Honey", value: parseFloat(this.props.content.ContentsHoney), action: (e)=>{this.handleSliderValueChange(e, 'honey')}},
                                {option: "Nectar", value: parseFloat(this.props.content.ContentsNectar), action: (e)=>{this.handleSliderValueChange(e, 'nectar')}},
                                {option: "Pollen", value: parseFloat(this.props.content.ContentsPollen), action: (e)=>{this.handleSliderValueChange(e, 'pollen')}},
                                {option: "Eggs", value: parseFloat(this.props.content.ContentsEggs), action: (e)=>{this.handleSliderValueChange(e, 'eggs')}},
                                {option: "Larva", value: parseFloat(this.props.content.ContentsLarva), action: (e)=>{this.handleSliderValueChange(e, 'larva')}},
                                {option: "Drone Brood", value: parseFloat(this.props.content.ContentsBroodDrone), action: (e)=>{this.handleSliderValueChange(e, 'drone')}},
                                {option: "Worker Brood", value: parseFloat(this.props.content.ContentsBroodWorker), action: (e)=>{this.handleSliderValueChange(e, 'worker')}}
                                ];
        return(
            <Animated.View style={[styles.container,{height: this.state.animation}]}>
            <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                <Text style={styles.title}>{this.state.title}</Text>
                <TouchableHighlight 
                    style={styles.button} 
                    onPress={this.toggle.bind(this)}
                    underlayColor="#f1f1f0">
                       <Text> {this.state.expanded? "close" : "open"}</Text>
                </TouchableHighlight>
            </View>
                
            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                <SectionHeader>Comb Contents</SectionHeader>
                <SliderList options = {sliderOptions} height={550}/>
                <SectionHeader>Details</SectionHeader>
                <View>
                    <FlatList
                        data={switchOptions}
                        alwaysBounceVertical={false}
                        keyExtractor={item=>item.option}
                        renderItem={({ item }) => (
                            <View style={styles.listItem}>
                                <Text style={styles.listText}>{item.option}</Text>
                                <Switch
                                    onValueChange={item.action}
                                    value={item.value}
                                />
                            </View>
                        )}
                    />
                    <FlatList
                        data={numberOptions}
                        alwaysBounceVertical={false}
                        keyExtractor={item=>item.key}
                        renderItem={({item})=>(
                            <TitledIntegerInput label={item.key} value={item.value} onChangeText={item.action}/>
                        )}
                    />
                </View> 
            </View>
        </Animated.View>
        );
    }
}

export {ExpandableBarCard};

const styles = StyleSheet.create({
    container   : {
        backgroundColor: '#fff',
        margin:10,
        overflow:'hidden'
    },
    titleContainer : {
        flexDirection: 'row'
    },
    title       : {
        flex    : 1,
        padding : 10,
        color   :'#2a2f43',
        fontWeight:'bold'
    },
    button      : {

    },
    buttonImage : {
        width   : 30,
        height  : 25
    },
    body        : {
        padding     : 10,
        paddingTop  : 0
    },
    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,
        marginBottom: 5
    },
    listText: {
        alignSelf: 'center',
        fontSize: 16
    }
});