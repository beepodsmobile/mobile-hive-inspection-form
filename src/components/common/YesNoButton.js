import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import {PrimaryColor } from '../../beePodsStyles.js';

const YesNoButton = ({ Action,Title }) => {
    return (
      <View style={styles.action}>
        <TouchableHighlight
          underlayColor={'#4F80E1'}
          onPress={Action}>
          <Text style={styles.actionText}>{Title}</Text>
        </TouchableHighlight>
      </View>
    );
};

const styles = {
    actionText: {
        color: '#f0f0f1',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center'
      },
      action: {
        backgroundColor: PrimaryColor,
        borderColor: 'transparent',
        flex:3,
        paddingTop:30,
        marginBottom:30,
        marginLeft:10,
        marginRight:10,
        borderRadius:5,
      },
}

export {YesNoButton};