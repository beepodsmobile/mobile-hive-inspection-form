import React, { Component } from 'react';
import { Text, View, Slider } from 'react-native';
import { PrimaryColor } from '../../beePodsStyles.js';
import { Button, Question } from '../common';
import { BeepodsFont } from '../../beePodsStyles.js';

class NumberQuestion extends Component {

  constructor(props){
    super(props);
    this.state = {
      question: props.question,
      options: props.options,
      buttonText: props.buttonText,
      buttonAction: props.buttonAction,
      value: props.value,
      minimumValue: props.minimumValue,
      maximumValue: props.maximumValue,
      step: props.step,
      onValueChange: props.onValueChange,
      buttonAction: props.buttonAction
    };
  }

	render () {
    const { containerStyle, numberContainerStyle, numberTextStyle } = styles;
    return(
			<View style={containerStyle}>
        <Question>
          {this.props.question}
        </Question>
        <View style={numberContainerStyle}>
          <Text style={numberTextStyle}>{this.props.value}</Text>
          <Slider
            minimumValue={this.props.minimumValue}
            maximumValue={this.props.maximumValue}
            step={this.props.step}
            value={this.props.value}
            onValueChange={this.props.onValueChange}
            minimumTrackTintColor={PrimaryColor}
          />
        </View>
        <Button color={PrimaryColor} height={80} onPress={this.props.buttonAction}>{this.props.buttonText}</Button>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  numberContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  numberTextStyle: {
    fontSize: 72,
    fontFamily: BeepodsFont,
    textAlign: 'center'
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    alignSelf: 'center',
    fontSize: 16
  }
};

export { NumberQuestion };
