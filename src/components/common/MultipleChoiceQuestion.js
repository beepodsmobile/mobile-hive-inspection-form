import React, { Component } from 'react';
import { View, ScrollView, FlatList } from 'react-native';
import { PrimaryColor } from '../../beePodsStyles.js';
import { Button, Question } from '../common';

class MultipleChoiceQuestion extends Component {

  constructor(props){
    super(props);
    this.state = {
      question: props.question,
      options: props.options
    };
  }

	render () {
    const { containerStyle, optionsContainerStyle, listItem } = styles;
    return(
			<View style={containerStyle}>
        <Question>
          {this.props.question}
        </Question>
        <ScrollView style={optionsContainerStyle}>
          <FlatList
            data={this.props.options}
            alwaysBounceVertical={false}
            renderItem={({ item }) => (
              <View style={listItem}>
                <Button color={PrimaryColor} onPress={item.action} height={item.buttonHeight}>{item.option}</Button>
              </View>
            )}
          />
        </ScrollView>
			</View>
		);
	}

}

const styles = {
	containerStyle: {
    flex: 1,
    paddingLeft: 20,
    alignItems: 'center',
    paddingRight: 20
  },
  optionsContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  }
};

export { MultipleChoiceQuestion };
///this is broken somehow