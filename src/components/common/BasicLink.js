import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { PrimaryColor } from '../../beePodsStyles.js';

const BasicLink = ({ Action,Title }) => {
    return (
      <View style={styles.action}>
        <Text
            onPress= {()=> {Action}}
            style = {styles.TextStyle}
        >
            {Title}
        </Text>
      </View>
    );
};

const styles = {
    TextStyle: {
        color: PrimaryColor,
        fontSize: 14,
        textAlign: 'center',
      },
    action: {
        //bottom margin mostly for the login page
        marginBottom:8,
    }
}


export {BasicLink} ;
