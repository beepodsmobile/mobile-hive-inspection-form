import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import PrimaryColor from '../../beePodsStyles.js';

const LargeButton = ({ Action,Title }) => {

    return (
      <View style={styles.action}>
        <TouchableHighlight
          underlayColor={'#4F80E1'}
          onPress={Action}>
          <Text style={styles.actionText}>{Title}</Text>
        </TouchableHighlight>
      </View>
    );
};

const styles = {
    actionText: {
        color: '#f0f0f1',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center'
      },
      action: {
        backgroundColor: PrimaryColor,
        borderColor: 'transparent',
        height: 200,
        paddingTop:65,
        marginTop:20,
        marginBottom:20,
        marginLeft:10,
        marginRight:10,
        borderRadius:5,
      },
}

//was widebutton
export {LargeButton};