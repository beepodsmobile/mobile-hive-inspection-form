import React, {Component} from 'react';
import {Text, TouchableOpacity, View, TouchableHighlight} from 'react-native';
import {LightGrayColor, BeepodsFont, PrimaryColor} from '../../beePodsStyles.js';

class ColorButton extends Component {
	constructor(props){
    	super(props);
    	this.state = {
    		pressed: false
  	}
  }
  	componentWillReceiveProps(nextProps){

  			this.setState({pressed: nextProps.coco})
  	}

  	
	render(){
	return (
      <View >
        <TouchableOpacity onPress = {this.props.action} style={[styles.unselectedStyle, this.state.pressed && styles.selectedStyle, {backgroundColor: this.props.hue}]}>

        </TouchableOpacity>
      </View>
    );
}
}

const styles = {
	unselectedStyle: {
		borderWidth: 2,
		borderRadius: 5,
		padding: 5,
		margin: 5,
		position: 'relative',
		height: 125,
		width: 125,
		alignItems: 'center',
		//borderColor: LightGrayColor,
		justifyContent: 'space-around',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:100,
	},
	selectedStyle: {
		borderWidth: 10,
		borderRadius: 5,
		borderColor: PrimaryColor,
		padding: 5,
		margin: 5,
		position: 'relative',
		height: 125,
		width: 125,
		alignItems: 'center',
		//borderColor: LightGrayColor,
		justifyContent: 'space-around',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:100,
	}
 };

 export {ColorButton};