import React from 'react';
import {View} from 'react-native';
import {DisplayTiles, Button, Header} from '../common';
import { PrimaryColor, BeepodsGreen } from '../../beePodsStyles.js';

//TODO: Add Header and footer components
const YardSelectionView = (props) => {
	return (

			<View>
				<Header headerText = 'Yard Selection'/>
				<Button color = {PrimaryColor}>
					Settings
				</Button>
				<DisplayTiles tileColor = {BeepodsGreen} />		
			</View>
		);
}



export default YardSelectionView;