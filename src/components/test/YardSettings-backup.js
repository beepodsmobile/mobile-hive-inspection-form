import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, ScrollView, Picker } from 'react-native';
import { ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput, TitledInputMultiLine } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';
import axios from 'axios';

class YardSettings extends Component {
  constructor(props){
    super(props);

    this.states = [
      ['Arizona', 'AZ'],
      ['Alabama', 'AL'],
      ['Alaska', 'AK'],
      ['Arkansas', 'AR'],
      ['California', 'CA'],
      ['Colorado', 'CO'],
      ['Connecticut', 'CT'],
      ['Delaware', 'DE'],
      ['Florida', 'FL'],
      ['Georgia', 'GA'],
      ['Hawaii', 'HI'],
      ['Idaho', 'ID'],
      ['Illinois', 'IL'],
      ['Indiana', 'IN'],
      ['Iowa', 'IA'],
      ['Kansas', 'KS'],
      ['Kentucky', 'KY'],
      ['Louisiana', 'LA'],
      ['Maine', 'ME'],
      ['Maryland', 'MD'],
      ['Massachusetts', 'MA'],
      ['Michigan', 'MI'],
      ['Minnesota', 'MN'],
      ['Mississippi', 'MS'],
      ['Missouri', 'MO'],
      ['Montana', 'MT'],
      ['Nebraska', 'NE'],
      ['Nevada', 'NV'],
      ['New Hampshire', 'NH'],
      ['New Jersey', 'NJ'],
      ['New Mexico', 'NM'],
      ['New York', 'NY'],
      ['North Carolina', 'NC'],
      ['North Dakota', 'ND'],
      ['Ohio', 'OH'],
      ['Oklahoma', 'OK'],
      ['Oregon', 'OR'],
      ['Pennsylvania', 'PA'],
      ['Rhode Island', 'RI'],
      ['South Carolina', 'SC'],
      ['South Dakota', 'SD'],
      ['Tennessee', 'TN'],
      ['Texas', 'TX'],
      ['Utah', 'UT'],
      ['Vermont', 'VT'],
      ['Virginia', 'VA'],
      ['Washington', 'WA'],
      ['West Virginia', 'WV'],
      ['Wisconsin', 'WI'],
      ['Wyoming', 'WY'],
    ];

    this.state = {
      yardName: null,
      latitude: null,
      longitude: null,
      error: null,
      showErrors: false,
      Address1: null,
      City: null,
      State: 'AR',
      ZipCode: null
    };

  }

  getGoogleMapsGeolocationFromAdress(){
    this.setState({
      latitude: null,
      longitude: null,
      error: null,
      showErrors: false
    });
    let APICall = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    let googleMapsAPIKey = "&key=AIzaSyAU4pj1i5c905yjhgWE15V1SbT-jOgRaMg";
    let errorMessages = [];
    if ( this.state.yardName == null ) {
      errorMessages.push('You must provide a yard name.');
    }
    if ( this.state.Address1 == null ) {
      errorMessages.push('You must provide an address.');
    }
    if ( this.state.City == null ) {
      errorMessages.push('You must provide a city.');
    }
    if ( this.state.ZipCode == null ) {
      errorMessages.push('You must provide a ZipCode.');
    }
    if ( errorMessages.length > 0 ) {
      let errors = '';
      for ( let i = 0; i < errorMessages.length; i++ ) {
        errors += errorMessages[i];
        if ( i < errorMessages.length-1 ) {
          errors += '\n';
        }
      }
      this.setState({ error: errors, showErrors: true });
    } else {
      this.setState({ error: null, showErrors: false });
      APICall += this.cleanCode(this.state.Address1) + '+';
      APICall += this.cleanCode(this.state.City) + '+';
      APICall += this.state.State + '+';
      APICall += this.cleanCode(this.state.ZipCode) + '+';
      APICall += googleMapsAPIKey;
      this.getLocation(APICall);
    }
  }

  getLocation(APICall) {
    axios.get(APICall)
      .then(response => {
        let results = response.data.results[0];
        if ( results == null ) {
          this.setState({ error: 'No address found.', showErrors: true });
        } else {
          let location = results.geometry.location;
          lat = location.lat;
          lng = location.lng;
          this.setState({ latitude: lat });
          this.setState({ longitude: lng });
        }
      });
  }

  getErrorMessages() {
    const { errorViewStyle, errorTextStyle } = styles;
    if(this.state.showErrors) {
      return(
        <View style={errorViewStyle}>
          <Text style={errorTextStyle}>{ this.state.error }</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  cleanCode(c) {
      return c.replace(/[^A-Za-z0-9_]/g, "");
  }

  render() {
    let stateItems = this.states.map( (s, i) => {
      return <Picker.Item key={i} value={s[1]} label={s[0]} />
    });

    const { containerStyle, insetContainerStyle, labelStyle } = styles;
    return (
      <ScrollView style={containerStyle}>
        <SectionHeader>Yard Settings</SectionHeader>
        <View style={insetContainerStyle}>
          <TitledInput
            label='Yard Name'
            placeholder='enter your yard name'
            value={this.state.yardName}
            onChangeText={yardName => this.setState({ yardName })}
          />
          <TitledInput
            label='Address'
            value={this.state.Address1}
            onChangeText={Address1 => this.setState({ Address1 })}
          />
          <TitledInput
            label='City'
            value={this.state.City}
            onChangeText={City => this.setState({City})}
          />
          <TitledInput
            label='Zip Code'
            value={this.state.ZipCode}
            onChangeText={ZipCode => this.setState({ZipCode})}
            //maxLength={5}
          />
          <Text style={labelStyle}>STATE</Text>
          <Picker
            selectedValue = {this.state.State}
            onValueChange = {itemValue => this.setState({State: itemValue})}
          >
          {stateItems}
          </Picker>
          <Button color={PrimaryColor} onPress={this.getGoogleMapsGeolocationFromAdress.bind(this)}>Save Yard Settings</Button>
          {this.getErrorMessages()}
          <Text>Latitude: {this.state.latitude}</Text>
          <Text>Longitude: {this.state.longitude}</Text>
        </View>
        <SectionHeader>Hives in Yard</SectionHeader>
        <View style={insetContainerStyle}>
          <ManagedList
            data={[
              {key: 'hive number one'},
              {key: 'hive number two'},
              {key: 'hive number three'},
              {key: 'hive number four'},
              {key: 'hive number five'}
            ]}
            addButtonText="Add Hive"
            addButtonOnPress={() => {console.log('add hive button pressed')}}
            removeButtonOnPress={() => {console.log('remove hive button pressed')}}
          />
        </View>
        <SectionHeader>Yard Users</SectionHeader>
        <View style={insetContainerStyle}>
          <ManagedList
            data={[
              {key: 'nathanrogersofficial'},
              {key: 'David Merkel'},
              {key: 'Joe K'},
              {key: 'Aaron Edelman'},
              {key: 'Rick Kippert'},
            ]}
            addButtonText="Add User"
            addButtonOnPress={() => {console.log('add user button pressed')}}
            removeButtonOnPress={() => {console.log('remove user button pressed')}}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    marginTop: 40
  },
  insetContainerStyle: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  labelStyle: {
    fontSize: 12,
    color: DarkGrayColor,
    fontWeight: '200',
    flex: 1
  },
  errorViewStyle: {
    backgroundColor: DangerColor,
    borderRadius: 5,
    padding: 10,
    marginBottom: 10
  },
  errorTextStyle: {
    color: '#fff'
  }
});

export default YardSettings;
