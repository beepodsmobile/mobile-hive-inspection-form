import React from 'react';
import { StyleSheet, Text, View, Button,Image,Linking } from 'react-native';
import TitledInput from '../common/TitledInput.js';
import WideButton from '../common/WideButton.js';
import BasicLink from '../common/BasicLink.js';

export default class LoginForm extends React.Component {
  constructor(props){
    super(props);
    this.state = { email: '', password: '',error: '', loading:false };
  }

  LogIn(){
  }

  onEmailChange(text){
    this.props.emailChanged(text);
  }
  onPasswordChange ( text ){
    this.props.passwordChanged(text);
  }
  renderButton(){
    if (this.props.loading){
      return <Spinner size = "large" />
    }
    return (
      <WideButton Title="Login" Action={this.onButtonPress.bind(this)} />
      <Button onPress = {this.onButtonPress.bind(this)}>
        Login
      </Button>
    );
  }
  onButtonPress (){ 
    const {email, password} = this.props;
    this.props.loginUser({ email, password});
  }
  render() {
    const signUpURL = "https://www.beepods.com/my-account/";
    const forgotPassword = "https://www.beepods.com/my-account/lost-password/";
    
    return (
      <View style={styles.container}>
        <View style= {styles.image}>
          <Image
           source={require('../../img/beepods_tagline_logo-LARGE.png')}
           style = {{height:200, width: 350}}
          />
        </View>
        <Text style={styles.errorTextStyle}>{this.state.error}</Text>
        <View style ={styles.loginForm}>
          <TitledInput 
            label='Email Address'
            placeholder='you@domain.com'
            onChangeText = {this.onEmailChange.bind(this)}
            value = {this.props.email}
          />
          <TitledInput 
            label='Password'
            autoCorrect={false}
            placeholder='*******'
            secureTextEntry
            onChangeText = {this.onPasswordChange.bind(this)}
            value = {this.props.password}
          />
          {this.renderButton()}
        </View>

        <View style = {styles.links}>
          <BasicLink
            Title="Forgot your password?"
            Action={()=> Linking.openURL(forgotPassword)}
          />
          <BasicLink
            Title="Sign up"
            Action={()=> Linking.openURL(signUpURL)}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:20,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  image:{
    flex:4,
    alignItems: 'center'
  },
  loginForm:{
    flex:3,
    padding:10,
    marginBottom:10,
    justifyContent: 'center'
  },
  links:{
    flex:1
  },
  errorTextStyle: {
    color: '#E64A19',
    alignSelf: 'center',

}

});
