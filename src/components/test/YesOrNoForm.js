import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { YesNoButton, Question } from '../common';
import { DarkGrayColor} from '../../beePodsStyles';

const YesOrNoForm = ({Question, YesAction, NoAction }) => { {
    return (
        <View style = {styles.container}>
            <View style={styles.containerStyle}>
                <Text style={styles.labelStyle}>{Question}</Text>
            </View>
            <YesNoButton
                Title= "Yes"
                Action = {YesAction}
            />
            <View style = {styles.flexSpacer}/>
            <YesNoButton
                Title= "No"
                Action = {NoAction}
            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:2,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  labelStyle: {
    paddingTop:15,
    fontSize: 24,
    color: DarkGrayColor,
    fontWeight: 'bold',
},
containerStyle: {
    flex:4,
    alignItems: 'center',
    justifyContent: 'center',
},
flexSpacer:{
    flex:1
}
});

export default YesOrNoForm;