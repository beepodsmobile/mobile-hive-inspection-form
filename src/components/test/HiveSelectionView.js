import React from 'react';
import {View} from 'react-native';
import {DisplayTiles, Button, Header} from '../common';
import { PrimaryColor, DangerColor, BeepodsLogoYellow } from '../../beePodsStyles.js';

//TODO: Add Header and footer components
const HiveSelectionView = (props) => {
	return (

			<View>
				<Header headerText = 'Hive Selection'/>
				<Button color = {PrimaryColor}>
					Settings
				</Button>
				<DisplayTiles tileColor = {BeepodsLogoYellow} />		
			</View>
		);
}



export default HiveSelectionView;