import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import LargeButton from '../common/LargeButton.js';

export default class HiveSelectorForm extends React.Component {

  constructor(props){
    super(props);
  }

  DoAThing(){
    console.log('OH WOW');
  }

  //TO DO! Generate buttons using a loop when props can be passed
  render() {
    return (
      <ScrollView>
          <LargeButton
            Title = 'Hive 1'
            Action = {this.DoAThing}
          />
          <LargeButton
            Title = 'Hive 1'
            Action = {this.DoAThing}
          />
          <LargeButton
            Title = 'Hive 1'
            Action = {this.DoAThing}
          />
          <LargeButton
            Title = 'Hive 1'
            Action = {this.DoAThing}
          />
          <LargeButton
            Title = 'Hive 1'
            Action = {this.DoAThing}
          />

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});