import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, ScrollView } from 'react-native';
import { ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput, TitledInputMultiLine } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';

class HiveSettings extends Component {
  constructor(props){
    super(props);
    this.state = { hiveName: '', hiveDescription: '' };
  }

  render() {
    const { containerStyle, insetContainerStyle, footerViewStyle, footerTextStyle } = styles;
    return (
      <ScrollView style={containerStyle}>
        <SectionHeader>Hive Settings</SectionHeader>
        <View style={insetContainerStyle}>
          <TitledInput
            label='Hive Name'
            placeholder='enter your hive name'
            value={this.state.hiveName}
            onChangeText={hiveName => this.setState({ hiveName })}
          />
          <TitledInputMultiLine
            label='Hive Description'
            placeholder='enter your hive description'
            value={this.state.hiveDescription}
            onChangeText={hiveDescription => this.setState({ hiveDescription })}
          />
          <Button color={PrimaryColor}>Save Hive Settings</Button>
        </View>
        <SectionHeader>Hive Users</SectionHeader>
        <View style={insetContainerStyle}>
          <ManagedList
            data={[
              {key: 'nathanrogersofficial'},
              {key: 'David Merkel'},
              {key: 'Joe K'},
              {key: 'Aaron Edelman'},
              {key: 'Rick Kippert'},
              {key: 'nathanrogersofficial2'},
              {key: 'David Merkel2'},
              {key: 'Joe K2'},
              {key: 'Aaron Edelman2'},
              {key: 'Rick Kippert2'}
            ]}
            addButtonText="Add User"
            addButtonOnPress={() => {console.log('add button pressed')}}
            removeButtonOnPress={() => {console.log('remove button pressed')}}
          />
          <HorizontalRule />
          <View style={footerViewStyle}>
            <Text style={footerTextStyle}>Hive Serial #:</Text>
            <Text style={footerTextStyle}>XXXXXXXXXXXXXXXXXX</Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    marginTop: 40
  },
  insetContainerStyle: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  footerViewStyle: {
    marginTop: 20,
    marginBottom: 20
  },
  footerTextStyle: {
    color: GrayColor,
    textAlign: 'center'
  }
});

export default HiveSettings;
