
import React, { Component } from 'react';
import { View, Text,Picker } from 'react-native';
import { TitledInput, WideButton} from '../common';

class Geolocation extends Component {
  constructor(props) {
    super(props);

    googleMapsURL = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    googleMapsAPIKey = "&key=AIzaSyAU4pj1i5c905yjhgWE15V1SbT-jOgRaMg";

      this.states = [
      ['Arizona', 'AZ'],
      ['Alabama', 'AL'],
      ['Alaska', 'AK'],
      ['Arkansas', 'AR'],
      ['California', 'CA'],
      ['Colorado', 'CO'],
      ['Connecticut', 'CT'],
      ['Delaware', 'DE'],
      ['Florida', 'FL'],
      ['Georgia', 'GA'],
      ['Hawaii', 'HI'],
      ['Idaho', 'ID'],
      ['Illinois', 'IL'],
      ['Indiana', 'IN'],
      ['Iowa', 'IA'],
      ['Kansas', 'KS'],
      ['Kentucky', 'KY'],
      ['Louisiana', 'LA'],
      ['Maine', 'ME'],
      ['Maryland', 'MD'],
      ['Massachusetts', 'MA'],
      ['Michigan', 'MI'],
      ['Minnesota', 'MN'],
      ['Mississippi', 'MS'],
      ['Missouri', 'MO'],
      ['Montana', 'MT'],
      ['Nebraska', 'NE'],
      ['Nevada', 'NV'],
      ['New Hampshire', 'NH'],
      ['New Jersey', 'NJ'],
      ['New Mexico', 'NM'],
      ['New York', 'NY'],
      ['North Carolina', 'NC'],
      ['North Dakota', 'ND'],
      ['Ohio', 'OH'],
      ['Oklahoma', 'OK'],
      ['Oregon', 'OR'],
      ['Pennsylvania', 'PA'],
      ['Rhode Island', 'RI'],
      ['South Carolina', 'SC'],
      ['South Dakota', 'SD'],
      ['Tennessee', 'TN'],
      ['Texas', 'TX'],
      ['Utah', 'UT'],
      ['Vermont', 'VT'],
      ['Virginia', 'VA'],
      ['Washington', 'WA'],
      ['West Virginia', 'WV'],
      ['Wisconsin', 'WI'],
      ['Wyoming', 'WY'],
  ];

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
      Address1: null,
      Address2: null,
      City: null,
      State: 'AZ',
      ZipCode: null,
    };
  }

  getGoogleMapsGeolocationFromAdress(){
    let APICall = this.googleMapsURL;
    APICall += this.state.Address1 + '+';
    if(this.state.Address2 == null){
      APICall += this.state.Address2 + '+';
    }
    APICall += this.state.City + '+';
    APICall += this.state.State + '+';
    APICall += this.state.ZipCode+ '+';
    APICall += this.googleMapsApiKey;
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  render() {
    
    let stateItems = this.states.map( (s, i) => {
      return <Picker.Item key={i} value={s[1]} label={s[0]} />
    }); 

    return (
      <View style={styles.container}>
          <TitledInput 
            label='Address'
            value={this.state.Address1}
            onChangeText={Address1 => this.setState({ Address1 })}
          />
          <TitledInput 
            label='City'
            value={this.state.City}
            onChangeText={City => this.setState({City})}
          />
          <TitledInput 
            label='Zip Code'
            value={this.state.ZipCode}
            onChangeText={ZipCode => this.setState({ZipCode})}
            maxLength = '5'
          /> 
          <Text style={styles.labelStyle}>State</Text>         
          <Picker
            selectedValue = {this.state.State}
            onValueChange = {itemValue => this.setState({State: itemValue})}
          >
          {stateItems}
          </Picker> 

          <WideButton Title="Log in" Action={this.getGoogleMapsGeolocationFromAdress()} />
        <Text>Latitude: {this.state.latitude}</Text>
        <Text>Longitude: {this.state.longitude}</Text>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    padding:20,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  labelStyle: {
    fontSize: 12,
    color: "#303030",
    fontWeight: '200',
    flex: 1
},
}
export default Geolocation;