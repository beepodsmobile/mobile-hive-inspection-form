
import React, {Component} from 'react';
import { Text, View } from 'react-native';
import axios from 'axios';

class Weather extends Component {
	constructor(props) {
    super(props);

		openWeatherURL = 'http://api.openweathermap.org/data/2.5/weather?';
		openWeatherAPIKey = 'cea53ed56c3ee5fc14e6d1120b4e9e9d';


		lat = 40; //replace with real number for view to work.
		lon = 40;

		this.state = {
			Weather: {
				main: {
					temp: '',
					pressure: '',
					humidity: ''
				}
			}
		 };
	}
		getWeather(){
			let APICall = openWeatherURL;
			APICall += 'lat=' + lat + '&lon=' + lon;
			APICall += '&APPID=' + openWeatherAPIKey;
			console.log(APICall);
			axios.get(APICall)
			.then(response => this.setState( {
				Weather: response.data
			}));
		}

		componentDidMount(){
			this.getWeather();
		}

		render () {
		return(
			<View>
				<Text>{this.state.Weather.main.temp -273.15 }</Text>
				<Text>{this.state.Weather.main.pressure}</Text>
				<Text>{this.state.Weather.main.humidity}</Text>
			</View>
		);
	}
}

export default Weather;

