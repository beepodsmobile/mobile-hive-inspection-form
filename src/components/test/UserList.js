import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { ListButton, Button } from '../common';
import { PrimaryColor, DangerColor, LightGrayColor } from '../../beePodsStyles.js';

const UserList = ({ data }) => {
  return (
    <View style={styles.container}>
      <Button color={PrimaryColor} onPress={() => {console.log('pressed')}}>
        Add User
      </Button>
      <FlatList
        data={data}
        alwaysBounceVertical={false}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text style={styles.listText}>{item.key}</Text>
            <ListButton onPress={() => {console.log('pressed')}} color={DangerColor}>Remove</ListButton>
          </View>
        )}
        ItemSeparatorComponent={this.renderSeparator}
      />
    </View>
  );
}

renderSeparator = () => {
  return (
    <View
      style={{
        height: 1,
        backgroundColor: LightGrayColor,
      }}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    alignSelf: 'center',
    fontSize: 16
  }
});

export default UserList;
