import React, {Component} from 'react';
import { StyleSheet, Text, View, Slider } from 'react-native';
import { SliderList, WideButton} from '../common';
import Question from '../common/Question.js';

class BarSliderForm extends Component{
	constructor(props) {
    	super(props);
    	this.state = {
    		combBuiltOut: 0,
    		honey: 0,
    		nectar: 0,
    		pollen: 0,
    		larva: 0,
    		droneBrood: 0,
    		workerBrood: 0
    	}
	}

	ComponentDidMount(){

	}
	adjustComb(value){
		this.setState({
			combBuiltOut: value
			});
	}
	handleSliderValueChange(e, selection){
		let honey = this.state.honey;
		let nectar = this.state.nectar;
		let pollen = this.state.pollen;
		let larva = this.state.larva;
		let droneBrood = this.state.droneBrood;
		let workerBrood = this.state.workerBrood;

		switch(selection){
			case "comb":
				this.adjustComb(e);
				break;
			case "honey":
				honey = e;
				if((honey + nectar + pollen + larva + droneBrood + workerBrood)> 100){
					return;
				}
				break;
			case "nectar":
				nectar = e;
				if((honey + nectar + pollen + larva + droneBrood + workerBrood)> 100){
					return;
				}
				break;
			case "pollen":
				pollen = e;
				if((honey + nectar + pollen + larva + droneBrood + workerBrood)> 100){
					return;
				}
				break;
			case "larva":
				larva = e;
				if((honey + nectar + pollen + larva + droneBrood + workerBrood)> 100){
					return;
				}
				break;
			case "droneBrood":
				droneBrood = e;
				if((honey + nectar + pollen + larva + droneBrood + workerBrood)> 100){
					return;
				}
				break;
			case "workerBrood":
				workerBrood = e;
				if((honey + nectar + pollen + larva + droneBrood + workerBrood)> 100){
					return;
				}
				break;
		}
		this.setState({
			honey: honey,
    		nectar: nectar,
    		pollen: pollen,
    		larva: larva,
    		droneBrood: droneBrood,
    		workerBrood: workerBrood
		});
	}
	handleSliderComplete(){
		let honey = this.state.honey;
		let nectar = this.state.nectar;
		let pollen = this.state.pollen;
		let larva = this.state.larva;
		let droneBrood = this.state.droneBrood;
		let workerBrood = this.state.workerBrood;
		console.log(honey, nectar, pollen, larva, droneBrood, workerBrood);
		this.setState({
			honey: 100,
    		nectar: 100,
    		pollen: 100,
    		larva: 100,
    		droneBrood: 100,
    		workerBrood: 100
		});
	}

	render(){
		const options = [
			{ key: 0, option: 'Comb Built Out', value: this.state.combBuiltOut, action: (e) => {this.handleSliderValueChange(e, "comb")}},
      		{ key: 1, option: 'Honey', value: this.state.honey, action: (e) => {this.handleSliderValueChange(e, "honey")}},
      		{ key: 2, option: 'Nectar', value: this.state.nectar, action:(e) => {this.handleSliderValueChange(e, "nectar")}},
      		{ key: 3, option: 'Pollen', value: this.state.pollen, action: (e) => {this.handleSliderValueChange(e, "pollen")} },
      		{ key: 4, option: 'Larva', value: this.state.larva, action: (e) => {this.handleSliderValueChange(e, "larva")}},
      		{ key: 5, option: 'Drone Brood', value: this.state.droneBrood, action: (e) => {this.handleSliderValueChange(e, "droneBrood")}},
      		{ key: 6, option: 'Worker Brood', value: this.state.workerBrood, action: (e) => {this.handleSliderValueChange(e, "workerBrood")}}];
		return(
			<View>
				<SliderList
					options = {options}
					height = {400}
				/>
			</View>
		);
	}
}
export default BarSliderForm;