import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CircularButton from '../common/CircularButton.js';
import OtherCircularButton from '../common/OtherCircularButton.js';
import Question from '../common/Question.js';
import { DarkGrayColor} from '../../beePodsStyles.js';

const ColorPickerForm = ({Question,CircleAction}) => { {
    return (
        <View style = {styles.container}>
            <View style={styles.containerStyle}>
                <Text style={styles.labelStyle}>{Question}</Text>
            </View>

            <View style= {styles.CircleButtonRow}>
                <View style = {styles.CircleButton}>
                    <CircularButton
                        action = {CircleAction}
                        color = '#f9c901'
                    />
                </View>
                <View style = {styles.CircleButton}>
                    <CircularButton
                        action = {CircleAction}
                        color = '#f6e000'
                    />
                </View>
            </View>

            <View style= {styles.CircleButtonRow}>
                <View style = {styles.CircleButton}>
                    <CircularButton
                        action = {CircleAction}
                        color = '#985b10'
                    />
                </View>
                <View style = {styles.CircleButton}>
                    <CircularButton
                        action = {CircleAction}
                        color = '#896800'
                    />
                </View>
            </View>

            <View style= {styles.CircleButtonRow}>
                <View style = {styles.CircleButton}>
                    <CircularButton
                        action = {CircleAction}
                        color = '#896800'
                    />'
                </View>
                <View style = {styles.CircleButton}>
                    <OtherCircularButton
                        action = {CircleAction}
                    />
                </View>
            </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  labelStyle: {
    paddingTop:15,
    fontSize: 24,
    color: DarkGrayColor,
    fontWeight: 'bold',
},
containerStyle: {
    flex:2,
    alignItems: 'center',
    justifyContent: 'center',
},
CircleButtonRow:{
    flexDirection:'row',
    flex:2,
},
CircleButton:{
    flex:3,
    alignItems: 'center',
    justifyContent: 'center'
}

});

export default ColorPickerForm;