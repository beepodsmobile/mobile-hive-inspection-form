import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Flatlist, ActivityIndicator } from 'react-native';
import { Actions } from 'reacte-native-router-flux';
import {setHive } from '../../actions';
import {List} from 'react-native-elements';
import { LargeButton } from '../common';

class HiveFormSelection extends Component {

	componentWillMount(){
		//put in call for hives here
	};

render(){

		renderFooter = () => {
		 	return(
		 		<LargeButton
		     		Title = "Add Hive"
		     		Action = {()=> {Actions.addHiveForm()}}
		    	   />
		 	)
		}
		
		renderSeperator = () => {
			return(
		 		<View
			 		style = {{
			 			//style here for space between
			 		}}
		 		/>
		 	)
		}

		return(
			<List>
				<Flatlist
					data = {this.props.data}
					renderItem = {{ item } => {
						//set item here
						<LargeButton
        	   				Title = {this.item.hiveName}
        	   				Action = {()=> {this.props.setHive({this.item.hiveInfo})}}
        	 			/>
					}}
				keyExtractor = {item => item.hiveKey}
				ItemSeperatorComponenet = {this.renderSeperator}
				ListFooterComponent = {this.renderFooter}

				/>
			</List>

			)
	}

}

const mapStateToProps = (state) => {
	const {
		data,
} = state.form;
	return {
		data
	};


export default connect( mapStateToProps, {setHive})(HiveFormSelection);