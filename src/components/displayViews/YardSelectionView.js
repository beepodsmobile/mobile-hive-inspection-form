import React, {Component} from 'react';
import {View} from 'react-native';
import { connect } from 'react-redux';
import {BottomNavBar, DisplayTiles, Button, Header, Spinner, SectionHeader} from '../common';
import axios from 'axios';
import { getYard, addYard, loadYards} from '../../actions';
import { PrimaryColor, BeepodsGraySix } from '../../beePodsStyles.js';

//TODO: Add Header and footer components
class YardSelectionView extends Component {

    constructor(props){
        super(props);
        this.makeCall();
        this.state = {
            data: [{'Email': "fail"}]
        }
    }

    componentWillReceiveProps(){
        this.makeCall();
    }
    makeCall(){
        let yardCall = `https://www.beepods.com/wp-json/inspection/v1/useryards/?id=${this.props.user['id']}`;
        let placeholder = {hello: "hello"}; 
        let config = {"Cache-Control" : "no-cache, no-store, must-revalidate"};
        axios.post(yardCall, placeholder, config)
            .then((response) => {
                const data = response['data'];
                this.props.loadYards({data});
                let yardOptions = response['data'].map(item =>{
                   let option = { };
                   option.key = parseInt(item['YardId']);
                   option.display = item['YardName'];
                   const yardId = item['YardId'];
                   const yardDescription = item['YardDescription'];
                   const id = this.props.user['id'];
                   const yardName = item['YardName'];
                   console.log(id);
                   option.action = () => {this.props.getYard({yardId, id, yardName, yardDescription})};
                   return option;
                });
                let addYardOption = {
                    key: 0,
                    display: "+",
                    action: () => this.props.addYard(this.props.user['id'])
                }
                yardOptions.push(addYardOption);
                this.setState( {
                    data: yardOptions
                });
                console.log(this.state.data);
            })
            .catch((response)=> {
                console.log(response);
                let yardOptions = [{
                    key: 0,
                    display: "+",
                    action: () => this.props.addYard(this.props.user['id'])
                }];
                this.setState( {
                    data: yardOptions
                });
            });

    }
    renderList(){
        if (this.props.yardLoading){
            return <View style={{ flex: .9}}><Spinner size = "large" /></View>
        }
        return (
            <DisplayTiles options={this.state.data} />
        );
    } 

    render(){

        return (
            <View style={{flex: 1}}>
                <SectionHeader>Yards</SectionHeader>
                {this.renderList()}
                <BottomNavBar activeTab={1}/>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { user } = state.auth;
    const {yardLoading} = state.display;
    return { user, yardLoading };
};

export default connect( mapStateToProps, {getYard, addYard, loadYards})(YardSelectionView);