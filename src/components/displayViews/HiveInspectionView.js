import React, { Component } from 'react';
import {View, FlatList, Text} from 'react-native';
import { connect } from 'react-redux';
import { BottomNavBar, Button, Question, HorizontalRule, WideButton } from '../common';
import { editInspection, hiveSettings} from '../../actions';
import { PrimaryColor, BeepodsGraySix } from '../../beePodsStyles.js';

class HiveInspectionView extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            options: [],
            settingsAction: () => {this.props.hiveSettings({})}
        }
    }
    componentWillMount(){
        console.log(this.props.hiveInspections);
        let inspections = this.props.hiveInspections.map(item =>{
            let inspectionObject = {
                key: parseInt(item['HiveInspectionId']),
                inspectionDate: item['DatePosted'],
                editAction: () => {this.props.editInspection({currentInspection: item})}
            }
            return inspectionObject;
        });
        this.setState({
            options: inspections
        });

    }
    editInspection(id){
        let currentInspection;
        for(let i = 0; i < this.props.hiveInspections.length; i++){
            if(this.props.hiveInspections[i]['HiveInspectionId'] == id){
                currentInspection = this.props.hiveInspections[i];
            }
        }
        this.props.editInspection({currentInspection});
    }
    renderInspections(){
        const { containerStyle, optionsContainerStyle, listItem, listText, radioListStyle } = styles;
        if(this.state.options.length > 0 ){
            return (
                <FlatList
                    style={radioListStyle}
                    data={this.state.options}
                    alwaysBounceVertical={false}
                    renderItem={({ item }) => (
                        <View style={listItem}>
                            <Text style={listText}>Inspection Date: {item.inspectionDate}</Text>
                            <WideButton Title={' Edit    '} Action={item.editAction}/>
                        </View>
                    )}
                />
            );
        }
        else{
            return <View style={{flex:.9}}><Text>No Inspections Available</Text></View>
        }
    }
    renderSettingsButton(){
        if(this.props.hiveOwnerStatus){
            return<Button height = {50} onPress={this.state.settingsAction} color = {PrimaryColor}>Edit Hive</Button>
        }
    }
    renderSeparator(){
        return (
            <HorizontalRule />
        );
    };
	render(){
       
		return(
            <View style={{flex:1}}>
            {this.renderInspections()}
            {this.renderSettingsButton()}
            <BottomNavBar activeTab={1}/>
            </View>
		);
	}

}
const styles = {
    containerStyle: {
    paddingLeft: 20,
    paddingRight: 20
  },
  optionsContainerStyle: {
    flexDirection: 'column',
    marginBottom: 20
  },
  radioListStyle: {
    flex: .9,
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    paddingLeft: 10,
    alignSelf: 'center',
    fontSize: 16
  }
};
const mapStateToProps = (state) => {
    const {user} = state.auth;
	const {hiveInspections, hiveOwnerStatus} = state.display;
	return {hiveInspections, user, hiveOwnerStatus};
};
export default connect( mapStateToProps, {editInspection, hiveSettings})(HiveInspectionView);