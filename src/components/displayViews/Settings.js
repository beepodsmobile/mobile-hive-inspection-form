import React, {Component} from 'react';
import {View} from 'react-native';
import { connect } from 'react-redux';
import {logout} from '../../actions'; 
import {BottomNavBar, WideButton, SectionHeader} from '../common';

class Settings extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:.9}}>
                    <WideButton Title={' Logout    '} Action={this.props.logout}/>
                </View>
                <BottomNavBar activeTab={2}/>
            </View>
            );
    }

}
const mapStateToProps = (state) => {
    const {} = state.display
    return {};
};
export default connect( mapStateToProps, {logout})(Settings);