import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveHiveSettings, updateHiveSetting, getHive} from '../../actions';
import { StyleSheet, Text, View, FlatList, ScrollView, Picker, Modal } from 'react-native';
import { ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput, WideButton } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';
import axios from 'axios';

class HiveSettings extends Component {
    constructor(props){
        super(props);

        this.state = {
            hiveName: "",
            hiveDescription: "",
            userOptions: [],
            addUserModalVisible: false,
            userEmail: ""
        }

    }
    componentWillMount(){
        this.getHiveInfo();
        this.getUsers();
    }
    getUsers(){
        console.log('getusers');
        let userRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/hiveusers/?hiveId=${this.props.currentHiveId}`;
        let config = {"Cache-Control" : "no-store"};
        axios.get(userRequestUrl, config)
        .then(response => {
            console.log(response);
            let options = response.data.map(user=>{
            if(user.ID != this.props.user["id"]){
                let userObject = {
                key: user['ID'],
                name: user['user_email'],
                visible: false,
                accept: () => { this.removeUser(user['ID']);
                                this.switchUserModal(user['ID']);
                                },
                removeButtonOnPress: () => {this.switchUserModal(user['ID'])},
                decline: () => {this.switchUserModal(user['ID'])}
                };
                return userObject;
            }
            });
            let opts =  options.filter(item=>{
                if(item==undefined){
                    return false;
                }
                else{
                    return true;
                }
            })
            console.log(opts);
            this.setState({
                userOptions: opts
            })
        })
        .catch(response => {console.log(response)});  
    }
    getHiveInfo(){
        let selectedHive = this.props.yardHives.filter(hive=>{
            if(hive.HiveId == this.props.currentHiveId){
                return true;
            }
            else{
                return false;
            }
        });
        this.setState({
            hiveName: selectedHive[0].HiveName,
            hiveDescription: selectedHive[0].HiveDescription,
            selectedHive: selectedHive[0]
        });
    }
    addUser(){
        let addObject = {
            uid: this.props.user['id'],
            hiveId: this.props.currentHiveId,
            userToAdd: this.state.userEmail
        }
        let config = {'content-type': 'application/json'}
        let addUserUrl = `https://www.beepods.com/wp-json/inspection/v1/yardadduser/`;
        axios.post(addUserUrl, addObject, config)
            .then(response=>{
                this.setState({
                    userEmail: ""
                });
        });
    }
    removeUser(id){
        let removeObject = {
            uid: this.props.user['id'],
            hiveId: this.props.currentYardId,
            usersToRemove: [id]
        }
        let config = {'content-type': 'application/json'}
        let removeUserUrl = `https://www.beepods.com/wp-json/inspection/v1/hiveremoveuser/`;
        axios.post(removeUserUrl, removeObject, config)
            .then(response => {
                let users = this.props.userOptions.map(user=>{
                    if(user['key'] != id){
                    return user;
                    }
                });
            this.setState({
                userOptions: users
            });

        })
        .catch(response => {
            console.log('fail', response);
        });
    } 
    switchUserModal(key){
        let userOptions = this.state.userOptions.map((item)=>{
            if(item.key==key){
                item.visible = !item.visible;
            }
            return item;
        });
        this.setState({
            userOptions: userOptions
        });
    }
    save(){
        let selectedHive = this.state.selectedHive;
        selectedHive.HiveName = this.state.hiveName;
        selectedHive.HiveDescription = this.state.hiveDescription;
        console.log(selectedHive);
        let updateObject = {
            'hiveId': selectedHive.HiveId,
            'uid': this.props.user['id'],
            'yardId': selectedHive.YardId,
            'breedId': selectedHive.BreedId,
            'hiveName': selectedHive.HiveName,
            'hiveDescription': selectedHive.HiveDescription,
            'dateAdded': selectedHive.DateCreated,
            'dateModified': selectedHive.DateModified,
            'serial': selectedHive.SerialNumber,
            'email': selectedHive.OwnerEmail
        };
        let config = {'content-type': 'application/json'};
        let postHiveUpdateUrl = `https://www.beepods.com/wp-json/inspection/v1/hiveaddupdate/`;
        axios.post(postHiveUpdateUrl, updateObject, config)
            .then(response => {
                console.log(response);
            })
        .catch(response=>{console.log(response)});
        this.props.getHive({hiveId: selectedHive.HiveId, id: this.props.user['id'], hiveName: selectedHive.HiveName});
    }
  render(){
    const { containerStyle, insetContainerStyle, modalContainerStyle} = styles;
    return(
        <ScrollView style={containerStyle}>
            <SectionHeader>Hive Info</SectionHeader>
            <View style={insetContainerStyle}>
                <TitledInput
                    label='Hive Name'
                    value={this.state.hiveName}
                    onChangeText={value=>this.setState({ hiveName: value})}
                />
                <TitledInput
                    label='Hive Description'
                    value={this.state.hiveDescription}
                    onChangeText={value=>this.setState({ hiveDescription: value})}
                />
            </View>
            <SectionHeader>Hive Users</SectionHeader>
            <View style={insetContainerStyle}>
                <ManagedList
                    data={this.state.userOptions}
                    addButtonText={"Add User"}
                    addButtonOnPress={() => {this.setState({
                        addUserModalVisible: true
                    })}}
                    removeButtonOnPress={() => {console.log('remove user button pressed')}}
                />
                <Modal
                    animationType="fade"
                    transparent= {true}
                    visible={this.state.addUserModalVisible}
                    onRequestClose={() => {
                        this.setState({
                        addUserModalVisible: false,
                        });
                }}>
                    <View style={modalContainerStyle}>
                        <View style={{flex: .8, justifyContent: 'center', backgroundColor: "#e6e6e6", padding: 10, paddingTop: 30, borderRadius: 5}}>
                            <TitledInput label={"Enter user's email"} value={this.state.userEmail} onChangeText={(value)=>{this.setState({userEmail:value})}}/>
                            <WideButton Action={() => {this.addUser(); this.setState({addUserModalVisible: false})}} Title={' Add    '}/>
                            <WideButton Action={() => {this.setState({addUserModalVisible: false})}} Title={' Close    '}/>
                        </View>
                    </View>
                </Modal>
            </View>
          <Button color={PrimaryColor} onPress={this.save.bind(this)}>Save Details</Button>
        </ScrollView>
    );
  }

}
const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        marginTop: 10
    },
    modalContainerStyle:{
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
        position: 'relative',
        padding: 20,
        flex: 1,
        justifyContent: 'center'
    },
    insetContainerStyle: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10
    },
});
const mapStateToProps = (state) =>{
    const {user} = state.auth;
    const { currentHiveId, yardHives, currentHiveName} = state.display;
    return {user, currentHiveName, currentHiveId, yardHives};
}

export default connect(mapStateToProps, {saveHiveSettings, getHive})(HiveSettings);