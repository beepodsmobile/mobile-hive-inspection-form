import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveNewHive, updateYardSetting } from '../../actions';
import { StyleSheet, Text, View, FlatList, ScrollView, Picker } from 'react-native';
import { ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';
import axios from 'axios';

class AddHiveView extends Component {
    constructor(props){
        super(props);
        this.state = {
            beeBreedDefault: "Select Breed",
            beeBreedSelection: "Select Breed",
            breedOptions: ["Select Breed"]
        }
        this.save = this.save.bind(this);
    }
    componentWillMount(){
        let breedRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/beeoptions/`
        axios.get(breedRequestUrl)
            .then((response)=>{
                let options = response.data.map((option)=>{
                    console.log(option);
                    return option.BreedName;
                });
                options.push(this.state.beeBreedSelection);
                this.setState({breedOptions: options, breeds:response.data});
                console.log(this.state.breeds);
            });

    }
    save(){
        console.log(this.props.currentHiveSerial);
        if(this.state.beeBreedSelection == "Select Breed"){
            this.setState({error: "Please select a breed"});
        }
        else{
            console.log(this.props.currentHiveSerial);
            const name = this.props.currentHiveName;
            const description = this.props.currentHiveDescription;
            const serial = this.props.currentHiveSerial;
            const user = this.props.user;
            const yard = this.props.currentYardId;
            let breedId;
            for(let i = 0; i < this.state.breeds.length; i++){
                if(this.state.breeds[i]['BreedName'] == this.state.beeBreedSelection){
                    breedId = this.state.breeds[i]['BreedId'];
                }
            }

            this.props.saveNewHive({name, description, serial, breedId, user, yard});
        }
    }
    renderError(){
        if(this.state.error != "none"){
            return <Text>{this.state.error}</Text>
        }
    }
    render(){
        const pickItems = this.state.breedOptions.map((item)=>{
            console.log(item);
            return <Picker.Item key= {item} label={item} value = {item} />
        });
        const { containerStyle, insetContainerStyle } = styles;
        return(
            <ScrollView style={containerStyle}>
                <View style={insetContainerStyle}>
                    <TitledInput
                        label='Hive Name'
                        value={this.props.currentHiveName}
                        onChangeText={value=>this.props.updateYardSetting({prop:'currentHiveName', value})}
                    />
                    <TitledInput
                        label='Hive Description'
                        value={this.props.currentHiveDescription}
                        onChangeText={value=>this.props.updateYardSetting({prop:'currentHiveDescription', value})}
                    />
                    <TitledInput
                        label='Serial Number'
                        value={this.props.currentHiveSerial}
                        onChangeText={value=>this.props.updateYardSetting({prop:'currentHiveSerial', value})}
                    />
                    <Picker
                        selectedValue={this.state.beeBreedSelection}
                        onValueChange={(breed)=>(this.setState({beeBreedSelection: breed}))}>
                        {pickItems}
                    </Picker>
                    {this.renderError()}
                    <Button color={PrimaryColor} onPress={this.save}>Save Hive</Button>
                </View>
            </ScrollView>
            );
    }
}
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    marginTop: 10
  },
  insetContainerStyle: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
});

const mapStateToProps = (state) => {
    const { user } = state.auth;
    const { currentHiveDescription, currentHiveName, currentYardId, currentHiveSerial } = state.display;
    return { user, currentHiveName, currentHiveDescription, currentYardId, currentHiveDescription, currentHiveSerial };
}   
export default connect( mapStateToProps, {saveNewHive, updateYardSetting})(AddHiveView);