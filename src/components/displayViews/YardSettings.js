import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveYardSettings, updateYardSetting, addHive, removeHive } from '../../actions';
import { StyleSheet, Text, View, FlatList, ScrollView, Picker, Modal } from 'react-native';
import { ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput, WideButton } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';
import axios from 'axios';

class YardSettings extends Component {
  constructor(props){
    super(props);

    this.state = {
      yardName: '',
      latitude: null,
      longitude: null,
      error: '',
      showErrors: false,
      Address1: '',
      City: '',
      State: '',
      ZipCode: '',
      addUserModalVisible: false,
      hiveOptions: [],
      userOptions: []
    };

  }
  componentWillMount(){
    this.getUsers();
    let hiveOptions = this.props.yardHives.map((hive)=>{
      const hiveId = hive.HiveId;
      const userId = this.props.user['id'];
      const yardId = this.props.currentYardId;
      let hiveObject = {
        key: hive.HiveId,
        name: hive.HiveName,
        visible: false,
        accept: () => {this.props.removeHive({hiveId, yardId, userId});
                        this.switchHiveModal(hive.HiveId);
                      },
        removeButtonOnPress: () => {this.switchHiveModal(hive.HiveId)},
        decline: () => {this.switchHiveModal(hive.HiveId)}
      }
      return hiveObject;
    });
    this.setState({
      hiveOptions: hiveOptions
    });
    console.log(this.state.hiveOptions);
  }
  switchHiveModal(key){
    let hiveOptions = this.state.hiveOptions.map((item)=>{
      if(item.key==key){
        item.visible = !item.visible;
      }
      return item;
    });
    this.setState({
      hiveOptions: hiveOptions
    });
  }
  switchUserModal(key){
    let userOptions = this.state.userOptions.map((item)=>{
      if(item.key==key){
        item.visible = !item.visible;
      }
      return item;
    });
    this.setState({
      userOptions: userOptions
    });
  }
  getGoogleMapsGeolocationFromAdress(){
    this.setState({
      latitude: null,
      longitude: null,
      error: '',
      showErrors: false
    });
    let APICall = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    let googleMapsAPIKey = "&key=AIzaSyAU4pj1i5c905yjhgWE15V1SbT-jOgRaMg";
    let errorMessages = [];
    if ( !(this.props.currentYardName.length > 0) ) {
      errorMessages.push('You must provide a yard name.');
    }
    if ( !(this.props.currentYardAddressLine.length > 0) ) {
      errorMessages.push('You must provide an street address.');
    }
    if ( (this.props.currentYardAddressZip.length !== 5) || isNaN(this.state.ZipCode) ) {
      errorMessages.push('You must provide a 5-digit zip code.');
    }
    if ( errorMessages.length > 0 ) {
      let errors = '';
      for ( let i = 0; i < errorMessages.length; i++ ) {
        errors += errorMessages[i];
        if ( i < errorMessages.length-1 ) {
          errors += '\n';
        }
      }
      this.setState({ error: errors, showErrors: true });
    } 
    else {
      this.setState({ error: '', showErrors: false });
      APICall += this.cleanCode(this.props.currentYardAddressLine) + '+';
      APICall += this.cleanCode(this.props.currentYardAddressCity) + '+';      
      APICall += this.cleanCode(this.props.currentYardAddressState) + '+';
      APICall += this.cleanCode(this.props.currentYardAddressZip) + '+';
      APICall += googleMapsAPIKey;
      this.getLocation(APICall);
    }
  }

  getLocation(APICall) {
    axios.get(APICall)
      .then(response => {
        let results = response.data.results[0];
        if ( results == null ) {
          this.setState({ error: 'Invalid Address.', showErrors: true });
        } else {
          let addressComponents = results.address_components;
          let stateAbbreviation = null;
          let cityName = null;
          for (let i=0; i<addressComponents.length; i++) {
            if ( addressComponents[i].types.includes('administrative_area_level_1') ) {
              stateAbbreviation = addressComponents[i].short_name;
            }
            if ( addressComponents[i].types.includes('locality') ) {
              cityName = addressComponents[i].short_name;
            }
          }
          let location = results.geometry.location;
          lat = location.lat;
          lng = location.lng;
          const email = this.props.user['email'];
          const userId = this.props.user['id'];
          const yardId = this.props.currentYardId;
          const latitude = lat;
          const longitude = lng;
          const addressLine = this.props.currentYardAddressLine;
          const city = this.props.currentYardAddressCity;
          const state = this.props.currentYardAddressState;
          const zip = this.props.currentYardAddressZip;
          const yardName = this.props.currentYardName;
          const yardDescription = this.props.currentYardDescription;
          this.props.saveYardSettings({address: addressLine, city: city, state: state, zip: zip, lat: latitude, lng: longitude, userId: userId, yardId: yardId, email: email, yardName: yardName, yardDescription: yardDescription});
          this.setState({ latitude: lat });
          this.setState({ longitude: lng });
        }
      });
  }
  getErrorMessages() {
    const { errorViewStyle, errorTextStyle } = styles;
    if(this.state.showErrors) {
      return(
        <View style={errorViewStyle}>
          <Text style={errorTextStyle}>{ this.state.error }</Text>
        </View>
      );
    }
    else {
      return null;
    }
  }

  getLatLng() {
    if ( this.state.latitude && this.state.longitude ) {
      return(
        <View>
          <Text>Latitude: {this.state.latitude}</Text>
          <Text>Longitude: {this.state.longitude}</Text>
        </View>
      );
    } else {
      return null;
    }
  }
  getUsers(){
    console.log('getusers');
    let userRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/yardusers/?yardId=${this.props.currentYardId}`;
    let config = {"Cache-Control" : "no-cache"};
    axios.get(userRequestUrl, config)
      .then(response => {
        let options = response.data.map(user=>{
          if(user.ID != this.props.user["id"]){
            let userObject = {
              key: user['ID'],
              name: user['user_email'],
              visible: false,
              accept: () => { this.removeUser(user['ID']);
                              this.switchUserModal(user['ID']);
                            },
              removeButtonOnPress: () => {this.switchUserModal(user['ID'])},
              decline: () => {this.switchUserModal(user['ID'])}
            }
            return userObject;
          }
        });
      })
      .catch(response => {console.log(response)});
  }
  addUser(){
    let addObject = {
      uid: this.props.user['id'],
      yardId: this.props.currentYardId,
      userToAdd: this.state.userEmail
    }
    let config = {'content-type': 'application/json'}
    let addUserUrl = `https://www.beepods.com/wp-json/inspection/v1/yardadduser/`;
    axios.post(addUserUrl, addObject, config)
      .then(response=>{
        this.setState({
          userEmail: ""
        });
      })

  }
  removeUser(id){
    let removeObject = {
      uid: this.props.user['id'],
      yardId: this.props.currentYardId,
      usersToRemove: [id]
    }
    let config = {'content-type': 'application/json'}
    let removeUserUrl = `https://www.beepods.com/wp-json/inspection/v1/yardremoveuser/`;
    axios.post(removeUserUrl, removeObject, config)
        .then(response => {
            let users = this.props.userOptions.map(user=>{
              if(user['key'] != id){
                return user;
              }
            });
            this.setState({
              userOptions: users
            });
        })
        .catch(response => {
            console.log('fail', response);
        });
  }
  cleanCode(c) {
      return c.replace(/[^A-Za-z0-9_]/g, "");
  }

  render() {
    const { containerStyle, insetContainerStyle, labelStyle, modalContainerStyle} = styles;
    return (
      <ScrollView style={containerStyle}>
        <SectionHeader>Yard Address</SectionHeader>
        <View style={insetContainerStyle}>
          <TitledInput
            label='Yard Name'
            value={this.props.currentYardName}
            onChangeText={value=>this.props.updateYardSetting({prop:'currentYardName', value})}
          />
          <TitledInput
            label='Yard Description'
            value={this.props.currentYardDescription}
            onChangeText={value=>this.props.updateYardSetting({prop:'currentYardDescription', value})}
          />
          <TitledInput
            label='Street Address'
            value={this.props.currentYardAddressLine}
            onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressLine', value})}
          />
          <TitledInput
            label='City'
            value={this.props.currentYardAddressCity}
            onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressCity', value})}
          />
          <TitledInput
            label='State'
            value={this.props.currentYardAddressState}
            onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressState', value})}
          />         
          <TitledInput
            label='Zip Code'
            value={this.props.currentYardAddressZip}
            onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressZip', value})}
          />
          <Button color={PrimaryColor} onPress={this.getGoogleMapsGeolocationFromAdress.bind(this)}>Save Address</Button>
          {this.getErrorMessages()}
          {this.getLatLng()}
        </View>
        <SectionHeader>Hives in Yard</SectionHeader>

        <View style={insetContainerStyle}>
          <ManagedList
            data={this.state.hiveOptions}
            addButtonText={"Add Hive"}
            addButtonOnPress={() => {this.props.addHive(this.props.user['id'])}}
            removeButtonOnPress={() => {console.log('remove hive button pressed')}}
          />
        </View>
        <SectionHeader>Yard Users</SectionHeader>
        <View style={insetContainerStyle}>
          <ManagedList
            data={this.state.userOptions}
            addButtonText={"Add User"}
            addButtonOnPress={() => {this.setState({
              addUserModalVisible: true
            })}}
            removeButtonOnPress={() => {console.log('remove user button pressed')}}
          />
          <Modal
                animationType="fade"
                transparent= {true}
                visible={this.state.addUserModalVisible}
                onRequestClose={() => {
                    this.setState({
                      addUserModalVisible: false,
                    });
                }}>
            <View style={modalContainerStyle}>
              <View style={{flex: .8, justifyContent: 'center', backgroundColor: "#e6e6e6", padding: 10, paddingTop: 30, borderRadius: 5}}>
                <TitledInput label={"Enter user's email"} value={this.state.userEmail} onChangeText={(value)=>{this.setState({userEmail:value})}}/>
                <WideButton Action={() => {this.addUser(); this.setState({addUserModalVisible: false})}} Title={' Add    '}/>
                <WideButton Action={() => {this.setState({addUserModalVisible: false})}} Title={' close    '}/>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    marginTop: 10
  },
  modalContainerStyle:{
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    padding: 20,
    flex: 1,
    justifyContent: 'center'
  },
  insetContainerStyle: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  labelStyle: {
    fontSize: 12,
    color: DarkGrayColor,
    fontWeight: '200',
    flex: 1
  },
  errorViewStyle: {
    backgroundColor: DangerColor,
    borderRadius: 5,
    padding: 10,
    marginBottom: 10
  },
  errorTextStyle: {
    color: '#fff'
  },
  inputDisplayOnlyStyle: {
      paddingRight: 5,
      paddingLeft: 5,
      color: DarkGrayColor,
      fontSize: 18,
      fontWeight: '200',
      flex: 1,
      height: 40,
  },
  displayOnlyContainer: {
    height: 45,
    flexDirection: 'column',
    width: '100%',
    borderColor: LightGrayColor,
    borderBottomWidth: 1,
    marginBottom: 10
  }
});

const mapStateToProps = (state) => {
    const { user } = state.auth;
    const { currentYardAddressLine, currentYardAddressCity, currentYardDescription, currentYardId, currentYardName, currentYardAddressState, currentYardAddressZip, yardHives} = state.display;
    return { user, currentYardAddressLine, currentYardAddressCity, currentYardName, currentYardDescription, currentYardId, currentYardAddressState, currentYardAddressZip, yardHives };
}   

export default connect( mapStateToProps, {saveYardSettings, updateYardSetting, addHive, removeHive})(YardSettings);
