import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveYardSettings, updateYardSetting, submitForm } from '../../actions';
import { StyleSheet, Text, TextInput, View, FlatList, ScrollView, Picker, Switch } from 'react-native';
import { ExpandableBarCard, ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput, TitledInputMultiLine, TitledIntegerInput } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';
import axios from 'axios';

class EditInspectionView extends Component {
    constructor(props){
        super(props);
        this.state ={
            inspection: this.props.currentInspection,
        }       
    }
    getExteriorOptions(){
        let options = [
            {   key: 1, 
                option: "Exterior Bearding?", 
                value: this.state.inspection.ExteriorBearding == 1?true:false,
                 action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        ExteriorBearding: value == true?1:0
                                        }   
                                    })
                                }
             },
            {   key: 2, option: "Exterior Guarding?",
                value: this.state.inspection.ExteriorGaurding == 1?true:false,
                action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        ExteriorGaurding: value == true?1:0
                                        }   
                                    })
                                }
            },
            {   key: 3,
                option: "Exterior Drones?",
                value: this.state.inspection.ExteriorDrones == 1?true:false,
                action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        ExteriorDrones: value == true?1:0
                                        }   
                                    })
                                }
            },
            {   key: 4,
                option: "Exterior Robbers?",
                value: this.state.inspection.ExteriorRobbers == 1?true:false,
                action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        ExteriorRobbers: value == true?1:0
                                        }   
                                    })
                                }
            }];
        return options;
    }
    submit(){
        let teaCupTotal = 0;
        let uncappedTotal = 0;
        let cappedTotal = 0;
        let emergedTotal = 0;
        for(var i = 0; i < this.state.inspection.Bars.length; i++){
            teaCupTotal += this.state.inspection.Bars[i].TeaCupCount;
            uncappedTotal += this.state.inspection.Bars[i].UncappedQueenCellCount;
            cappedTotal += this.state.inspection.Bars[i].CappedCount;
            emergedTotal += this.state.inspection.Bars[i].ESSmergedQueenCellCount;
        }
         const inspection = {
            id: this.props.user['id'],
            inspectionId: this.state.inspection.HiveInspectionId,
            hiveId: this.state.inspection.HiveId,
            inspectorEmail: this.props.user['email'],
            inspectorName: this.props.user['firstname']+" "+this.props.user['lastname'],
            guests: this.state.inspection.Guests,
            datePosted: this.state.inspection.DatePosted,
            dateModified: this.state.inspection.DateModified,
            temperature: this.state.inspection.Temperature,
            airPressure: this.state.inspection.AirPressure,
            humidity: this.state.inspection.Humidity,
            exteriorActivity: this.state.inspection.ExteriorActivity,
            exteriorBearding: this.state.inspection.ExteriorBearding,
            exteriorDrones: this.state.inspection.ExteriorDrones,
            exteriorGaurding: this.state.inspection.ExteriorRobbers,
            exteriorRobbers: this.state.inspection.ExteriorRobbers,
            entranceActivity: this.state.inspection.EntranceActivity,
            entrancesUsed: this.state.inspection.EntrancesUsed,
            hiveCapacity: this.state.HiveCapacity,
            pollenCollection: this.state.inspection.PollenCollection,
            pollenArray: this.state.inspection.PollenColors,
            interiorActivity: this.state.inspection.InteriorActivity,
            numbersOfBarsWithAttachedComb: this.state.inspection.NumberOfBarsWithCombAttached,
            initialNumberOfBarsInInnerChamber: this.state.inspection.InitialBarsWithInnerChamber,
            beesSeenOnLid: this.state.inspection.BeesSeenOnLid,
            beesSeenOnBars: this.state.inspection.BeesSeenOnBars,
            beesSeenOnFloor: this.state.inspection.BeesSeenOnFloors,
            beesSeenDead: this.state.inspection.DeadBeesFound,
            critterDetails: this.state.inspection.Critters,
            critterArray: this.state.inspection.CritterArray,
            bugDetails: this.state.inspection.Bugs, 
            bugArray: this.state.inspection.BugArray,
            ventBoardPosition: this.state.inspection.VentBoardPosition,
            queenCellContent:this.state.inspection.QueenCellContent,
            barArray: this.state.inspection.Bars,
            teaCupTotal: teaCupTotal,
            uncappedTotal: uncappedTotal,
            cappedTotal: cappedTotal,
            emergedTotal: emergedTotal,
            miteCheckPerformed: this.state.inspection.MiteCheckPerformed,
            shouldColonyBeSplit: this.state.inspection.ShouldColonyBeSplit,
            observations: this.state.inspection.Observations,
            actionsTaken: this.state.inspection.ActionsTaken,
            futureActions: this.state.inspection.FutureActions,
            nextTimeBring: this.state.inspection.NextTimeBring,
            initialFeederFluid: this.state.inspection.InitialFeederFluid,
            finalFeederFluid: this.state.inspection.FinalFeederFluid,
            finalBarsInInnerChamber: this.state.inspection.FinalBarsWithOnnerChamber,
            honeyCollectedForMe: this.state.inspection.HoneyHarvestedForMe,
            honeyCollectedForBees: this.state.inspection.HoneyHarvestedForBees,
            complete: true
        }
        this.props.submitForm({inspection});
    }
    getBeeLocationOptions(){
        let options = [
            {key: 5,
            option: "Bees on Bars?",
            value: this.state.inspection.BeesSeenOnBars == 1?true:false, 
            action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        BeesSeenOnBars: value == true?1:0
                                        }   
                                    })
                                }},
            {key: 6,
            option: "Bees on Lid?",
            value: this.state.inspection.BeesSeenOnLid == 1?true:false, 
            action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        BeesSeenOnLid: value == true?1:0
                                        }   
                                    })
                                }},
            {key: 7, 
            option: "Bees on Floor?", 
            value: this.state.inspection.BeesSeenOnFloors == 1?true:false, 
            action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        BeesSeenOnFloors: value == true?1:0
                                        }   
                                    })
                                }},
            {key: 8, 
            option: "Dead Bees?", 
            value: this.state.inspection.DeadBeesFound == 1?true:false,
            action: (value)=>{this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        DeadBeesFound: value == true?1:0
                                        }   
                                    })
                                }}];
        return options;
    }
    renderBars(){
        let bars = this.state.inspection.Bars.map(bar=>{
            return <ExpandableBarCard 
                key={bar.BarInspectionId}
                title={`Bar Number ${bar.BarNumber}`}
                content={{
                            ContentsBroodDrone: bar.ContentsBroodDrone,
                            ContentsBroodWorker: bar.ContentsBroodWorker,
                            ContentsEggs: bar.ContentsEggs,
                            ContentsHoney: bar.ContentsHoney,
                            ContentsLarva: bar.ContentsLarva,
                            ContentsNectar: bar.ContentsNectar,
                            ContentsPollen: bar.ContentsPollen,
                            AttachedComb: bar.AttachedComb,
                            CombBuiltOut: bar.CombBuiltOut,
                            CappedQueenCellCount: bar.CappedQueenCellCount,
                            EmergedQueenCellCountt: bar.EmergedQueenCellCount,
                            Follower: bar.Follower,
                            JoinedBar: bar.JoinedBar,
                            Spacer: bar.Spacer,
                            TeaCupCount: bar.TeaCupCount,
                            UncappedQueenCellCount: bar.UncappedQueenCellCount,
                            WavyComb: bar.WavyComb
                        }}
                barUpdate={({prop, value}) => {
                    console.log(value, prop)
                    let newBars = this.state.inspection.Bars.map(newBar=>{
                        if(newBar.BarInspectionId == bar.BarInspectionId){
                            let barObject = { ...newBar, [prop]: value}
                            return barObject;
                        }
                        else{
                            return newBar;
                        }
                    });
                    let newInspection = {
                        ...this.state.inspection,
                        Bars: newBars
                    }
                    this.setState({
                        inspection: newInspection
                    });
                    }
                }
            />
        });
        return bars;
    }
    getExteriorActivityOptions(){
        let options = ["HEAVY","MODERATE","LIGHT", "NONE"];
        let pickerOptions = options.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        }); 
        return pickerOptions;
    }
    getEntranceActivityOptions(){
        let options = ["ENTERING MORE","EXITING MORE","ABOUT THE SAME", "NONE"];
        let pickerOptions = options.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        }); 
        return pickerOptions;
    }
    getEntranceUsedOptions(){
        let options = ["SIDE","END","BOTH", "ALTERNATIVE", "NONE"];
        let pickerOptions = options.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        }); 
        return pickerOptions;
    }
    getHiveCapacityOptions(){
        let options = ["FULL","PARTLY FULL","SPARSE", "EMPTY"];
        let pickerOptions = options.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        }); 
        return pickerOptions;
    }
    getInternalActivityOptions(){
        let options = ["ACTIVE","DOCILE","FRENZIED", "DEFENSIVE", "NONE"];
        let pickerOptions = options.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        }); 
        return pickerOptions;
    }
    getVentBoardOptions(){
        let options = ["REMOVED","FULLY OPEN","PARTIALLY OPEN", "CLOSED"];
        let pickerOptions = options.map((item)=>{
            return <Picker.Item key= {item} label={item} value = {item} />
        }); 
        return pickerOptions;
    }
    render() {
        const { containerStyle, insetContainerStyle, listItem, listText, labelStyle, radioListStyle, inputStyle } = styles;
        const exteriorOptions = this.getExteriorOptions();
        const beeLocationOptions = this.getBeeLocationOptions();
        const exteriorActivityPickerOptions = this.getExteriorActivityOptions();
        const entranceActivityPickerOptions = this.getEntranceActivityOptions();
        const entranceUsedPickerOptions = this.getEntranceUsedOptions();
        const hiveCapacityPickerOptions = this.getHiveCapacityOptions();
        const internalActivityPickerOptions = this.getInternalActivityOptions();
        const ventBoardOptions = this.getVentBoardOptions();
        return(
            <ScrollView style={containerStyle}>
                <SectionHeader>Observations</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInputMultiLine
                    label = "Observations" 
                        value = {this.state.inspection.Observations}
                        onChangeText = {(value) => {
                            
                            this.setState({
                            inspection: {
                                ...this.state.inspection,
                                Observations: value
                            }
                            });
                        }}
                    />
                </View>
                <SectionHeader>Actions Taken</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInputMultiLine
                        label="Actions Taken"
                        value = {this.state.inspection.ActionsTaken}
                        onChangeText = {(value) => {
                            
                            this.setState({
                            inspection: {
                                ...this.state.inspection,
                                ActionsTaken: value
                            }
                            });
                        }}
                    />
                </View>
                <SectionHeader>Future Actions Required</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInputMultiLine
                        label= "Future Actions"
                        value = {this.state.inspection.FutureActions}
                        onChangeText = {(value) => {
                            
                            this.setState({
                            inspection: {
                                ...this.state.inspection,
                                FutureActions: value
                            }
                            });

                        }}
                    />
                </View>
                <SectionHeader>Next Time Bring</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInputMultiLine
                        label = "Bring"
                        value = {this.state.inspection.NextTimeBring}
                        onChangeText = {(value) => {
                            this.setState({
                            inspection: {
                                ...this.state.inspection,
                                NextTimeBring: value
                            }
                            });
                        }}
                    />
                </View>
                <SectionHeader>Guests</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInputMultiLine
                        label = "Guests"
                        value = {this.state.inspection.Guests}
                        onChangeText = {(value) => {
                            
                            this.setState({
                            inspection: {
                                ...this.state.inspection,
                                Guests: value
                            }
                            });
                        }}
                    />
                </View>
                <SectionHeader>Weather Conditions</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledIntegerInput
                        label='Temperature Kelvin'
                        value={this.state.inspection.Temperature.toString()}
                        onChangeText={(value)=> {this.setState({inspection: {...this.state.inspection,Temperature: value}});}}
                    />
                    <TitledIntegerInput
                        label='Air Pressure hPa'
                        value={this.state.inspection.AirPressure.toString()}
                        onChangeText={(value)=>{this.setState({inspection: {...this.state.inspection,AirPressure: value}});}}
                    />
                    <TitledIntegerInput
                        label='Humidity %'
                        value={this.state.inspection.Humidity.toString()}
                        onChangeText={(value)=>{this.setState({inspection: {...this.state.inspection, Humidity: value}})}}
                    />
                </View>
                <SectionHeader>Date Posted</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInput
                        label='Date'
                        value={this.state.inspection.DatePosted.toString()}
                        onChangeText={(value)=>{this.setState({
                                                    inspection: {
                                                        ...this.state.inspection,
                                                        DatePosted: value
                                                    }
                                                    });}}
                    />
                </View>
                <SectionHeader>Exterior</SectionHeader>
                <View style={insetContainerStyle}>
                    <View>
                        <Text style={listText}>Exterior Activity Level</Text>
                        <Picker
                            selectedValue={this.state.inspection.ExteriorActivity}
                            onValueChange={(value)=>{
                                this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                         ExteriorActivity: value
                                    }
                                });
                            }}>
                            {exteriorActivityPickerOptions}
                        </Picker>
                    </View>
                    <View>
                        <Text style={listText}>Entrance Activity Level</Text>
                        <Picker
                            selectedValue={this.state.inspection.EntranceActivity}
                            onValueChange={(value)=>{
                                this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                         EntranceActivity: value
                                    }
                                });
                            }
                        }>
                            {entranceActivityPickerOptions}
                        </Picker>
                    </View>
                    <View>
                        <Text style={listText}>Entrances Used</Text>
                        <Picker
                            selectedValue={this.state.inspection.EntrancesUsed}
                            onValueChange={(value)=>{
                                this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        EntrancesUsed: value
                                    }
                                });
                            }}>
                            {entranceUsedPickerOptions}
                        </Picker>
                    </View>
                    <FlatList
                        style={radioListStyle}
                        data={exteriorOptions}
                        alwaysBounceVertical={false}
                        renderItem={({ item }) => (
                            <View style={listItem}>
                                <Text style={listText}>{item.option}</Text>
                                <Switch
                                    onValueChange={item.action}
                                    value={item.value}
                                />
                            </View>
                        )}
                        ItemSeparatorComponent={this.renderSeparator}
                    />
                </View>
                {/*<SectionHeader>Pollen</SectionHeader>
                <View style={insetContainerStyle}>
                </View>*/}
                <SectionHeader>Critters</SectionHeader>
                <View style={insetContainerStyle}>
                    <TitledInputMultiLine
                        label="Critter Notes"
                        value = {this.state.inspection.Critters}
                        onChangeText={(value)=>{this.setState({
                                                    inspection: {
                                                        ...this.state.inspection,
                                                        Critters: value
                                                    }
                                                    });
                                    }}
                    />
                </View>
                <SectionHeader>Interior</SectionHeader>
                <View style={insetContainerStyle}>
                    <View>
                        <Text style={listText}>Hive Capacity</Text>
                        <Picker
                            selectedValue={this.state.inspection.HiveCapacity}
                            onValueChange={(value)=>{
                               this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        HiveCapacity: value
                                    }
                                }); 
                            }}>
                            {hiveCapacityPickerOptions}
                        </Picker>
                    </View>
                    <View>
                        <Text style={listText}>Interior Activity</Text>
                        <Picker
                            selectedValue={this.state.inspection.InteriorActivity}
                            onValueChange={(value)=>{
                                this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        InteriorActivity: value
                                    }
                                });
                            }}>
                            {internalActivityPickerOptions}
                        </Picker>
                    </View>
                    <View>
                        <Text style={labelStyle}>Number of Bars With Comb Attached</Text>
                        <TextInput
                            autoCorrect={false}
                            keyboardType='numeric'
                            value={this.state.inspection.NumberOfBarsWithCombAttached.toString()}
                            onChangeText={(value)=>{
                                this.setState({
                                    inspection:{
                                        ...this.state.inspection,
                                        NumberOfBarsWithCombAttached: parseInt(value) == NaN?0:value
                                    }
                                });
                            }}
                            style={inputStyle}
                            underlineColorAndroid = 'transparent'
                        />
                    </View>
                    <View>
                        <Text style={listText}>Vent Board</Text>
                        <Picker
                            selectedValue={this.state.inspection.VentBoardPosition}
                            onValueChange={(value)=>{
                                this.setState({
                                    inspection: {
                                        ...this.state.inspection,
                                        VentBoardPosition: value
                                    }
                                });
                            }}>
                            {ventBoardOptions}
                        </Picker>
                    </View>
                    <View>
                        <Text style={labelStyle}>Number of Bars In Inner Chamber</Text>
                        <TextInput
                            autoCorrect={false}
                            keyboardType='numeric'
                            value={this.state.inspection.InitialBarsWithInnerChamber.toString()}
                            onChangeText={(value)=>{
                                this.setState({
                                    inspection:{
                                        ...this.state.inspection,
                                        InitialBarsWithInnerChamber: parseInt(value) == NaN?0:value
                                    }
                                });
                            }}
                            style={inputStyle}
                            underlineColorAndroid = 'transparent'
                        />
                    </View>
                    <FlatList
                        style={radioListStyle}
                        data={beeLocationOptions}
                        alwaysBounceVertical={false}
                        renderItem={({ item }) => (
                            <View style={listItem}>
                                <Text style={listText}>{item.option}</Text>
                                <Switch
                                    onValueChange={item.action}
                                    value={item.value}
                                />
                            </View>
                        )}
                        ItemSeparatorComponent={this.renderSeparator}
                    />
                </View>
                <SectionHeader>Bars</SectionHeader>
                <View>
                    <FlatList
                        data = {this.state.inspection.Bars}
                        alwaysBounceVertical = {false}
                        renderItem={({item})=>(
                            <ExpandableBarCard 
                                key={item.BarInspectionId}
                                title={`Bar Number ${item.BarNumber}`}
                                content={{
                                    ContentsBroodDrone: parseInt(item.ContentsBroodDrone),
                                    ContentsBroodWorker: parseInt(item.ContentsBroodWorker),
                                    ContentsEggs: parseInt(item.ContentsEggs),
                                    ContentsHoney: parseInt(item.ContentsHoney),
                                    ContentsLarva: parseInt(item.ContentsLarva),
                                    ContentsNectar: parseInt(item.ContentsNectar),
                                    ContentsPollen: parseInt(item.ContentsPollen),
                                    AttachedComb: parseInt(item.AttachedComb),
                                    CombBuiltOut: parseInt(item.CombBuiltOut),
                                    CappedQueenCellCount: parseInt(item.CappedQueenCellCount),
                                    EmergedQueenCellCount: parseInt(item.EmergedQueenCellCount),
                                    Follower: parseInt(item.Follower),
                                    JoinedBar: item.JoinedBar,
                                    Spacer: item.Spacer,
                                    TeaCupCount: parseInt(item.TeaCupCount),
                                    UncappedQueenCellCount: parseInt(item.UncappedQueenCellCount),
                                    WavyComb: parseInt(item.WavyComb)
                                }}
                                barUpdate={({prop, value}) => {
                                    console.log(value, prop)
                                    let newBars = this.state.inspection.Bars.map(newBar=>{
                                        if(newBar.BarInspectionId == item.BarInspectionId){
                                            let barObject = { ...newBar, [prop]: value}
                                            return barObject;
                                        }
                                        else{
                                            return newBar;
                                        }
                                    });
                                    let newInspection = {
                                        ...this.state.inspection,
                                        Bars: newBars
                                        }
                                    this.setState({
                                        inspection: newInspection
                                    });
                                }}
                            />
                            )}
                        />
                </View>
                <SectionHeader>Feeder Jar</SectionHeader>
                <View style={insetContainerStyle}>
                    <View>
                        <Text style={labelStyle}>Initial Feeder Fluid</Text>
                        <TextInput
                            autoCorrect={false}
                            keyboardType='numeric'
                            value={this.state.inspection.InitialFeederFluid.toString()}
                            onChangeText={(value)=>{
                                this.setState({
                                    inspection:{
                                        ...this.state.inspection,
                                        InitialFeederFluid: parseInt(value) == NaN?0:value
                                    }
                                });
                            }}
                            style={inputStyle}
                            underlineColorAndroid = 'transparent'
                        />
                    </View>
                    <View>
                        <Text style={labelStyle}>Final Feeder Fluid</Text>
                        <TextInput
                            autoCorrect={false}
                            keyboardType='numeric'
                            value={this.state.inspection.FinalFeederFluid.toString()}
                            onChangeText={(value)=>{
                                this.setState({
                                    inspection:{
                                        ...this.state.inspection,
                                        FinalFeederFluid: parseInt(value) == NaN?0:value
                                    }
                                });
                            }}
                            style={inputStyle}
                            underlineColorAndroid = 'transparent'
                        />
                    </View>
                </View>
                <SectionHeader>Honey</SectionHeader>
                <View style={insetContainerStyle}>
                    <View>
                        <Text style={labelStyle}>Honey Harvested For Me</Text>
                        <TextInput
                            autoCorrect={false}
                            keyboardType='numeric'
                            value={this.state.inspection.HoneyHarvestedForMe.toString()}
                            onChangeText={(value)=>{
                                this.setState({
                                    inspection:{
                                        ...this.state.inspection,
                                        HoneyHarvestedForMe: parseInt(value) == NaN?0:value
                                    }
                                });
                            }}
                            style={inputStyle}
                            underlineColorAndroid = 'transparent'
                        />
                    </View>
                    <View>
                        <Text style={labelStyle}>Honey Harvested For Bees</Text>
                        <TextInput
                            autoCorrect={false}
                            keyboardType='numeric'
                            value={this.state.inspection.HoneyHarvestedForBees.toString()}
                            onChangeText={(value)=>{
                                this.setState({
                                    inspection:{
                                        ...this.state.inspection,
                                        HoneyHarvestedForBees: parseInt(value) == NaN?0:value
                                    }
                                });
                            }}
                            style={inputStyle}
                            underlineColorAndroid = 'transparent'
                        />
                    </View>
                </View>
                <Button color={PrimaryColor} height={40} onPress={this.submit.bind(this)}> Save </Button>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    marginTop: 10
  },
  insetContainerStyle: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  labelStyle: {
    fontSize: 12,
    color: DarkGrayColor,
    fontWeight: '200',
    flex: 1
  },
  errorViewStyle: {
    backgroundColor: DangerColor,
    borderRadius: 5,
    padding: 10,
    marginBottom: 10
  },
  errorTextStyle: {
    color: '#fff'
  },
  inputStyle: {
        paddingRight: 5,
        paddingLeft: 5,
        paddingBottom: 2,
        color: DarkGrayColor,
        fontSize: 18,
        fontWeight: '200',
        flex: 1,
        height: 40
    },
  inputDisplayOnlyStyle: {
      paddingRight: 5,
      paddingLeft: 5,
      color: DarkGrayColor,
      fontSize: 18,
      fontWeight: '200',
      flex: 1,
      height: 40,
  },
  displayOnlyContainer: {
    height: 45,
    flexDirection: 'column',
    width: '100%',
    borderColor: LightGrayColor,
    borderBottomWidth: 1,
    marginBottom: 10
  },
  radioListStyle: {
    marginBottom: 20
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5
  },
  listText: {
    alignSelf: 'center',
    fontSize: 16
  }
});
const mapStateToProps = (state) => {
    const { user } = state.auth;
    const { currentInspection } = state.display;
    return  {   user, 
                currentInspection
            };
}   

export default connect( mapStateToProps, {saveYardSettings, updateYardSetting, submitForm})(EditInspectionView);