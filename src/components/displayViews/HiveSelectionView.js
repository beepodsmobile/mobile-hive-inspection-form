import React, {Component} from 'react';
import {View} from 'react-native';
import { connect } from 'react-redux';
import {BottomNavBar, DisplayTiles, Button, Header, Spinner, SectionHeader} from '../common';
import axios from 'axios';
import { getHive, addHive, yardSettings, populateYardInfo} from '../../actions';
import { PrimaryColor, BeepodsGraySix } from '../../beePodsStyles.js';

class HiveSelectionView extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: [{'Email': "fail"}],
            settingsAction: () => {this.props.yardSettings({})}
        }
    }

    componentWillMount(){
        this.fetchYardInfo();
        let hiveOptions = this.props.yardHives.map(item =>{
                   let option = { };
                   option.key = parseInt(item['HiveId']);
                   option.display = item['HiveName'].substring(0, 20);
                   const hiveId = item['HiveId'];
                   const id = this.props.user['id'];
                   const hiveName = item['HiveName'];
                   option.action = () => {this.props.getHive({hiveId, id, hiveName})}
                   return option;
                });
        let addHiveOption = {
            key: 0,
            display: "+",
            action: () => this.props.addHive(this.props.user['id'])
        }
        hiveOptions.push(addHiveOption);
        this.setState( {
            data: hiveOptions
        });

    }
    fetchYardInfo(){
        for (var i = this.props.yards.length - 1; i >= 0; i--) {
            console.log('loop', this.props.yards[i], this.props.currentYardId);
            if(this.props.yards[i]['YardId'] === this.props.currentYardId){
                const currentYard = this.props.yards[i];
                this.props.populateYardInfo({currentYard});
            }
        }
    }
    renderList(){
        if (this.props.yardLoading){
            return <View style={{flex: .9}}><Spinner size = "large" /></View>
        }
        return (
            <DisplayTiles options={this.state.data} />
        );
    } 

    renderSettingsButton(){
        if(this.props.yardOwnerStatus){
            return<Button height = {50} onPress={this.state.settingsAction} color = {PrimaryColor}>Edit Yard</Button>
        }
    }
    
    render(){
        console.log(this.state.data);
        return (
            <View style={{flex:1}}>
                <SectionHeader> Hives in This Yard</SectionHeader>
                {this.renderList()}
                {this.renderSettingsButton()}
                <BottomNavBar activeTab={1}/>  
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { user } = state.auth;
    const {hiveLoading, yardHives, currentYardName, currentYardId, yardOwnerStatus, yards} = state.display
    return { user, hiveLoading, yardHives, currentYardName, currentYardId, yardOwnerStatus, yards };
};

export default connect( mapStateToProps, {getHive, addHive, yardSettings, populateYardInfo})(HiveSelectionView);