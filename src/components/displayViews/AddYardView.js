import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveNewYard , updateYardSetting } from '../../actions';
import { StyleSheet, Text, View, FlatList, ScrollView, Picker } from 'react-native';
import { ListButton, Button, SectionHeader, ManagedList, HorizontalRule, TitledInput } from '../common';
import { PrimaryColor, DangerColor, GrayColor, LightGrayColor, DarkGrayColor } from '../../beePodsStyles.js';
import axios from 'axios';

class AddYardView extends Component {
    constructor(props){
        super(props);
        this.save = this.save.bind(this);

    }
    save(){
        const user = this.props.user;
        const name = this.props.currentYardName;
        const description = this.props.currentYardDescription;
        const address = this.props.currentYardAddressLine;
        const city = this.props.currentYardAddressCity;
        const state = this.props.currentYardAddressState;
        const zip = this.props.currentYardAddressZip;
        const lat = this.props.currentYardLatitude;
        const lng = this.props.currentYardLongitude;

        this.props.saveNewYard({name, description, address, city, state, zip, lat, lng, user});
    }
    render(){
        const { containerStyle, insetContainerStyle, labelStyle } = styles;
    return (

        <ScrollView style={containerStyle}>
            <View style={insetContainerStyle}>
            <TitledInput
                label='Yard Name'
                value={this.props.currentYardName}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardName', value})}
            />
            <TitledInput
                label='Yard Description'
                value={this.props.currentYardDescription}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardDescription', value})}
            />
            <TitledInput
                label='Street Address'
                value={this.props.currentYardAddressLine}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressLine', value})}
            />
            <TitledInput
                label='City'
                value={this.props.currentYardAddressCity}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressCity', value})}
            />
            <TitledInput
                label='State'
                value={this.props.currentYardAddressState}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressState', value})}
            />         
            <TitledInput
                label='Zip Code'
                value={this.props.currentYardAddressZip}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardAddressZip', value})}
            />
            <TitledInput
                label='Latitude'
                value={this.props.currentYardLatitude}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardLatitude', value})}
            />         
            <TitledInput
                label='Longitude'
                value={this.props.currentYardLongitude}
                onChangeText={value=>this.props.updateYardSetting({prop:'currentYardLongitude', value})}
            />
            <Button color={PrimaryColor} onPress={this.save}>Save Yard</Button>
            </View>
        </ScrollView>
            );
    }
}
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    marginTop: 10
  },
  insetContainerStyle: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  labelStyle: {
    fontSize: 12,
    color: DarkGrayColor,
    fontWeight: '200',
    flex: 1
  },
  errorViewStyle: {
    backgroundColor: DangerColor,
    borderRadius: 5,
    padding: 10,
    marginBottom: 10
  },
  errorTextStyle: {
    color: '#fff'
  },
  inputDisplayOnlyStyle: {
      paddingRight: 5,
      paddingLeft: 5,
      color: DarkGrayColor,
      fontSize: 18,
      fontWeight: '200',
      flex: 1,
      height: 40,
  },
  displayOnlyContainer: {
    height: 45,
    flexDirection: 'column',
    width: '100%',
    borderColor: LightGrayColor,
    borderBottomWidth: 1,
    marginBottom: 10
  }
});

const mapStateToProps = (state) => {
    const { user } = state.auth;
    const { currentYardAddressLine, currentYardAddressCity, currentYardDescription, currentYardName, currentYardAddressState, currentYardAddressZip,currentYardLongitude, currentYardLatitude } = state.display;
    return { user, currentYardAddressLine, currentYardAddressCity, currentYardName, currentYardDescription, currentYardAddressState, currentYardAddressZip, currentYardLongitude, currentYardLatitude };
}   
export default connect( mapStateToProps, {saveNewYard, updateYardSetting})(AddYardView);