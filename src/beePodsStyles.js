

export const BeepodsTagLine = "Beekeeping for the Bees";



//COLORS
export const BeepodsGreen = "#74c176"; //not official beepods color
export const BeepodsLogoYellow = "#ffc265";
export const BeepodsLogoBlack = "#303030";
export const BeepodsPaleYellow = "#ffe5a4";
export const BeepodsTan = "#ffd48f";
export const BeepodsPaleOrange = "#ffad5a";
export const BeepodsOrange = "#ffc265";
export const BeepodsGraySix = "#a5a5a8";
export const BeepodsGrayFour = "#b9b8b9";
export const BeepodsGrayTwo = "#dbd9d6";



export const PrimaryColor = "#00addd";
export const SecondaryColor = "#8e8c84";
export const SuccessColor = "#93c457";
export const InfoColor = "#2a5e86";
export const WarningColor = "#fa7944";
export const DangerColor = "#de4f51";

export const DarkGrayColor = "#444444";
export const GrayColor = "#888888";
export const LightGrayColor = "#cccccc";
export const LighterGrayColor = "#e1e1e1";

//FONTS
export const BeepodsFont = "Exo 2";
export const BeepodsFontItalic = "Exo 2 Italic";
export const BeepodsFontBold = "Exo 2 Bold";
