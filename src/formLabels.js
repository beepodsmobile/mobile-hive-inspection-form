var Email_Key = "input_2";
var Yard_Key = "input_3";
var Hive_Key = "input_4";
var Date_Key = "input_6";
var Name_Key = "input_7";
var Guests_Key = "input_8";
var Temperature_Key = "input_9";
var AirPressure_Key = "input_10";
var Humidity_Key = "input_11";
var ExteriorActivity_Key = "input_13";
//value must be "Light" "Heavy" or "Moderate" 
var Bearding_Key = "input_14";
//value must be "True" or "False" (case should just be consistant across app)
var Gaurding_Key = "input_15";
//value must be "True" or "False" (case should just be consistant across app)
var Drones_Key = "input_16";
//value must be "True" or "False" (case should just be consistant across app)
var Robbers_Key = "input_17";
//value must be "True" or "False" (case should just be consistant across app)
var Entering_Key = "input_40";
//value must be "Entering More" "Exiting More" or "Same" 
var AlternateEntrances_Key = "input_20";
//value must be "True" or "False" (case should just be consistant across app)
var PollenCollection_Key = "input_21";
//value must be "True" or "False" (case should just be consistant across app)
var PollenColor_Key = "input_22";
var Entering_Key = "input_23";
//value must be "Side" "End" or "Both" 
var HiveCapacity_Key = "input_24";
//value must be "Full" "Partly Full" or "Sparse" 
var InteriorActivity_Key = "input_25";
//value must be "Active" "Docile" "Frenzied" or "Defensive" 
var NumberOfBarsWithCombAttachedToWalls_Key = "input_26";
var NumberOfBarsInInnerChamber_Key = "input_27";
var BeesFoundOnBars_Key = "input_28";
//value must be "True" or "False" (case should just be consistant across app)
var BeesFoundOnLids_Key = "input_29";
//value must be "True" or "False" (case should just be consistant across app)
var BeesFoundOnFloors_Key = "input_30";
//value must be "True" or "False" (case should just be consistant across app)
var DeadBeesFound_Key = "input_31";
//value must be "True" or "False" (case should just be consistant across app)
var CritterEvidence_Key = "input_33";
var BugsSeen_Key = "input_32";
var VentBoardPosition_Key = "input_34";
//value must be "Removed" "Fully Open" "Partly Open" or "Closed"

//Bar One
var BarOneCombBuiltOut_Key = "input_39";
//value must be string number 0-100 indicating percentage built out
var BarOneHoney_Key = "input_74";
//value must be string number 0-100 indicating percentage filled
var BarOneNectar_Key = "input_73";
//value must be string number 0-100 indicating percentage filled
var BarOnePollen_Key = "input_75";
//value must be string number 0-100 indicating percentage filled
var BarOneEggs_Key = "input_76";
//value must be string number 0-100 indicating percentage filled
var BarOneLarva_Key = "input_77";
//value must be string number 0-100 indicating percentage filled
var BarOneBrood_Key = "input_78";
//value must be string number 0-100 indicating percentage filled
var BarOneDroneBrood_Key = "input_79";
//value must be string number 0-100 indicating percentage filled
var BarOneSpacer_Key = "input_80";
//value must be "Left" "Right" or "Join"
var BarOneW_Key = "input_81";
//value must be "True" or "False" (case should just be consistant across app)
var BarOneA_Key = "input_82";
//value must be "True" or "False" (case should just be consistant across app)
var BarOneFollower_Key = "input_83";
//value must be "True" or "False" (case should just be consistant across app)
var BarOneTeaCupCount_Key = "input_84";
//value must be string number indicating count
var BarOneUncappedQueenCellCount_Key = "input_85";
//value must be string number indicating count
var BarOneCappedQueenCellCount_Key = "input_86";
//value must be string number indicating count
var BarOneEmergedQueenCellCount_Key = "input_87";
//value must be string number indicating count

//Bar Two
var BarTwoCombBuiltOut_Key = "input_42";
//value must be string number 0-100 indicating percentage built out
var BarTwoHoney_Key = "input_88";
//value must be string number 0-100 indicating percentage filled
var BarTwoNectar_Key = "input_98";
//value must be string number 0-100 indicating percentage filled
var BarTwoPollen_Key = "input_99";
//value must be string number 0-100 indicating percentage filled
var BarTwoEggs_Key = "input_100";
//value must be string number 0-100 indicating percentage filled
var BarTwoLarva_Key = "input_101";
//value must be string number 0-100 indicating percentage filled
var BarTwoBrood_Key = "input_102";
//value must be string number 0-100 indicating percentage filled
var BarTwoDroneBrood_Key = "input_111";
//value must be string number 0-100 indicating percentage filled
var BarTwoSpacer_Key = "input_103";
//value must be "Left" "Right" or "Join"
var BarTwoW_Key = "input_104";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwoA_Key = "input_105";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwoFollower_Key = "input_106";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwoTeaCupCount_Key = "input_107";
//value must be string number indicating count
var BarTwoUncappedQueenCellCount_Key = "input_110";
//value must be string number indicating count
var BarTwoCappedQueenCellCount_Key = "input_108";
//value must be string number indicating count
var BarTwoEmergedQueenCellCount_Key = "input_109";
//value must be string number indicat_Keying count

//Bar Three
var BarThreeCombBuiltOut_Key = "input_43";
//value must be string number 0-100 indicating percentage built out
var BarThreeHoney_Key = "input_97";
//value must be string number 0-100 indicating percentage filled
var BarThreeNectar_Key = "input_112";
//value must be string number 0-100 indicating percentage filled
var BarThreePollen_Key = "input_114";
//value must be string number 0-100 indicating percentage filled
var BarThreeEggs_Key = "input_115";
//value must be string number 0-100 indicating percentage filled
var BarThreeLarva_Key = "input_116";
//value must be string number 0-100 indicating percentage filled
var BarThreeBrood_Key = "input_117";
//value must be string number 0-100 indicating percentage filled
var BarThreeDroneBrood_Key = "input_118";
//value must be string number 0-100 indicating percentage filled
var BarThreeSpacer_Key = "input_119";
//value must be "Left" "Right" or "Join"
var BarThreeW_Key = "input_120";
//value must be "True" or "False" (case should just be consistant across app)
var BarThreeA_Key = "input_121";
//value must be "True" or "False" (case should just be consistant across app)
var BarThreeFollower_Key = "input_122";
//value must be "True" or "False" (case should just be consistant across app)
var BarThreeTeaCupCount_Key = "input_123";
//value must be string number indicating count
var BarThreeUncappedQueenCellCount_Key = "input_124";
//value must be string number indicating count
var BarThreeCappedQueenCellCount_Key = "input_125";
//value must be string number indicating count
var BarThreeEmergedQueenCellCount_Key = "input_126";
//value must be string number indicating count

//Bar Four
var BarFourCombBuiltOut_Key = "input_44";
//value must be string number 0-100 indicating percentage built out
var BarFourHoney_Key = "input_96";
//value must be string number 0-100 indicating percentage filled
var BarFourNectar_Key = "input_127";
//value must be string number 0-100 indicating percentage filled
var BarFourPollen_Key = "input_128";
//value must be string number 0-100 indicating percentage filled
var BarFourEggs_Key = "input_129";
//value must be string number 0-100 indicating percentage filled
var BarFourLarva_Key = "input_130";
//value must be string number 0-100 indicating percentage filled
var BarFourBrood_Key = "input_131";
//value must be string number 0-100 indicating percentage filled
var BarFourDroneBrood_Key = "input_132";
//value must be string number 0-100 indicating percentage filled
var BarFourSpacer_Key = "input_133";
//value must be "Left" "Right" or "Join"
var BarFourW_Key = "input_134";
//value must be "True" or "False" (case should just be consistant across app)
var BarFourA_Key = "input_135";
//value must be "True" or "False" (case should just be consistant across app)
var BarFourFollower_Key = "input_136";
//value must be "True" or "False" (case should just be consistant across app)
var BarFourTeaCupCount_Key = "input_137";
//value must be string number indicating count
var BarFourUncappedQueenCellCount_Key = "input_138";
//value must be string number indicating count
var BarFourCappedQueenCellCount_Key = "input_139";
//value must be string number indicating count
var BarFourEmergedQueenCellCount_Key = "input_140";
//value must be string number indicating count

//Bar Five
var BarFiveCombBuiltOut_Key = "input_45";
//value must be string number 0-100 indicating percentage built out
var BarFiveHoney_Key = "input_95";
//value must be string number 0-100 indicating percentage filled
var BarFiveNectar_Key = "input_141";
//value must be string number 0-100 indicating percentage filled
var BarFivePollen_Key = "input_144";
//value must be string number 0-100 indicating percentage filled
var BarFiveEggs_Key = "input_143";
//value must be string number 0-100 indicating percentage filled
var BarFiveLarva_Key = "input_144";
//value must be string number 0-100 indicating percentage filled
var BarFiveBrood_Key = "input_145";
//value must be string number 0-100 indicating percentage filled
var BarFiveDroneBrood_Key = "input_146";
//value must be string number 0-100 indicating percentage filled
var BarFiveSpacer_Key = "input_147";
//value must be "Left" "Right" or "Join"
var BarFiveW_Key = "input_148";
//value must be "True" or "False" (case should just be consistant across app)
var BarFiveA_Key = "input_149";
//value must be "True" or "False" (case should just be consistant across app)
var BarFiveFollower_Key = "input_150";
//value must be "True" or "False" (case should just be consistant across app)
var BarFiveTeaCupCount_Key = "input_151";
//value must be string number indicating count
var BarFiveUncappedQueenCellCount_Key = "input_152";
//value must be string number indicating count
var BarFiveCappedQueenCellCount_Key = "input_153";
//value must be string number indicating count
var BarFiveEmergedQueenCellCount_Key = "input_154";
//value must be string number indicating count

//Bar Six
var BarSixCombBuiltOut_Key = "input_46";
//value must be string number 0-100 indicating percentage built out
var BarSixHoney_Key = "input_94";
//value must be string number 0-100 indicating percentage filled
var BarSixNectar_Key = "input_156";
//value must be string number 0-100 indicating percentage filled
var BarSixPollen_Key = "input_157";
//value must be string number 0-100 indicating percentage filled
var BarSixEggs_Key = "input_158";
//value must be string number 0-100 indicating percentage filled
var BarSixLarva_Key = "input_159";
//value must be string number 0-100 indicating percentage filled
var BarSixBrood_Key = "input_160";
//value must be string number 0-100 indicating percentage filled
var BarSixDroneBrood_Key = "input_161";
//value must be string number 0-100 indicating percentage filled
var BarSixSpacer_Key = "input_162";
//value must be "Left" "Right" or "Join"
var BarSixW_Key = "input_163";
//value must be "True" or "False" (case should just be consistant across app)
var BarSixA_Key = "input_164";
//value must be "True" or "False" (case should just be consistant across app)
var BarSixFollower_Key = "input_165";
//value must be "True" or "False" (case should just be consistant across app)
var BarSixTeaCupCount_Key = "input_166";
//value must be string number indicating count
var BarSixUncappedQueenCellCount_Key = "input_167";
//value must be string number indicating count
var BarSixCappedQueenCellCount_Key = "input_168";
//value must be string number indicating count
var BarSixEmergedQueenCellCount_Key = "input_169";
//value must be string number indicating count

//Bar Seven
var BarSevenCombBuiltOut_Key = "input_47";
//value must be string number 0-100 indicating percentage built out
var BarSevenHoney_Key = "input_93";
//value must be string number 0-100 indicating percentage filled
var BarSevenNectar_Key = "input_170";
//value must be string number 0-100 indicating percentage filled
var BarSevenPollen_Key = "input_171";
//value must be string number 0-100 indicating percentage filled
var BarSevenEggs_Key = "input_172";
//value must be string number 0-100 indicating percentage filled
var BarSevenLarva_Key = "input_173";
//value must be string number 0-100 indicating percentage filled
var BarSevenBrood_Key = "input_174";
//value must be string number 0-100 indicating percentage filled
var BarSevenDroneBrood_Key = "input_175";
//value must be string number 0-100 indicating percentage filled
var BarSevenSpacer_Key = "input_176";
//value must be "Left" "Right" or "Join"
var BarSevenW_Key = "input_177";
//value must be "True" or "False" (case should just be consistant across app)
var BarSevenA_Key = "input_178";
//value must be "True" or "False" (case should just be consistant across app)
var BarSevenFollower_Key = "input_179";
//value must be "True" or "False" (case should just be consistant across app)
var BarSevenTeaCupCount_Key = "input_180";
//value must be string number indicating count
var BarSevenUncappedQueenCellCount_Key = "input_181";
//value must be string number indicating count
var BarSevenCappedQueenCellCount_Key = "input_182";
//value must be string number indicating count
var BarSevenEmergedQueenCellCount_Key = "input_183";
//value must be string number indicating count

//Bar Eight
var BarEightCombBuiltOut_Key = "input_48";
//value must be string number 0-100 indicating percentage built out
var BarEightHoney_Key = "input_92";
//value must be string number 0-100 indicating percentage filled
var BarEightNectar_Key = "input_184";
//value must be string number 0-100 indicating percentage filled
var BarEightPollen_Key = "input_185";
//value must be string number 0-100 indicating percentage filled
var BarEightEggs_Key = "input_186";
//value must be string number 0-100 indicating percentage filled
var BarEightLarva_Key = "input_187";
//value must be string number 0-100 indicating percentage filled
var BarEightBrood_Key = "input_188";
//value must be string number 0-100 indicating percentage filled
var BarEightDroneBrood_Key = "input_189";
//value must be string number 0-100 indicating percentage filled
var BarEightSpacer_Key = "input_190";
//value must be "Left" "Right" or "Join"
var BarEightW_Key = "input_191";
//value must be "True" or "False" (case should just be consistant across app)
var BarEightA_Key = "input_192";
//value must be "True" or "False" (case should just be consistant across app)
var BarEightFollower_Key = "input_193";
//value must be "True" or "False" (case should just be consistant across app)
var BarEightTeaCupCount_Key = "input_194";
//value must be string number indicating count
var BarEightUncappedQueenCellCount_Key = "input_195";
//value must be string number indicating count
var BarEightCappedQueenCellCount_Key = "input_196";
//value must be string number indicating count
var BarEightEmergedQueenCellCount_Key = "input_197";
//value must be string number indicating count

//Bar Nine
var BarNineCombBuiltOut_Key = "input_49";
//value must be string number 0-100 indicating percentage built out
var BarNineHoney_Key = "input_198";
//value must be string number 0-100 indicating percentage filled
var BarNineNectar_Key = "input_199";
//value must be string number 0-100 indicating percentage filled
var BarNinePollen_Key = "input_200";
//value must be string number 0-100 indicating percentage filled
var BarNineEggs_Key = "input_201";
//value must be string number 0-100 indicating percentage filled
var BarNineLarva_Key = "input_202";
//value must be string number 0-100 indicating percentage filled
var BarNineBrood_Key = "input_203";
//value must be string number 0-100 indicating percentage filled
var BarNineDroneBrood_Key = "input_204";
//value must be string number 0-100 indicating percentage filled
var BarNineSpacer_Key = "input_205";
//value must be "Left" "Right" or "Join"
var BarNineW_Key = "input_206";
//value must be "True" or "False" (case should just be consistant across app)
var BarNineA_Key = "input_207";
//value must be "True" or "False" (case should just be consistant across app)
var BarNineFollower_Key = "input_208";
//value must be "True" or "False" (case should just be consistant across app)
var BarNineTeaCupCount_Key = "input_209";
//value must be string number indicating count
var BarNineUncappedQueenCellCount_Key = "input_210";
//value must be string number indicating count
var BarNineCappedQueenCellCount_Key = "input_211";
//value must be string number indicating count
var BarNineEmergedQueenCellCount_Key = "input_212";
//value must be string number indicating count

//Bar Ten
var BarTenCombBuiltOut_Key = "input_50";
//value must be string number 0-100 indicating percentage built out
var BarTenHoney_Key = "input_213";
//value must be string number 0-100 indicating percentage filled
var BarTenNectar_Key = "input_214";
//value must be string number 0-100 indicating percentage filled
var BarTenPollen_Key = "input_215";
//value must be string number 0-100 indicating percentage filled
var BarTenEggs_Key = "input_216";
//value must be string number 0-100 indicating percentage filled
var BarTenLarva_Key = "input_217";
//value must be string number 0-100 indicating percentage filled
var BarTenBrood_Key = "input_218";
//value must be string number 0-100 indicating percentage filled
var BarTenDroneBrood_Key = "input_219";
//value must be string number 0-100 indicating percentage filled
var BarTenSpacer_Key = "input_220";
//value must be "Left" "Right" or "Join"
var BarTenW_Key = "input_221";
//value must be "True" or "False" (case should just be consistant across app)
var BarTenA_Key = "input_222";
//value must be "True" or "False" (case should just be consistant across app)
var BarTenFollower_Key = "input_223";
//value must be "True" or "False" (case should just be consistant across app)
var BarTenTeaCupCount_Key = "input_224";
//value must be string number indicating count
var BarTenUncappedQueenCellCount_Key = "input_225";
//value must be string number indicating count
var BarTenCappedQueenCellCount_Key = "input_226";
//value must be string number indicating count
var BarTenEmergedQueenCellCount_Key = "input_227";
//value must be string number indicating count

//Bar Eleven
var BarElevenCombBuiltOut_Key = "input_51";
//value must be string number 0-100 indicating percentage built out
var BarElevenHoney_Key = "input_228";
//value must be string number 0-100 indicating percentage filled
var BarElevenNectar_Key = "input_229";
//value must be string number 0-100 indicating percentage filled
var BarElevenPollen_Key = "input_230";
//value must be string number 0-100 indicating percentage filled
var BarElevenEggs_Key = "input_231";
//value must be string number 0-100 indicating percentage filled
var BarElevenLarva_Key = "input_232";
//value must be string number 0-100 indicating percentage filled
var BarElevenBrood_Key = "input_233";
//value must be string number 0-100 indicating percentage filled
var BarElevenDroneBrood_Key = "input_234";
//value must be string number 0-100 indicating percentage filled
var BarElevenSpacer_Key = "input_235";
//value must be "Left" "Right" or "Join"
var BarElevenW_Key = "input_236";
//value must be "True" or "False" (case should just be consistant across app)
var BarElevenA_Key = "input_237";
//value must be "True" or "False" (case should just be consistant across app)
var BarElevenFollower_Key = "input_238";
//value must be "True" or "False" (case should just be consistant across app)
var BarElevenTeaCupCount_Key = "input_239";
//value must be string number indicating count
var BarElevenUncappedQueenCellCount_Key = "input_240";
//value must be string number indicating count
var BarElevenCappedQueenCellCount_Key = "input_241";
//value must be string number indicating count
var BarElevenEmergedQueenCellCount_Key = "input_242";
//value must be string number indicating count

//Bar Twelve
var BarTwelveCombBuiltOut_Key = "input_52";
//value must be string number 0-100 indicating percentage built out
var BarTwelveHoney_Key = "input_243";
//value must be string number 0-100 indicating percentage filled
var BarTwelveNectar_Key = "input_244";
//value must be string number 0-100 indicating percentage filled
var BarTwelvePollen_Key = "input_245";
//value must be string number 0-100 indicating percentage filled
var BarTwelveEggs_Key = "input_246";
//value must be string number 0-100 indicating percentage filled
var BarTwelveLarva_Key = "input_247";   
//value must be string number 0-100 indicating percentage filled
var BarTwelveBrood_Key = "input_248";
//value must be string number 0-100 indicating percentage filled
var BarTwelveDroneBrood_Key = "input_249";
//value must be string number 0-100 indicating percentage filled
var BarTwelveSpacer_Key = "input_250";
//value must be "Left" "Right" or "Join"
var BarTwelveW_Key = "input_251";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwelveA_Key = "input_252";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwelveFollower_Key = "input_253";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwelveTeaCupCount_Key = "input_254";
//value must be string number indicating count
var BarTwelveUncappedQueenCellCount_Key = "input_255";
//value must be string number indicating count
var BarTwelveCappedQueenCellCount_Key = "input_256";
//value must be string number indicating count
var BarTwelveEmergedQueenCellCount_Key = "input_257";
//value must be string number indicating count

//Bar Thirteen
var BarThirteenCombBuiltOut_Key = "input_53";
//value must be string number 0-100 indicating percentage built out
var BarThirteenHoney_Key = "input_258";
//value must be string number 0-100 indicating percentage filled
var BarThirteenNectar_Key = "input_259";
//value must be string number 0-100 indicating percentage filled
var BarThirteenPollen_Key = "input_260";
//value must be string number 0-100 indicating percentage filled
var BarThirteenEggs_Key = "input_261";
//value must be string number 0-100 indicating percentage filled
var BarThirteenLarva_Key = "input_262";   
//value must be string number 0-100 indicating percentage filled
var BarThirteenBrood_Key = "input_263";
//value must be string number 0-100 indicating percentage filled
var BarThirteenDroneBrood_Key = "input_264";
//value must be string number 0-100 indicating percentage filled
var BarThirteenSpacer_Key = "input_265";
//value must be "Left" "Right" or "Join"
var BarThirteenW_Key = "input_266";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirteenA_Key = "input_267";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirteenFollower_Key = "input_268";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirteenTeaCupCount_Key = "input_269";
//value must be string number indicating count
var BarThirteenUncappedQueenCellCount_Key = "input_270";
//value must be string number indicating count
var BarThirteenCappedQueenCellCount_Key = "input_271";
//value must be string number indicating count
var BarThirteenEmergedQueenCellCount_Key = "input_272";
//value must be string number indicating count

//Bar Fourteen
var BarFourteenCombBuiltOut_Key = "input_54";
//value must be string number 0-100 indicating percentage built out
var BarFourteenHoney_Key = "input_273";
//value must be string number 0-100 indicating percentage filled
var BarFourteenNectar_Key = "input_274";
//value must be string number 0-100 indicating percentage filled
var BarFourteenPollen_Key = "input_275";
//value must be string number 0-100 indicating percentage filled
var BarFourteenEggs_Key = "input_276";
//value must be string number 0-100 indicating percentage filled
var BarFourteenLarva_Key = "input_277";   
//value must be string number 0-100 indicating percentage filled
var BarFourteenBrood_Key = "input_278";
//value must be string number 0-100 indicating percentage filled
var BarFourteenDroneBrood_Key = "input_279";
//value must be string number 0-100 indicating percentage filled
var BarFourteenSpacer_Key = "input_280";
//value must be "Left" "Right" or "Join"
var BarFourteenW_Key = "input_281";
//value must be "True" or "False" (case should just be consistant across app)
var BarFourteenA_Key = "input_282";
//value must be "True" or "False" (case should just be consistant across app)
var BarFourteenFollower_Key = "input_283";
//value must be "True" or "False" (case should just be consistant across app)
var BarFourteenTeaCupCount_Key = "input_284";
//value must be string number indicating count
var BarFourteenUncappedQueenCellCount_Key = "input_285";
//value must be string number indicating count
var BarFourteenCappedQueenCellCount_Key = "input_286";
//value must be string number indicating count
var BarFourteenEmergedQueenCellCount_Key = "input_287";
//value must be string number indicating count

//Bar Fifteen
var BarFifteenCombBuiltOut_Key = "input_55";
//value must be string number 0-100 indicating percentage built out
var BarFifteenHoney_Key = "input_288";
//value must be string number 0-100 indicating percentage filled
var BarFifteenNectar_Key = "input_289";
//value must be string number 0-100 indicating percentage filled
var BarFifteenPollen_Key = "input_290";
//value must be string number 0-100 indicating percentage filled
var BarFifteenEggs_Key = "input_291";
//value must be string number 0-100 indicating percentage filled
var BarFifteenLarva_Key = "input_292";   
//value must be string number 0-100 indicating percentage filled
var BarFifteenBrood_Key = "input_293";
//value must be string number 0-100 indicating percentage filled
var BarFifteenDroneBrood_Key = "input_294";
//value must be string number 0-100 indicating percentage filled
var BarFifteenSpacer_Key = "input_295";
//value must be "Left" "Right" or "Join"
var BarFifteenW_Key = "input_296";
//value must be "True" or "False" (case should just be consistant across app)
var BarFifteenA_Key = "input_297";
//value must be "True" or "False" (case should just be consistant across app)
var BarFifteenFollower_Key = "input_298";
//value must be "True" or "False" (case should just be consistant across app)
var BarFifteenTeaCupCount_Key = "input_299";
//value must be string number indicating count
var BarFifteenUncappedQueenCellCount_Key = "input_300";
//value must be string number indicating count
var BarFifteenCappedQueenCellCount_Key = "input_301";
//value must be string number indicating count
var BarFifteenEmergedQueenCellCount_Key = "input_302";
//value must be string number indicating count

//Bar Sixteen
var BarSixteenCombBuiltOut_Key = "input_56";
//value must be string number 0-100 indicating percentage built out
var BarSixteenHoney_Key = "input_303";
//value must be string number 0-100 indicating percentage filled
var BarSixteenNectar_Key = "input_304";
//value must be string number 0-100 indicating percentage filled
var BarSixteenPollen_Key = "input_305";
//value must be string number 0-100 indicating percentage filled
var BarSixteenEggs_Key = "input_306";
//value must be string number 0-100 indicating percentage filled
var BarSixteenLarva_Key = "input_307";   
//value must be string number 0-100 indicating percentage filled
var BarSixteenBrood_Key = "input_308";
//value must be string number 0-100 indicating percentage filled
var BarSixteenDroneBrood_Key = "input_309";
//value must be string number 0-100 indicating percentage filled
var BarSixteenSpacer_Key = "input_310";
//value must be "Left" "Right" or "Join"
var BarSixteenW_Key = "input_311";
//value must be "True" or "False" (case should just be consistant across app)
var BarSixteenA_Key = "input_312";
//value must be "True" or "False" (case should just be consistant across app)
var BarSixteenFollower_Key = "input_313";
//value must be "True" or "False" (case should just be consistant across app)
var BarSixteenTeaCupCount_Key = "input_314";
//value must be string number indicating count
var BarSixteenUncappedQueenCellCount_Key = "input_315";
//value must be string number indicating count
var BarSixteenCappedQueenCellCount_Key = "input_316";
//value must be string number indicating count
var BarSixteenEmergedQueenCellCount_Key = "input_317";
//value must be string number indicating count

//Bar Seventeen
var BarSeventeenCombBuiltOut_Key = "input_57";
//value must be string number 0-100 indicating percentage built out
var BarSeventeenHoney_Key = "input_318";
//value must be string number 0-100 indicating percentage filled
var BarSeventeenNectar_Key = "input_335";
//value must be string number 0-100 indicating percentage filled
var BarSeventeenPollen_Key = "input_320";
//value must be string number 0-100 indicating percentage filled
var BarSeventeenEggs_Key = "input_322";
//value must be string number 0-100 indicating percentage filled
var BarSevenTeenLarva_Key = "input_323";   
//value must be string number 0-100 indicating percentage filled
var BarSeventeenBrood_Key = "input_324";
//value must be string number 0-100 indicating percentage filled
var BarSeventeenDroneBrood_Key = "input_325";
//value must be string number 0-100 indicating percentage filled
var BarSeventeenSpacer_Key = "input_326";
//value must be "Left" "Right" or "Join"
var BarSeventeenW_Key = "input_327";
//value must be "True" or "False" (case should just be consistant across app)
var BarSeventeenA_Key = "input_328";
//value must be "True" or "False" (case should just be consistant across app)
var BarSeventeenFollower_Key = "input_329";
//value must be "True" or "False" (case should just be consistant across app)
var BarSeventeenTeaCupCount_Key = "input_330";
//value must be string number indicating count
var BarSeventeenUncappedQueenCellCount_Key = "input_331";
//value must be string number indicating count
var BarSeventeenCappedQueenCellCount_Key = "input_332";
//value must be string number indicating count
var BarSeventeenEmergedQueenCellCount_Key = "input_333";
//value must be string number indicating count

//Bar Eighteen
var BarEighteenCombBuiltOut_Key = "input_58";
//value must be string number 0-100 indicating percentage built out
var BarEighteenHoney_Key = "input_349";
//value must be string number 0-100 indicating percentage filled
var BarEighteenNectar_Key = "input_555";
//value must be string number 0-100 indicating percentage filled
var BarEighteenPollen_Key = "input_376";
//value must be string number 0-100 indicating percentage filled
var BarEighteenEggs_Key = "input_556";
//value must be string number 0-100 indicating percentage filled
var BarEighTeenLarva_Key = "input_403";   
//value must be string number 0-100 indicating percentage filled
var BarEighteenBrood_Key = "input_417";
//value must be string number 0-100 indicating percentage filled
var BarEighteenDroneBrood_Key = "input_431";
//value must be string number 0-100 indicating percentage filled
var BarEighteenSpacer_Key = "input_445";
//value must be "Left" "Right" or "Join"
var BarEighteenW_Key = "input_557";
//value must be "True" or "False" (case should just be consistant across app)
var BarEighteenA_Key = "input_473";
//value must be "True" or "False" (case should just be consistant across app)
var BarEighteenFollower_Key = "input_486";
//value must be "True" or "False" (case should just be consistant across app)
var BarEighteenTeaCupCount_Key = "input_500";
//value must be string number indicating count
var BarEighteenUncappedQueenCellCount_Key = "input_514";
//value must be string number indicating count
var BarEighteenCappedQueenCellCount_Key = "input_558";
//value must be string number indicating count
var BarEighteenEmergedQueenCellCount_Key = "input_541";
//value must be string number indicating count

//Bar Nineteen
var BarNineteenCombBuiltOut_Key = "input_59";
//value must be string number 0-100 indicating percentage built out
var BarNineteenHoney_Key = "input_334";
//value must be string number 0-100 indicating percentage filled
var BarNineteenNectar_Key = "input_319";
//value must be string number 0-100 indicating percentage filled
var BarNineteenPollen_Key = "input_336";
//value must be string number 0-100 indicating percentage filled
var BarNineteenEggs_Key = "input_337";
//value must be string number 0-100 indicating percentage filled
var BarNineTeenLarva_Key = "input_338";   
//value must be string number 0-100 indicating percentage filled
var BarNineteenBrood_Key = "input_339";
//value must be string number 0-100 indicating percentage filled
var BarNineteenDroneBrood_Key = "input_340";
//value must be string number 0-100 indicating percentage filled
var BarNineteenSpacer_Key = "input_341";
//value must be "Left" "Right" or "Join"
var BarNineteenW_Key = "input_342";
//value must be "True" or "False" (case should just be consistant across app)
var BarNineteenA_Key = "input_343";
//value must be "True" or "False" (case should just be consistant across app)
var BarNineteenFollower_Key = "input_344";
//value must be "True" or "False" (case should just be consistant across app)
var BarNineteenTeaCupCount_Key = "input_345";
//value must be string number indicating count
var BarNineteenUncappedQueenCellCount_Key = "input_346";
//value must be string number indicating count
var BarNineteenCappedQueenCellCount_Key = "input_347";
//value must be string number indicating count
var BarNineteenEmergedQueenCellCount_Key = "input_348";
//value must be string number indicating count

//Bar Twenty
var BarTwentyCombBuiltOut_Key = "input_60";
//value must be string number 0-100 indicating percentage built out
var BarTwentyHoney_Key = "input_362";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNectar_Key = "input_375";
//value must be string number 0-100 indicating percentage filled
var BarTwentyPollen_Key = "input_389";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEggs_Key = "input_402";
//value must be string number 0-100 indicating percentage filled
var BarTwentyLarva_Key = "input_416";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyBrood_Key = "input_430";
//value must be string number 0-100 indicating percentage filled
var BarTwentyDroneBrood_Key = "input_444";
//value must be string number 0-100 indicating percentage filled
var BarTwentySpacer_Key = "input_458";
//value must be "Left" "Right" or "Join"
var BarTwentyW_Key = "input_471";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyA_Key = "input_485";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFollower_Key = "input_499";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyTeaCupCount_Key = "input_513";
//value must be string number indicating count
var BarTwentyUncappedQueenCellCount_Key = "input_527";
//value must be string number indicating count
var BarTwentyCappedQueenCellCount_Key = "input_540";
//value must be string number indicating count
var BarTwentyEmergedQueenCellCount_Key = "input_554";
//value must be string number indicating count

//Bar TwentyOne
var BarTwentyOneCombBuiltOut_Key = "input_61";
//value must be string number 0-100 indicating percentage built out
var BarTwentyOneHoney_Key = "input_361";
//value must be string number 0-100 indicating percentage filled
var BarTwentyOneNectar_Key = "input_374";
//value must be string number 0-100 indicating percentage filled
var BarTwentyOnePollen_Key = "input_388";
//value must be string number 0-100 indicating percentage filled
var BarTwentyOneEggs_Key = "input_401";
//value must be string number 0-100 indicating percentage filled
var BarTwentyOneLarva_Key = "input_415";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyOneBrood_Key = "input_429";
//value must be string number 0-100 indicating percentage filled
var BarTwentyOneDroneBrood_Key = "input_443";
//value must be string number 0-100 indicating percentage filled
var BarTwentyOneSpacer_Key = "input_457";
//value must be "Left" "Right" or "Join"
var BarTwentyOneW_Key = "input_470";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyOneA_Key = "input_484";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyOneFollower_Key = "input_498";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyOneTeaCupCount_Key = "input_512";
//value must be string number indicating count
var BarTwentyOneUncappedQueenCellCount_Key = "input_526";
//value must be string number indicating count
var BarTwentyOneCappedQueenCellCount_Key = "input_539";
//value must be string number indicating count
var BarTwentyOneEmergedQueenCellCount_Key = "input_553";
//value must be string number indicating count

//Bar TwentyTwo
var BarTwentyTwoCombBuiltOut_Key = "input_62";
//value must be string number 0-100 indicating percentage built out
var BarTwentyTwoHoney_Key = "input_360";
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoNectar_Key = "input_373";
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoPollen_Key = "input_387";
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoEggs_Key = "input_400";
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoLarva_Key = "input_414";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoBrood_Key = "input_428";
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoDroneBrood_Key = "input_442";
//value must be string number 0-100 indicating percentage filled
var BarTwentyTwoSpacer_Key = "input_456";
//value must be "Left" "Right" or "Join"
var BarTwentyTwoW_Key = "input_469";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyTwoA_Key = "input_483";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyTwoFollower_Key = "input_497";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyTwoTeaCupCount_Key = "input_511";
//value must be string number indicating count
var BarTwentyTwoUncappedQueenCellCount_Key = "input_525";
//value must be string number indicating count
var BarTwentyTwoCappedQueenCellCount_Key = "input_538";
//value must be string number indicating count
var BarTwentyTwoEmergedQueenCellCount_Key = "input_552";
//value must be string number indicating count

//Bar TwentyThree
var BarTwentyThreeCombBuiltOut_Key = "input_63";
//value must be string number 0-100 indicating percentage built out
var BarTwentyThreeHoney_Key = "input_359";
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreeNectar_Key = "input_372";
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreePollen_Key = "input_386";
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreeEggs_Key = "input_399";
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreeLarva_Key = "input_413";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreeBrood_Key = "input_427";
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreeDroneBrood_Key = "input_441";
//value must be string number 0-100 indicating percentage filled
var BarTwentyThreeSpacer_Key = "input_455";
//value must be "Left" "Right" or "Join"
var BarTwentyThreeW_Key = "input_468";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyThreeA_Key = "input_482";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyThreeFollower_Key = "input_496";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyThreeTeaCupCount_Key = "input_510";
//value must be string number indicating count
var BarTwentyThreeUncappedQueenCellCount_Key = "input_524";
//value must be string number indicating count
var BarTwentyThreeCappedQueenCellCount_Key = "input_537";
//value must be string number indicating count
var BarTwentyThreeEmergedQueenCellCount_Key = "input_551";
//value must be string number indicating count

//Bar TwentyFour
var BarTwentyFourCombBuiltOut_Key = "input_64";
//value must be string number 0-100 indicating percentage built out
var BarTwentyFourHoney_Key = "input_358";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourNectar_Key = "input_371";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourPollen_Key = "input_385";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourEggs_Key = "input_398";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourLarva_Key = "input_412";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourBrood_Key = "input_426";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourDroneBrood_Key = "input_440";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFourSpacer_Key = "input_454";
//value must be "Left" "Right" or "Join"
var BarTwentyFourW_Key = "input_467";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFourA_Key = "input_481";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFourFollower_Key = "input_495";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFourTeaCupCount_Key = "input_509";
//value must be string number indicating count
var BarTwentyFourUncappedQueenCellCount_Key = "input_523";
//value must be string number indicating count
var BarTwentyFourCappedQueenCellCount_Key = "input_536";
//value must be string number indicating count
var BarTwentyFourEmergedQueenCellCount_Key = "input_550";
//value must be string number indicating count

//Bar TwentyFive
var BarTwentyFiveCombBuiltOut_Key = "input_65";
//value must be string number 0-100 indicating percentage built out
var BarTwentyFiveHoney_Key = "input_357";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFiveNectar_Key = "input_370";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFivePollen_Key = "input_384";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFiveEggs_Key = "input_397";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFiveLarva_Key = "input_410";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyFiveBrood_Key = "input_425";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFiveDroneBrood_Key = "input_439";
//value must be string number 0-100 indicating percentage filled
var BarTwentyFiveSpacer_Key = "input_453";
//value must be "Left" "Right" or "Join"
var BarTwentyFiveW_Key = "input_465";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFiveA_Key = "input_480";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFiveFollower_Key = "input_494";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyFiveTeaCupCount_Key = "input_508";
//value must be string number indicating count
var BarTwentyFiveUncappedQueenCellCount_Key = "input_522";
//value must be string number indicating count
var BarTwentyFiveCappedQueenCellCount_Key = "input_535";
//value must be string number indicating count
var BarTwentyFiveEmergedQueenCellCount_Key = "input_549";
//value must be string number indicating count

//Bar TwentySix
var BarTwentySixCombBuiltOut_Key = "input_66";
//value must be string number 0-100 indicating percentage built out
var BarTwentySixHoney_Key = "input_356";
//value must be string number 0-100 indicating percentage filled
var BarTwentySixNectar_Key = "input_369";
//value must be string number 0-100 indicating percentage filled
var BarTwentySixPollen_Key = "input_383";
//value must be string number 0-100 indicating percentage filled
var BarTwentySixEggs_Key = "input_396";
//value must be string number 0-100 indicating percentage filled
var BarTwentySixLarva_Key = "input_411";   
//value must be string number 0-100 indicating percentage filled
var BarTwentySixBrood_Key = "input_424";
//value must be string number 0-100 indicating percentage filled
var BarTwentySixDroneBrood_Key = "input_438";
//value must be string number 0-100 indicating percentage filled
var BarTwentySixSpacer_Key = "input_452";
//value must be "Left" "Right" or "Join"
var BarTwentySixW_Key = "input_466";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentySixA_Key = "input_479";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentySixFollower_Key = "input_493";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentySixTeaCupCount_Key = "input_507";
//value must be string number indicating count
var BarTwentySixUncappedQueenCellCount_Key = "input_521";
//value must be string number indicating count
var BarTwentySixCappedQueenCellCount_Key = "input_534";
//value must be string number indicating count
var BarTwentySixEmergedQueenCellCount_Key = "input_548";
//value must be string number indicating count

//Bar TwentySeven
var BarTwentySevenCombBuiltOut_Key = "input_67";
//value must be string number 0-100 indicating percentage built out
var BarTwentySevenHoney_Key = "input_355";
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenNectar_Key = "input_368";
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenPollen_Key = "input_382";
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenEggs_Key = "input_395";
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenLarva_Key = "input_409";   
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenBrood_Key = "input_423";
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenDroneBrood_Key = "input_437";
//value must be string number 0-100 indicating percentage filled
var BarTwentySevenSpacer_Key = "input_451";
//value must be "Left" "Right" or "Join"
var BarTwentySevenW_Key = "input_464";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentySevenA_Key = "input_478";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentySevenFollower_Key = "input_492";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentySevenTeaCupCount_Key = "input_506";
//value must be string number indicating count
var BarTwentySevenUncappedQueenCellCount_Key = "input_520";
//value must be string number indicating count
var BarTwentySevenCappedQueenCellCount_Key = "input_533";
//value must be string number indicating count
var BarTwentySevenEmergedQueenCellCount_Key = "input_547";
//value must be string number indicating count

//Bar TwentyEight
var BarTwentyEightCombBuiltOut_Key = "input_68";
//value must be string number 0-100 indicating percentage built out
var BarTwentyEightHoney_Key = "input_354";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightNectar_Key = "input_367";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightPollen_Key = "input_381";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightEggs_Key = "input_394";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightLarva_Key = "input_408";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightBrood_Key = "input_422";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightDroneBrood_Key = "input_436";
//value must be string number 0-100 indicating percentage filled
var BarTwentyEightSpacer_Key = "input_450";
//value must be "Left" "Right" or "Join"
var BarTwentyEightW_Key = "input_463";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyEightA_Key = "input_477";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyEightFollower_Key = "input_491";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyEightTeaCupCount_Key = "input_505";
//value must be string number indicating count
var BarTwentyEightUncappedQueenCellCount_Key = "input_519";
//value must be string number indicating count
var BarTwentyEightCappedQueenCellCount_Key = "input_532";
//value must be string number indicating count
var BarTwentyEightEmergedQueenCellCount_Key = "input_546";
//value must be string number indicating count

//Bar TwentyNine
var BarTwentyNineCombBuiltOut_Key = "input_69";
//value must be string number 0-100 indicating percentage built out
var BarTwentyNineHoney_Key = "input_353";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNineNectar_Key = "input_366";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNinePollen_Key = "input_380";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNineEggs_Key = "input_393";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNineLarva_Key = "input_407";   
//value must be string number 0-100 indicating percentage filled
var BarTwentyNineBrood_Key = "input_421";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNineDroneBrood_Key = "input_435";
//value must be string number 0-100 indicating percentage filled
var BarTwentyNineSpacer_Key = "input_449";
//value must be "Left" "Right" or "Join"
var BarTwentyNineW_Key = "input_462";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentNineA_Key = "input_476";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyNineFollower_Key = "input_490";
//value must be "True" or "False" (case should just be consistant across app)
var BarTwentyNineTeaCupCount_Key = "input_504";
//value must be string number indicating count
var BarTwentyNineUncappedQueenCellCount_Key = "input_518";
//value must be string number indicating count
var BarTwentyNineCappedQueenCellCount_Key = "input_531";
//value must be string number indicating count
var BarTwentyNineEmergedQueenCellCount_Key = "input_545";
//value must be string number indicating count

//Bar Thirty
var BarThirtyCombBuiltOut_Key = "input_70";
//value must be string number 0-100 indicating percentage built out
var BarThirtyHoney_Key = "input_352";
//value must be string number 0-100 indicating percentage filled
var BarThirtyNectar_Key = "input_365";
//value must be string number 0-100 indicating percentage filled
var BarThirtyPollen_Key = "input_379";
//value must be string number 0-100 indicating percentage filled
var BarThirtyEggs_Key = "input_392";
//value must be string number 0-100 indicating percentage filled
var BarThirtyLarva_Key = "input_406";   
//value must be string number 0-100 indicating percentage filled
var BarThirtyBrood_Key = "input_420";
//value must be string number 0-100 indicating percentage filled
var BarThirtyDroneBrood_Key = "input_434";
//value must be string number 0-100 indicating percentage filled
var BarThirtySpacer_Key = "input_448";
//value must be "Left" "Right" or "Join"
var BarThirtyW_Key = "input_461";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyA_Key = "input_475";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyFollower_Key = "input_489";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyTeaCupCount_Key = "input_503";
//value must be string number indicating count
var BarThirtyUncappedQueenCellCount_Key = "input_517";
//value must be string number indicating count
var BarThirtyCappedQueenCellCount_Key = "input_530";
//value must be string number indicating count
var BarThirtyEmergedQueenCellCount_Key = "input_544";
//value must be string number indicating count

//Bar ThirtyOne
var BarThirtyOneCombBuiltOut_Key = "input_71";
//value must be string number 0-100 indicating percentage built out
var BarThirtyOneHoney_Key = "input_351";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneNectar_Key = "input_364";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOnePollen_Key = "input_378";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneEggs_Key = "input_391";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneLarva_Key = "input_405";   
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneBrood_Key = "input_419";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneDroneBrood_Key = "input_433";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneSpacer_Key = "input_447";
//value must be "Left" "Right" or "Join"
var BarThirtyOneW_Key = "input_460";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyOneA_Key = "input_474";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyOneFollower_Key = "input_488";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyOneTeaCupCount_Key = "input_502";
//value must be string number indicating count
var BarThirtyOneUncappedQueenCellCount_Key = "input_516";
//value must be string number indicating count
var BarThirtyOneCappedQueenCellCount_Key = "input_528";
//value must be string number indicating count
var BarThirtyOneEmergedQueenCellCount_Key = "input_543";
//value must be string number indicating count

//Bar ThirtyOne
var BarThirtyOneCombBuiltOut_Key = "input_71";
//value must be string number 0-100 indicating percentage built out
var BarThirtyOneHoney_Key = "input_351";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneNectar_Key = "input_364";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOnePollen_Key = "input_378";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneEggs_Key = "input_391";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneLarva_Key = "input_405";   
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneBrood_Key = "input_419";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneDroneBrood_Key = "input_433";
//value must be string number 0-100 indicating percentage filled
var BarThirtyOneSpacer_Key = "input_447";
//value must be "Left" "Right" or "Join"
var BarThirtyOneW_Key = "input_460";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyOneA_Key = "input_474";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyOneFollower_Key = "input_488";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyOneTeaCupCount_Key = "input_502";
//value must be string number indicating count
var BarThirtyOneUncappedQueenCellCount_Key = "input_516";
//value must be string number indicating count
var BarThirtyOneCappedQueenCellCount_Key = "input_528";
//value must be string number indicating count
var BarThirtyOneEmergedQueenCellCount_Key = "input_543";
//value must be string number indicating count

//Bar ThirtyTwo
var BarThirtyTwoCombBuiltOut_Key = "input_72";
//value must be string number 0-100 indicating percentage built out
var BarThirtyTwoHoney_Key = "input_350";
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoNectar_Key = "input_363";
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoPollen_Key = "input_377";
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoEggs_Key = "input_390";
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoLarva_Key = "input_404";   
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoBrood_Key = "input_418";
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoDroneBrood_Key = "input_432";
//value must be string number 0-100 indicating percentage filled
var BarThirtyTwoSpacer_Key = "input_446";
//value must be "Left" "Right" or "Join"
var BarThirtyTwoW_Key = "input_459";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyTwoA_Key = "input_472";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyTwoFollower_Key = "input_487";
//value must be "True" or "False" (case should just be consistant across app)
var BarThirtyTwoTeaCupCount_Key = "input_501";
//value must be string number indicating count
var BarThirtyTwoUncappedQueenCellCount_Key = "input_515";
//value must be string number indicating count
var BarThirtyTwoCappedQueenCellCount_Key = "input_529";
//value must be string number indicating count
var BarThirtyTwoEmergedQueenCellCount_Key = "input_544";
//value must be string number indicating count


var QueenCellContent_Key = "input_559";
//value must be "Eggs" "Royal Jelly" "Larva" "Capped" or "Emerged" 
var MiteCheckPerformed_Key = "input_560";
//value must be "True" or "False" (case should just be consistant across app)
var ShouldHiveSplit_Key = "input_561";
//value must be "True" or "False" (case should just be consistant across app)            
var ActionsTaken_Key = "input_562";
//value can be any string (it is just a summary input)
var ActionsRequired_Key = "input_563";
//value can be any string (it is just a summary input)           
var NextTimeBring_Key = "input_564";
//value can be any string (it is just a summary input)
var JarFluidIntial_Key = "input_565";
//value can be any Number string (TBD)
var JarFluidFinal_Key = "input_566";
//value can be any Number string (TBD)
var FinalNumberOfBars_Key = "input_568";
//value can be any Number String between 1 and 32                
var HoneyHarvestedForBees_Key = "input_569";
//value can be any string (it is just a summary input)
var HoneyHarvestedForMe_Key = "input_570";
//value can be any string (it is just a summary input)                 
              