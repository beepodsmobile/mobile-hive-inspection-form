import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import {StyleSheet} from 'react-native';
import AddYardView from './components/displayViews/AddYardView';
import AddHiveView from './components/displayViews/AddHiveView';
import BarQuestion from './components/formViews/questions/BarQuestion';
import BeesFound from './components/formViews/questions/BeesFound';
import ColonySplit from './components/formViews/questions/ColonySplit';
import EditInspectionView from './components/displayViews/EditInspectionView';
import EntranceActivity from './components/formViews/questions/EntranceActivity';
import EntrancesUsed from './components/formViews/questions/EntrancesUsed';
import ExternalBeeActivityLevel from './components/formViews/questions/ExternalBeeActivityLevel';
import ExternalVisualCues from './components/formViews/questions/ExternalVisualCues';
import FormStart from './components/formViews/FormStart';
import FormEnd from './components/formViews/FormEnd';
import FinalJarFluid from './components/formViews/questions/FinalJarFluid';
import FinalNumberOfBars from './components/formViews/questions/FinalNumberOfBars';
import HiveCapacity from './components/formViews/questions/HiveCapacity';
import HoneyHarvestedForBees from './components/formViews/questions/HoneyHarvestedForBees';
import HoneyHarvestedForMe from './components/formViews/questions/HoneyHarvestedForMe';
import InternalActivity from './components/formViews/questions/InternalActivity';
import InitialJarFluid from './components/formViews/questions/InitialJarFluid';
import LoginForm from './components/authentication/LoginForm';
import MiteCheckPerformed from './components/formViews/questions/MiteCheckPerformed';
import NumberBarsCombsAttached from './components/formViews/questions/NumberBarsCombsAttached';
import NumberBarsInnerChamber from './components/formViews/questions/NumberBarsInnerChamber';
import PollenColor from './components/formViews/questions/PollenColor';
import PollenCollectionVisible from './components/formViews/questions/PollenCollectionVisible';
import VentBoardPosition from './components/formViews/questions/VentBoardPosition';
import YardSelectionView from './components/displayViews/YardSelectionView';
import YardFormSelection from './components/formViews/YardFormSelection';
import YardSettings from './components/displayViews/YardSettings';
import HiveSelectionView from './components/displayViews/HiveSelectionView';
import HiveFormSelection from './components/formViews/HiveFormSelection';
import HiveInspectionView from './components/displayViews/HiveInspectionView';
import HiveSettings from './components/displayViews/HiveSettings';
import Settings from './components/displayViews/Settings';
import BugPickerQuestion from './components/formViews/questions/BugPickerQuestion';
import CritterPickerQuestion from './components/formViews/questions/CritterPickerQuestion';
import QueenCellContent from './components/formViews/questions/QueenCellContent';
import {setModalState} from './actions';
import { connect } from 'react-redux';
import {View} from 'react-native';
import {Constants} from 'expo';



const RouterComponent = (props) => {
	return (
		<Router sceneStyle ={{marginTop: Constants.statusBarHeight}}>
			<Scene key = "root">
				<Scene key= "addYardView" component= {AddYardView} title="Add Yard" />
				<Scene key= "addHiveView" component= {AddHiveView} title="Add Hive" />
				<Scene key= "barQuestion" component= {BarQuestion} renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "beesFound"	component= {BeesFound} title="Bees Found"  renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "bugPickerQuestion" component= {BugPickerQuestion} title="Bugs Found" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "critterPickerQuestion" component= {CritterPickerQuestion} title="Critter Evidence" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "colonySplit"	component= {ColonySplit} title="Colony Split" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "editInspectionView" component= {EditInspectionView} />
				<Scene key= "entranceActivity"	component= {EntranceActivity} title="Entrance Activity" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "entrancesUsed" component = {EntrancesUsed} title= "Entrances Used" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "externalBeeActivityLevel"	component= {ExternalBeeActivityLevel} title="External Bee Activity Level" renderRightButton={(route) => { return route.noteButton }} />
				<Scene key= "externalVisualCues" component = {ExternalVisualCues} title = "External Visual Cues" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "formStart" component = {FormStart} title = "" />
				<Scene key= "formEnd" component = {FormEnd} title = "" />
				<Scene key= "finalJarFluid" component ={FinalJarFluid} title= "Feeder Jar" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "finalNumberOfBars" component = {FinalNumberOfBars} title = "Final Number of Bars" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "hiveCapacity"	component= {HiveCapacity} title="Hive Capacity" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= 'honeyHarvestedForBees' component= {HoneyHarvestedForBees} title="Honey" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= 'honeyHarvestedForMe' component= {HoneyHarvestedForMe} title="Honey" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "internalActivity"	component= {InternalActivity} title="Internal Activity" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "initialJarFluid" component={InitialJarFluid} title="Feeder Jar" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "login"	component= {LoginForm} title="" hideNavBar={true} sceneStyle ={styles.statusBarSpacing} initial />
				<Scene key= "miteCheckPerformed" component= {MiteCheckPerformed} title="Mite Check Performed"  renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "numberBarsCombsAttached" component= {NumberBarsCombsAttached} title="Number Bars Combs Attached" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "numberBarsInnerChamber"component= {NumberBarsInnerChamber} title="Number Bars Inner Chamber" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "pollenColor" component = {PollenColor} title = "Pollen Color" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "pollenCollectionVisible" component= {PollenCollectionVisible} title="Pollen Collection Visible" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "ventBoardPosition"	component = {VentBoardPosition} title="Vent Board Position" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "yardSelectionView" component = {YardSelectionView} title = "Select yard" renderBackButton = {()=>{return <View/>}}/> 
				<Scene key= "yardFormSelection" component = {YardFormSelection} title = "Select yard" renderBackButton = {()=>{return <View/>}}/> 
				<Scene key= "hiveSelectionView" component = {HiveSelectionView} title = "" />
				<Scene key= "hiveFormSelection" component = {HiveFormSelection} title = "" />
				<Scene key= "hiveInspectionView" component = {HiveInspectionView} title = "" />
				<Scene key= "hiveSettings" component = {HiveSettings} title = "Hive Settings" />
				<Scene key= "queenCellContent" component = {QueenCellContent} title ="Queen Cell Content" renderRightButton={(route) => { return route.noteButton }}/>
				<Scene key= "yardSettings"  component = {YardSettings} title = "Yard Settings" />
				<Scene key= "settings" component = {Settings} title = "Settings" renderBackButton = {()=>{return <View/>}}/>
			</Scene>
		</Router>
	);
};
const styles = StyleSheet.create({
	statusBarSpacing: {
		marginTop: Constants.statusBarHeight
	}
})

const mapStateToProps = (state) => {
  const {
  observations,
  modalVisible
  } = state.form
  return {
  observations,
  modalVisible
  };
};

export default connect( null, {setModalState})(RouterComponent);
