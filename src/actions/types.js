//Where we save the types for dispatch between actions and reducers
export const ADDRESS_LOADED = "address_loaded";

export const CONTINUE_FORM = "continue_form";

export const USER_UPDATE = 'user_update';

export const FORM_INITIALIZED = "form_initialized";
export const FORM_UPDATE = 'form_update';

export const LOGIN_USER = 'login_user';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const NONCE_FETCH_FAILURE = 'nonce_fetch_failure';

export const LOGOUT = "logout";
export const POLLEN_LOAD = 'pollen_load';

export const YARD_LOADING = "yard_loading";
export const YARD_LOADED = "yard_loaded";
export const YARD_OWNER_STATUS_LOADED = 'yard_owner_status_loaded';

export const HIVE_LOADING = "hive_loading";
export const HIVE_LOADED = "hive_loaded";
export const HIVE_OWNER_STATUS_LOADED = "hive_owner_status_loaded";
export const HIVES_LOADED_FOR_FORMS = "hives_loaded_for_forms";
export const HIVE_SELECTED_FOR_FORM = "hive_selected_for_form";

export const ADD_HIVE = "add_hive";
export const ADD_YARD = "add_yard";
export const ADD_YARD_OPEN = "add_yard_open";

export const SETTINGS_OPENED = "settings_opened";
export const YARD_SETTINGS_UPDATED = "yard_settings_updated";
export const LOAD_YARDS ="load_yards";
export const LOAD_YARD_FOR_FORM = "load_yard_for_form";

export const POPULATE_YARD_DATA = "populate_yard_data";
export const SAVE_YARD_SETTINGS = "save_yard_settings";

export const LOCATION_SUCCESS = "location_success";

export const INSPECTION_SELECTED = "inspection_selected";

export const WEATHER_SUCCESS = "weather_success";

export const NEXT_BAR = "next_bar";

export const MODAL_STATE_CHANGE = "modal_state_change";
