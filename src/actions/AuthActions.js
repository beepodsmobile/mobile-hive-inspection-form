import { 
	USER_UPDATE,
	LOGIN_USER,
	LOGIN_USER_SUCCESS,
	LOGIN_USER_FAIL,
	NONCE_FETCH_FAILURE
 } from './types';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';

export const callLoginUserFail = (response, dispatch) => {
		dispatch({
			type: LOGIN_USER_FAIL,
			payload: response
		});
	
};

export const callLoginUserSuccess = (response, dispatch) =>{
	console.log('callLoginUserSuccess');
		dispatch({
			type: LOGIN_USER_SUCCESS,
			payload: response['data']['user']
		});
		Actions.yardSelectionView({title: "Welcome " + response['data']['user']['firstname']});
	
};

export const callNonceError = (response, dispatch) => {
		dispatch({
			type: NONCE_FETCH_FAILURE,
			payload: response
		});
	
};
export const loginUser = ({ username, password}) => {		
	return(dispatch) => {
		dispatch({
			type: LOGIN_USER
		});
		nonceFetch(username, password, dispatch);
	}
};

export const nonceFetch = (username, password, dispatch) => {
	console.log('nonce fetch hit');
	console.log(username, password)
	let nonceRequestUrl = 'https://www.beepods.com/api/get_nonce/?controller=auth&method=generate_auth_cookie';
	axios.get(nonceRequestUrl)
		.then((response) => {
			var nonce = response['data']['nonce'];
			console.log('nonce fetch success', username, password, nonce);
			userFetch(username, password, nonce, dispatch);
		})
		.catch((response) => {
			callNonceError(response, dispatch);
		}
		);
};

export const userFetch = (username, password, nonce, dispatch) =>{
	console.log('userFetch', username, password, nonce);
	let userLoginRequestUrl = `https://www.beepods.com/api/auth/generate_auth_cookie/?nonce=${nonce}&username=${username}&password=${password}`;
			axios.get(userLoginRequestUrl)
				.then((response) => {
						console.log('Login User Success', response);
						callLoginUserSuccess(response, dispatch);	
				})
				.catch((response) => {
					console.log('Login User Fail', response, userLoginRequestUrl);
					callLoginUserFail(response, dispatch);
				});
};

export const userUpdate = ({prop, value}) =>  {
	return(dispatch) => {
		dispatch({
			type: USER_UPDATE,
			payload: {prop , value }
		});
	};
};
