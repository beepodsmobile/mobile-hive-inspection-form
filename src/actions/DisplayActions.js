//Where we do actions involving the components in the displayViewsimport axios from 'axios';h DisplayReducer state
import { 
    ADD_YARD,
    ADD_YARD_OPEN,
    ADDRESS_LOADED,
    HIVE_LOADING,
    HIVE_LOADED,
    HIVE_OWNER_STATUS_LOADED,
    INSPECTION_SELECTED,
    ADD_HIVE,
    LOAD_YARDS,
    LOCATION_SUCCESS,
    POPULATE_YARD_DATA,
    SAVE_YARD_SETTINGS,
    SETTINGS_OPENED,
    YARD_LOADED,
    YARD_LOADING,
    YARD_OWNER_STATUS_LOADED,
    YARD_SETTINGS_UPDATED,
    LOGOUT
 } from './types';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';

export const logout = ({}) =>{
    return(dispatch) => {
        dispatch({
            type: LOGOUT
        });
        Actions.login();
    }
};

export const addHive = ({hiveId}) => {       
    return(dispatch) => {
        dispatch({
            type: ADD_HIVE
        });
        Actions.addHiveView();
    }
};
export const addYard = ({userId}) => {       
    return(dispatch) => {
        dispatch({
            type: ADD_YARD
        });
        retrieveLocation(dispatch);
    }
};

export const editInspection = ({currentInspection}) => {
    console.log(currentInspection);
    return(dispatch)=>{
        dispatch({
            type: INSPECTION_SELECTED,
            payload: currentInspection
        });
        Actions.editInspectionView();
    }
};

const fetchAddress = (id, dispatch) => {
    let addressUrl = `https://www.beepods.com/wp-json/inspection/v1/Address/?id=${id}`
    axios.get(addressUrl)
        .then((response)=>{
            console.log('fetchAddresssuccess',response['data']);
                dispatch({
                    type: POPULATE_YARD_DATA,
                    payload: response['data']
                });
        });
};

const fetchHiveInfo = (hiveId, userId, hiveName, dispatch) =>{
    let yardRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/gethiveinspections/?id=${userId}&hiveId=${hiveId}`;
    let placeholder = {hello: "hello"};
    let config = {"Cache-Control" : "no-cache"};
    axios.post(yardRequestUrl, placeholder, config)
        .then((response) => {
            dispatch({
                type: HIVE_LOADED,
                payload: response['data']
            });
            fetchHiveOwnerStatus(hiveId, userId, dispatch);
            Actions.hiveInspectionView({title: hiveName});
        })
        .catch((response) => {
            console.log('catch', response);
            Actions.hiveInspectionView({title: hiveName});
        }
        );
    
};

const fetchYardInfo = (yardId, userId, yardName, dispatch) =>{
    let yardRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/userhives/?id=${userId}&yardId=${yardId}`;
    let config = {"Cache-Control" : "no-cache, no-store, must-revalidate"};
    let placeholder = {hello: "hello"};
    axios.post(yardRequestUrl, placeholder, config)
        .then((response) => {
            dispatch({
                type: YARD_LOADED,
                payload: response['data']
            });
            fetchYardOwnerStatus(yardId, userId, dispatch);
            Actions.hiveSelectionView({title: yardName});
        })
        .catch((response) => {
            dispatch({
                type: YARD_LOADED,
                payload: []
            });
            Actions.hiveSelectionView({title: yardName});
        }
        );
};

const fetchYardOwnerStatus = (yardId, userId, dispatch) => {
    let yardRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/yardOwnerStatus/?id=${userId}&yardId=${yardId}`;
    axios.get(yardRequestUrl)
        .then((response) => {
            console.log(response);
            dispatch({
                type: YARD_OWNER_STATUS_LOADED,
                payload: response['data']
            });
        })
        .catch((response) => {
            console.log('catch', response);
        }
        );
};
const fetchHiveOwnerStatus = (hiveId, userId, dispatch) => {
    let yardRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/hiveOwnerStatus/?id=${userId}&hiveId=${hiveId}`;
    axios.get(yardRequestUrl)
        .then((response) => {
            console.log(response);
            dispatch({
                type: HIVE_OWNER_STATUS_LOADED,
                payload: response['data']
            });
        })
        .catch((response) => {
            console.log('catch', response);
        }
        );
};
const getCurrentDate = () => {
    let dateItem = new Date();
    var year = dateItem.getFullYear();
    var month = dateItem.getMonth() + 1;
    var day = dateItem.getDate();
    var monthString;
    var dayString;
    if(month < 10){
        monthString = '0'+ String(month);
    }
    else{
        monthString = String(month);
    }
    if(day < 10 ){
        dayString = "0"+ String(day);
    }
    else{
        dayString= String(day);
    }
    return year+"-"+monthString+"-"+dayString;
};

export const getHive = ({ hiveId, id, hiveName}) => {      
    return(dispatch) => {
        dispatch({
            type: HIVE_LOADING,
            payload: {id: hiveId, name: hiveName}
        });
        fetchHiveInfo(hiveId, id, hiveName, dispatch);
    }
};

export const getYard = ({ yardId, id, yardName, yardDescription}) => {     
    return(dispatch) => {
        dispatch({
            type: YARD_LOADING,
            payload: { id: yardId, name: yardName, description: yardDescription}
        });
        fetchYardInfo(yardId, id, yardName, dispatch);
    }
};

export const loadYards = ({data}) => {
    return(dispatch)=> {
        dispatch({
            type: LOAD_YARDS,
            payload: data
        });
    }
};

export const populateYardInfo = ({currentYard}) => {
    return(dispatch) => {
        fetchAddress(currentYard['AddressId'], dispatch);
    }
};

const postAddressUpdate = (address, city, state, zip, lat, lng, userId, yardId, email, yardName, yardDescription, dispatch) => {
    console.log("address update");  
    let addressUpdateObject = {
        'uid': userId,
        'email': email,
        'yardName': yardName,
        'yardDescription': yardDescription,
        'yardId' : yardId,
        'latitude': lat,
        'longitude': lng,
        'address': address,
        'city': city,
        'state': state,
        'zip': zip,
        'country': "United States"
    };
    console.log(addressUpdateObject);
    let config = {'content-type': 'application/json'}
    let postAddressUpdateUrl = `https://www.beepods.com/wp-json/inspection/v1/yardaddupdate/`;
    axios.post(postAddressUpdateUrl, addressUpdateObject, config)
        .then(response => {
            console.log('success', response);
            Actions.yardSelectionView();
        })
        .catch(response => {
            console.log('fail', response);
        });

};

export const removeHive = ({hiveId, yardId, userId}) =>{
    let hiveRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/hiveremove/?id=${userId}&yardId=${yardId}&hiveId=${hiveId}`;
    let config = {"Cache-Control" : "no-cache"};
    let post = {};
    axios.post(hiveRequestUrl, post, config)
        .then(response=>{
            
        })
        .catch(response=>{
            
        });
        Actions.yardSelectionView();
        return(dispatch) => {
            dispatch({
                type: SAVE_YARD_SETTINGS
            });
        }     

}
const retrieveAddress = (data, dispatch) => {
    console.log(data);
    let APICall = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    let googleMapsAPIKey = "&key=AIzaSyAU4pj1i5c905yjhgWE15V1SbT-jOgRaMg";
    APICall += data['latitude'].toString(); 
    APICall +=",";
    APICall += data['longitude'].toString();
    console.log(APICall);
    axios.get(APICall)
        .then((response)=>{
            console.log(response);
            console.log(response["data"]["results"][0]['formatted_address']);
            let address = response["data"]["results"][0]['formatted_address'].split(',');
            let state = address[2].trim().split(' ');
            let addressObject = {
                address: address[0].trim(),
                city: address[1].trim(),
                state: state[0],
                zip: state[1]
            };
            dispatch({
                type: ADDRESS_LOADED,
                payload: addressObject
            });
            Actions.addYardView();
        })
        .catch((response)=>{Actions.addYardView()});
    
};

const retrieveLocation = (dispatch) => {
    navigator.geolocation.getCurrentPosition((response)=>{
        retrieveLocationSuccess(response, dispatch)
    }, retrieveLocationFailure);
};

const retrieveLocationFailure = (data) => {
    Actions.addYardView();
};

const retrieveLocationSuccess = (data, dispatch) => {
    console.log('success', data);
    dispatch({
        type: LOCATION_SUCCESS,
        payload: data['coords']
    });
    retrieveAddress(data['coords'], dispatch);
};

export const saveNewHive = ({name, description, serial, breedId, user, yard}) =>{
    console.log("serial", serial);
    let today = getCurrentDate();
    let hiveUpdateObject = {
        'uid': user['id'],
        'yardId': yard,
        'breedId': breedId,
        'hiveName': name,
        'hiveDescription': description,
        "dateAdded": today,
        "dateModified": today,
        "serial": serial,
        "email": user['email']
    };
    console.log(hiveUpdateObject);
    let config = {'content-type': 'application/json'};
    let postHiveUpdateUrl = `https://www.beepods.com/wp-json/inspection/v1/hiveaddupdate/`;
    axios.post(postHiveUpdateUrl, hiveUpdateObject, config)
        .then(response => {
            console.log(response);
        })
        .catch(response=>{console.log(response)});
    return(dispatch) =>{
        dispatch({type: ADD_HIVE});
        Actions.yardSelectionView();
    }
}

export const saveNewYard = ({name, description, address, city, state, zip, lat, lng, user}) => {
    let addressUpdateObject = {
        'uid': user['id'],
        'email': user['email'],
        'yardName': name,
        'yardDescription': description,
        'latitude': parseFloat(lat),
        'longitude': parseFloat(lng),
        'address': address,
        'city': city,
        'state': state,
        'zip': zip,
        'country': "United States"
    };
    console.log(addressUpdateObject);
    let config = {'content-type': 'application/json'};
    let postAddressUpdateUrl = `https://www.beepods.com/wp-json/inspection/v1/yardaddupdate/`;
    axios.post(postAddressUpdateUrl, addressUpdateObject, config)
        .then(response => {
            console.log('success', response);
            Actions.yardSelectionView();
        })
        .catch(response => {
            console.log('fail', response);
        });
        return(dispatch) => {
            dispatch({
                type: ADD_YARD
            });
        }
};

export const saveYardSettings = ({address, city, state, zip, lat, lng, userId, yardId, email, yardName, yardDescription}) => {
    
    return(dispatch) => {
        dispatch({
            type: SAVE_YARD_SETTINGS
        });
        postAddressUpdate(address, city, state, zip, lat, lng, userId, yardId, email, yardName, yardDescription, dispatch);
    }
};

export const updateYardSetting = ({prop, value}) =>{
    console.log('yardSettings', prop, value);
    return(dispatch) => {
        dispatch({
            type: YARD_SETTINGS_UPDATED,
            payload: {prop, value}
        });
    }
};

export const yardSettings = ({currentYard}) => {
    console.log(currentYard);
    return(dispatch) =>{
        dispatch({
            type: SETTINGS_OPENED
        });
        Actions.yardSettings();
    }
    
};
export const hiveSettings = ({}) => {
    return(dispatch) => {
        dispatch({
            type:SETTINGS_OPENED
        });
        Actions.hiveSettings();
    }
}