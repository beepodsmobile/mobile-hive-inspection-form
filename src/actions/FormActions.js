//Where we do actions involving the components in the formViews with FormReducer state
import { CONTINUE_FORM, FORM_INITIALIZED, FORM_UPDATE, POLLEN_LOAD, LOAD_YARD_FOR_FORM, HIVES_LOADED_FOR_FORMS, HIVE_SELECTED_FOR_FORM, WEATHER_SUCCESS, NEXT_BAR, MODAL_STATE_CHANGE } from './types';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';

const apiCallHives = (dispatch, yardId, yardName, userId) => {
	let hiveRequestUrl = `https://www.beepods.com/wp-json/inspection/v1/userhives/?id=${userId}&yardId=${yardId}`;
    let config = {"Cache-Control" : "no-cache"};
    let placeholder = {hello: "hello"};
    axios.post(hiveRequestUrl, placeholder, config)
        .then((response) => {
            dispatch({
                type: HIVES_LOADED_FOR_FORMS,
                payload: response['data']
            });
            Actions.hiveFormSelection({title: yardName});
        })
        .catch((response) => {
            console.log('catch', response);
            Actions.hiveFormSelection({title: yardName});
        }
        );
};

export const barUpdate = ({prop, value}) =>  {
	return(dispatch) => {
		dispatch({
			type: FORM_UPDATE,
			payload: {prop , value}
		})
		if(value[value.length-1]['follower'] == 1){
			navigatePage('barsFinished');
		}
		else if(value.length >= 32){
			navigatePage('barsFinished'); 
		}
		else{
			dispatch({
				type: NEXT_BAR,
			});
			Actions.barQuestion({title: "Bar " + (value.length+1)});
		}
		
	};	
};

export const beginForm = () => {
	return(dispatch) => {
		dispatch({
			type: FORM_INITIALIZED
		})
		Actions.externalBeeActivityLevel();
	}
}

export const continueForm = ({prop}) => {
	return(dispatch) => {
		dispatch({
			type: CONTINUE_FORM
		});
		navigatePage(prop);
	}
};

export const formUpdate = ({prop, value}) =>  {
	return(dispatch) => {
		dispatch({
			type: FORM_UPDATE,
			payload: {prop , value}
		})
		if(prop == 'honeyHarvestedForBeesComplete'){
			dispatch({
				type: FORM_UPDATE,
				payload: {prop: 'complete', value: true}
			});
		}
		if(prop == 'pollenCollectionVisible'){
			if(value==false){
				navigatePage('pollenColor');
			}
			else{
				navigatePage(prop);
			}
		}
		else{
			navigatePage(prop);
		}
		
	};	
};



export const getFormStartSplash = ({hiveId, hiveName}) => {
	return(dispatch) => {
		dispatch({
			type: HIVE_SELECTED_FOR_FORM,
			payload: {id: hiveId, name: hiveName}
		});
		Actions.formStart({title: hiveName + " Form"});
	}
};

export const getHivesForForm = ({yard, userId}) => {
	return(dispatch)=>{
		dispatch({
			type: LOAD_YARD_FOR_FORM,
			payload: {yardId: yard['YardId'], yardName: yard['YardName']}
		});
		getWeather(dispatch, yard);
		apiCallHives(dispatch, yard['YardId'], yard['YardName'], userId);
	}
}
const getWeather = (dispatch, yard) => {
	const openWeatherURL = 'http://api.openweathermap.org/data/2.5/weather?';
	const openWeatherAPIKey = 'cea53ed56c3ee5fc14e6d1120b4e9e9d';
	const lat = yard['Latitude'];
	const lng = yard['Longitude'];
	let temperature;
	let humidity;
	let airPressure;
	let weatherCall = `${openWeatherURL}lat=${lat}&lon=${lng}&APPID=${openWeatherAPIKey}`;
	axios.get(weatherCall)
			.then(response => {
				temperature = response.data.main.temp;
				airPressure = response.data.main.pressure;
				humidity = response.data.main.humidity;
				dispatch({
					type: WEATHER_SUCCESS,
					payload: {
						temperature: temperature,
						humidity: humidity,
						airPressure: airPressure
					}
				});
			});
}
export const navigatePage = (prop) => {
			switch (prop) {
			case 'externalBeeActivityLevel':
				Actions.externalVisualCues();
				break;

			case 'externalVisual':
				Actions.entranceActivity();
				break;

			case 'entranceActivity':
				Actions.entrancesUsed();
				break;

			case 'entrancesUsed':
				Actions.pollenCollectionVisible();
				break;

			case 'pollenCollectionVisible':
				Actions.pollenColor();
				break;

			case 'pollenColor':
				Actions.hiveCapacity();
				break;

			case 'hiveCapacity':
				Actions.internalActivity();
				break;

			case 'internalActivity':
				Actions.numberBarsCombsAttached();
				break;

			case 'numberBarsCombsAttachedComplete':
				Actions.numberBarsInnerChamber();
				break;

			case 'numberBarsInnerChamberComplete':
				Actions.beesFound();
				break;

			case 'beesFound':
				Actions.critterPickerQuestion();
				break;
			case 'bugs':
			// needs a filler screen to refresh
				Actions.beesFound();
				Actions.bugPickerQuestion();
				break;
			case 'bugsSeen':
				Actions.critterPickerQuestion();
				break;
			case 'critters':
			// needs filler screen to refresh
				Actions.beesFound();
				Actions.critterPickerQuestion();
				break;
			case 'critterEvidence':
				Actions.ventBoardPosition();
				break;
			case 'ventBoardPosition':
				Actions.barQuestion({title: "Bar 1"});
				break;
			case 'barsFinished':
				Actions.queenCellContent();
				break;
			case 'queenCellContent':
				Actions.miteCheckPerformed();
				break;
			case 'miteCheckPerformed':
				Actions.colonySplit();
				break;
			case 'colonySplit':
				Actions.finalNumberOfBars();
				break;
			case 'finalNumberBarsComplete':
				Actions.initialJarFluid();
				break;
			case 'initialJarFluidComplete':
				Actions.finalJarFluid();
				break;
			case 'finalJarFluidComplete':
				Actions.honeyHarvestedForMe();
				break;
			case 'honeyHarvestedForMeComplete':
				Actions.honeyHarvestedForBees();
				break;
			case 'honeyHarvestedForBeesComplete':
				Actions.formEnd();
			default:
				break;
	}
};
export const setModalState = () => {
	return(dispatch) => {
		dispatch({
			type: MODAL_STATE_CHANGE
		});
	}
}

export const submitForm = ({inspection}) => {
	console.log(inspection);
	let config = {'content-type': 'application/json'}
    let postInspectionUrl = `https://www.beepods.com/wp-json/inspection/v1/addupdateinspection/`;
    axios.post(postInspectionUrl, inspection, config)
        .then(response => {
            console.log('success', response);
            Actions.yardSelectionView();
        });
	return(dispatch) => {
		dispatch({
			type: FORM_INITIALIZED
		});

	}
};