import React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { Font } from 'expo';

//import { ColorButton, ColorOptionsQuestion } from './src/components/common';
//import PollenColor from './src/questions/PollenColor';
import YardSelectionView from './src/components/test/YardSelectionView';
import HiveSelectionView from './src/components/test/HiveSelectionView';

import HiveSettings from './src/components/test/HiveSettings';
import BarSliderForm from './src/components/test/BarSliderForm';
import RouterComponent from './src/Router';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import { createStore, applyMiddleware } from 'redux';


export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
      await Font.loadAsync({
      "Exo 2": require("./assets/fonts/Exo2-Regular.ttf"),
      "Exo 2 Bold": require("./assets/fonts/Exo2-Bold.ttf"),
      "Exo 2 Italic": require("./assets/fonts/Exo2-Italic.ttf")
    });

    this.setState({ fontLoaded: true });
  }

  render() {
    const store= createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      //Aaron:
      //Nathan: <BarSliderForm />
      this.state.fontLoaded? (
          <Provider store = {store}>
            <RouterComponent />
          </Provider>
      ) : null
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

